#ifndef EventRecoAnalysis_h
#define EventRecoAnalysis_h


#include <iostream>
#include <string>
#include <math.h>
#include <vector>
#include <set>
#include <map>

#include <TFile.h>
#include <TChain.h>
#include <TH1.h>
#include <TH2.h>
#include <TLorentzVector.h>
#include <TLegend.h>


#include "ClassifyHF/ClassifyHF.h"
#include "ClassifyHF/GeneratorInfo.h"

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/tools/MassDropTagger.hh"
#include "fastjet/tools/Filter.hh"
//#include "fastjet/tools/JetMedianBackgroundEstimator.hh"

/*
#include "HEPTopTagger/TopTagger.hh"
#include "HEPTopTagger/Range.hh"
#include "Python.h"
#include "PDFTool.h"

#include <TStyle.h>
#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <THStack.h>

#include <TPython.h>

#include "ApplyJetCalibration/ApplyJetCalibration/ApplyJetCalibration.h"
For efficiency, should loop over datasets separately, and then use the map structure to read histograms from the files.

//using namespace std;



*/

using namespace std;
using namespace fastjet;


class EventRecoAnalysis : public TObject{

  ClassDef(EventRecoAnalysis, 1); //The class title

 public:
  EventRecoAnalysis(int ds_ID, vector<string> ds_fileloc, int b_io, string s_out, string s_tree, int nfirst=0,int nlast=-1,int doJES=0);
  ~EventRecoAnalysis();
  int Init();
  int Loop();
  int Process(long entry);
  int Terminate();

 protected:
  map<string, TH1D*> m_h1d;
  map<string, TH2D*> m_h2d;

  int m_isSignal;
  int m_channel;
  bool m_isMC;
  string m_stree;
  vector<string> m_fileloc;
  string m_ds_ID;
  int m_IO;
  int m_nfirst;
  int m_nlast;
  float m_doJES;

  string m_pregroom_jetcol;
  int m_pregroom_jetID;
  string m_sout_hist;
  string m_sout_tree;
  TFile* m_outfile_hist;
  TFile* m_outfile_tree;

  map<int, string> m_fatjetcol_IDs;
  string m_jetcol_ID;
  TChain* m_chain;
  TTree* m_outtree;
  int m_nev;
  float m_ev_wgt;
  int m_PDF_set;

  //PDFTool* m_pdfTool;
  //JetAnalysisCalib::JetCalibrationTool* m_calib_Tool;

  map<string, vector<int> > m_rebuild_jetcol_IDs; //to optimise jet rebuilding

  static const float GeV;
  static const double MASS_W;
  static const double MASS_TOP;

  static const float m_jet_ptmin;
  static const float m_jet_etamax;
  static const float m_jet_mv1cut;
  static const float m_fatjet_ptmin_tight;
  static const float m_fatjet_ptmin_loose;
  static const float m_fatjet_etamax;
  static const float m_el_ptmin;
  static const float m_el_etamax;
  static const float m_mu_ptmin;
  static const float m_mu_etamax;
  static const float m_tau_ptmin;
  static const float m_tau_etamax;

  static const float m_lep_ptmin;
  static const float m_lep_etamax;


  static const float m_e_j_or_dr;
  static const float m_j_e_or_dr;
  static const float m_j_mu_or_dr;

  static const double m_ptbinsize; static const double m_ptbinmin; static const double m_ptbinmax;
  static const double m_mbinsize; static const double m_mbinmin; static const double m_mbinmax;
  static const double m_etabinsize; static const double m_etabinmin; static const double m_etabinmax;
  static const double m_phibinsize; static const double m_phibinmin; static const double m_phibinmax;

  static const double m_rspnsbinsize; static const double m_rspnsbinmin; static const double m_rspnsbinmax;
  static const double m_drbinsize; static const double m_drbinmin; static const double m_drbinmax;
  static const double m_ssbinsize; static const double m_ssbinmin; static const double m_ssbinmax;

  static const double m_beam_nrg;
  static const double m_beam_nrg_RW;

  static const int WHIST;
  static const int WTREE;

  static const int I_PYTHIA8;
  static const int I_PYTHIA6;
  static const int I_HERWIG;
  static const int I_MADPYTH;

  static const int ELE;
  static const int MUON;
  static const int TAU;

  static const int I_LEPTOP_B;
  static const int I_HADTOP_B;
  static const int I_HADTOP_Q1;
  static const int I_HADTOP_Q2;
  static const int I_HIGGS_B1;
  static const int I_HIGGS_B2;
  static const int I_EXTRA_HF;

  static const int MC_GHOST_SHIFT;
  static const int B_GHOST_SHIFT;

  static const int I_BL;
  static const int I_1P0B;
  static const int I_1P1B;
  static const int I_Q1Q2;
  static const int I_2P1BCOMB;
  static const int I_2P1BPART;
  static const int I_2P2BCOMB;
  static const int I_B1B2;
  static const int I_BHQ1Q2;
  static const int I_3P1BCOMB;
  static const int I_3P2BCOMB;
  static const int I_3P3BCOMB;
  static const int I_GT3P;
  static const int I_NOP;

  static const double m_drmatch_l;
  static const double m_drmatch_j;
  static const double m_drmatch_fj;

  static const int inTOPO;
  static const int inTRK;
  static const int inTRUTH;
  static const int inMITRU;

  static const int sigLC;
  static const int sigEM;

  static const int I_PREGROOM_INPUT;
  static const int rc_ak4_15;
  //static const int rc_ak4_jvf5;
  static const int rc_ak4_elj;
  static const int rc_el_cl;
  static const int rc_ak4_nonb;
  static const int rc_ak4_b;
  static const int rc_ak4_nolepb;
  static const int rc_ak4_vetolist;

  static const double kappa;
  static const double c_track_pt;
  static const double c_track_deltaR;
  static const int c_track_Nmin;
  static const double c_track_d0;
  static const double c_track_z0;
  static const double c_track_chi2;
  static const int c_track_nPixHits;
  static const int c_track_nSCTHits;

  map<int, int> m_mc_bc_ind;
  vector<int> m_jet_ind;
  vector<int> m_el_ind;
  vector<int> m_mu_ind;
  vector<int> m_jet_e_overlap_ind; //indices of jets to be removed due to overlap with electron
  vector<int> m_el_jet_overlap_ind; //indices of electrons to be removed due to overlap with jet
  vector<int> m_mu_jet_overlap_ind; //indices of muons to be removed due to overlap with jet
  map<int, vector<int> > m_fatjet_ind;

  static const string s_nolepb;
  static const string s_nolepb_trim;
  static const string s_nolepb_akt;
  static const string s_nolepb_akt4;
  static const string s_nolepb_R6;
  static const string s_nolepb_trim_R6;
  static const string s_nolepb_R10;
  static const string s_nolepb_trim_R10;
  //static const string s_vetoht_nolepb_bdrs;

  static const string s_truth;
  static const string s_truth_trim;
  static const string s_truth_akt;
  static const string s_truth_R6;
  static const string s_truth_trim_R6;
  static const string s_truth_R10;
  static const string s_truth_trim_R10;
  //static const string s_vetoht_truth_bdrs;

  static const int i_nolepb;
  static const int i_nolepb_trim;
  static const int i_nolepb_akt;
  static const int i_nolepb_akt4;
  static const int i_nolepb_R6;
  static const int i_nolepb_trim_R6;
  static const int i_nolepb_R10;
  static const int i_nolepb_trim_R10;
  //static const int i_vetoht_nolepb_bdrs;
  
  static const int i_truth;
  static const int i_truth_trim;
  static const int i_truth_akt;
  static const int i_truth_R6;
  static const int i_truth_trim_R6;
  static const int i_truth_R10;
  static const int i_truth_trim_R10;
  
  //static const int i_vetoht_truth_bdrs;



  //static const double m_drmatch_j = 0.2;
  //static const double m_drmatch_fj = 0.8;


  int m_p_leptop_lep_flav; //el:0; mu:1; tau:2;
  int m_j_leptop_lep_flav; //el:0; mu:1; tau:2;

  //parton indices
  int m_ip_top_hard;
  int m_ip_atop_hard;
  int m_ip_leptop_hard;
  int m_ip_hadtop_hard;
  int m_ip_higgs_hard;

  int m_ip_top;
  int m_ip_atop;
  int m_ip_leptop;
  int m_ip_hadtop;
  int m_ip_higgs;

  int m_ip_top_b;
  int m_ip_top_W;
  int m_ip_atop_b;
  int m_ip_atop_W;


  int m_ip_leptop_b;
  int m_ip_leptop_W;
  int m_ip_leptop_lep;
  int m_ip_leptop_nu;

  int m_ip_hadtop_b;
  int m_ip_hadtop_W;
  int m_ip_hadtop_q1;
  int m_ip_hadtop_q2;

  int m_ip_higgs_b1;
  int m_ip_higgs_b2;

  //jet and lepton selection
  int m_njet;
  int m_nlep;


  //jet match indices
  int m_ij_leptop_b;

  int m_ij_hadtop_b;
  int m_ij_hadtop_q1;
  int m_ij_hadtop_q2;

  int m_ij_higgs_b1;
  int m_ij_higgs_b2;

  //fatjet match indices
  map<int,int> m_ifj_hadtop;
  map<int,int> m_ifj_higgs; 

  //Candidate indices
  int m_ij_hadW_cand;
  int m_ij_hadtop_nolepb_cand;
  int m_ij_hadtop_truth_cand;
  int m_ij_hadtop_Wbhtt_cand;
  int m_ij_hadtop_Wbtrim_cand;

  //-------------------------------------------------------------------------
  //jet match indices
  int m_ij_ind_leptop_b;

  int m_ij_ind_hadtop_b;
  int m_ij_ind_hadtop_q1;
  int m_ij_ind_hadtop_q2;

  int m_ij_ind_higgs_b1;
  int m_ij_ind_higgs_b2;

  //fatjet match indices
  map<int,int> m_ifj_ind_hadtop;
  map<int,int> m_ifj_ind_higgs; 

  //Candidate indices
  int m_ij_ind_hadW_cand;
  int m_ij_ind_hadtop_nolepb_cand;
  int m_ij_ind_hadtop_truth_cand;
  int m_ij_ind_hadtop_Wbhtt_cand;
  int m_ij_ind_hadtop_Wbtrim_cand;

  //-------------------------------------------------------------------------


  //TLorentzVector* tlv_test;
  
  //TLorentzVectors for matched partons and jets
  //Create some local M_TLVs
  TLorentzVector* m_tlv_p_top_hard;
  TLorentzVector* m_tlv_p_atop_hard;
  
  TLorentzVector* m_tlv_p_leptop_hard;
  TLorentzVector* m_tlv_p_hadtop_hard;
  
  TLorentzVector* m_tlv_p_higgs_hard;
  TLorentzVector* m_tlv_p_ttbar_hard;

  //  
  TLorentzVector* m_tlv_p_top;
  TLorentzVector* m_tlv_p_atop;
  
  TLorentzVector* m_tlv_p_leptop;
  TLorentzVector* m_tlv_p_hadtop;
  
  TLorentzVector* m_tlv_p_higgs;
  TLorentzVector* m_tlv_p_ttbar;

  //
  TLorentzVector* m_tlv_p_leptop_b;
  TLorentzVector* m_tlv_p_leptop_W;
  TLorentzVector* m_tlv_p_leptop_lep;
  TLorentzVector* m_tlv_p_leptop_nu;

  TLorentzVector* m_tlv_p_hadtop_b;
  TLorentzVector* m_tlv_p_hadtop_W;
  TLorentzVector* m_tlv_p_hadtop_q1;
  TLorentzVector* m_tlv_p_hadtop_q2;

  TLorentzVector* m_tlv_p_higgs_b1;
  TLorentzVector* m_tlv_p_higgs_b2;

  //0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  //Input branch list
  //Fat jets
  map<int, int> fatjet_n;
  map<int, vector<float>* > fatjet_pt;
  map<int, vector<float>* > fatjet_eta;
  map<int, vector<float>* > fatjet_phi;
  map<int, vector<float>* > fatjet_m;
  map<int, vector<float>* > fatjet_constscale_pt;
  map<int, vector<float>* > fatjet_constscale_eta;
  map<int, vector<float>* > fatjet_constscale_phi;
  map<int, vector<float>* > fatjet_constscale_m;
  map<int, vector<float>* > fatjet_ActiveArea;
  map<int, vector<float>* > fatjet_ActiveArea_px;
  map<int, vector<float>* > fatjet_ActiveArea_py;
  map<int, vector<float>* > fatjet_ActiveArea_pz;
  map<int, vector<float>* > fatjet_ActiveArea_E;

  map<int, vector<float>* > fatjet_SPLIT12;
  map<int, vector<float>* > fatjet_SPLIT23;
  map<int, vector<float>* > fatjet_ZCUT12;
  map<int, vector<float>* > fatjet_ZCUT23;
  map<int, vector<float>* > fatjet_WIDTH;
  map<int, vector<float>* > fatjet_PlanarFlow;
  map<int, vector<float>* > fatjet_Angularity;
  map<int, vector<float>* > fatjet_PullMag;
  map<int, vector<float>* > fatjet_PullPhi;
  map<int, vector<float>* > fatjet_Tau1;
  map<int, vector<float>* > fatjet_Tau2;
  map<int, vector<float>* > fatjet_Tau3;
  map<int, vector<int>* > fatjet_nbtag;
  map<int, vector<int>* > fatjet_nbtag_geom;
  map<int, vector<int>* > fatjet_MCclass;
  map<int, vector<int>* > fatjet_orig_ind;
  map<int, vector<int>* > fatjet_assoc_ind;

  map<int, vector<int>* > fatjet_constit_n;
  map<int, vector<vector<int> >* > fatjet_constit_index;

  //map<int, vector<int>* > fatjet_CamKt12LCTopo_SplitFilt_n;
  //map<int, vector<vector<int> >* > fatjet_CamKt12LCTopo_SplitFilt_index;

  //map<int, vector<int>* > jet_CamKt12LCTopo_SplitFilteredmassFraction67minSplitR0_n;
  //map<int, vector<vector<int> >* > jet_CamKt12LCTopo_SplitFilteredmassFraction67minSplitR0_index;
  

  Int_t           jet_n;
  vector<float>*         jet_pt; 
  vector<float>*         jet_eta;
  vector<float>*         jet_phi;
  vector<float>*         jet_E;  
  vector<float>*         jet_jvf; 
  vector<int>*           jet_trueflav; 
  vector<float>*         jet_MV1; 
  vector<float>*     jet_nTrk_pv0_1GeV;
  vector<float>*   jet_trackWIDTH_pv0_1GeV;
  vector<int>*  jet_constit_n;
  vector<vector<int> >*  jet_constit_index;
  
  UInt_t          larError;
  UInt_t          tileError;
  UInt_t          coreFlags;

  UInt_t           runNumber;
  UInt_t           eventNumber;
  UInt_t           channelNumber;
  Float_t         mu;
  Int_t           vxp_n;
  vector<int>*    vxp_nTracks;
  vector<int>*    vxp_type;

  Bool_t          EF_mu24i_tight;
  Bool_t          EF_mu36_tight;
  Bool_t          EF_e24vhi_medium1;
  Bool_t          EF_e60_medium1;

  Float_t         lep_pt;
  Float_t         lep_eta;
  Float_t         lep_phi;
  Float_t         lep_E;
  Float_t         lep_m;
  Float_t         lep_charge;
  int             lep_ind;

  Float_t MET_et;
  Float_t MET_phi;


  ///////

   Int_t           el_n;
   vector<float>   *el_E;
   vector<float>   *el_Et;
   vector<float>   *el_pt;
   vector<float>   *el_m;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<float>   *el_charge;
   vector<int>     *el_author;
   vector<int>     *el_tightPP;
   vector<float>   *el_trackz0pvunbiased;
   vector<float>   *el_tracketa;
   vector<float>   *el_trackphi;
   vector<float>   *el_cl_E;
   vector<float>   *el_cl_pt;
   vector<float>   *el_cl_eta;
   vector<float>   *el_cl_phi;
   vector<unsigned int> *el_OQ;
   vector<float>   *el_etap;
   vector<float>   *el_etas2;
   vector<float>   *el_MI10_max40_ptsum;


   Int_t           mu_n;
   vector<float>   *mu_E;
   vector<float>   *mu_pt;
   vector<float>   *mu_m;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_charge;
   vector<int>     *mu_author;
   vector<int>     *mu_tight;
   vector<int>     *mu_nPixHits;
   vector<int>     *mu_nSCTHits;
   vector<int>     *mu_nTRTHits;
   vector<int>     *mu_nPixHoles;
   vector<int>     *mu_nSCTHoles;
   vector<int>     *mu_nTRTOutliers;
   vector<int>     *mu_nPixelDeadSensors;
   vector<int>     *mu_nSCTDeadSensors;
   vector<float>   *mu_id_z0_exPV;
   vector<float>   *mu_MI10_max40_ptsum;

   /////////////////////////////////////////////

   //Topoclusters
   Int_t           cl_n;
   vector<float>   *cl_pt;
   vector<float>   *cl_eta;
   vector<float>   *cl_phi;
   vector<float>   *cl_centermag;

   //Topoclusters
   Int_t           cell_n;
   vector<float>   *cell_pt;
   vector<float>   *cell_eta;
   vector<float>   *cell_phi;
   vector<float>   *cell_E;

   /*
   //minij (micorjet inputs for jet clustering)
   Int_t           minij_n;
   vector<float>   *minij_pt;
   vector<float>   *minij_eta;
   vector<float>   *minij_phi;
   vector<float>   *minij_centermag;
   */

   //Tracks
   Int_t           trk_n;
   vector<float>   *trk_pt;
   vector<float>   *trk_eta;
   vector<float>   *trk_d0_wrtPV;
   vector<float>   *trk_z0_wrtPV;
   vector<float>   *trk_phi_wrtPV;
   vector<float>   *trk_theta_wrtPV;
   vector<float>   *trk_qoverp_wrtPV;
   vector<float>   *trk_chi2;
   vector<int>     *trk_ndof;
   vector<int>     *trk_nPixHits;
   vector<int>     *trk_nSCTHits;


 
  //MC particles
  vector<vector<double> >*         mcevt_weight;
  vector<double>*        mcevt_pdf1;
  vector<double>*        mcevt_pdf2;
  vector<int>*           mcevt_pdf_id1;
  vector<int>*           mcevt_pdf_id2;
  vector<double>*        mcevt_pdf_scale;
  vector<double>*        mcevt_pdf_x1;
  vector<double>*        mcevt_pdf_x2;

  Int_t           mc_n;
  vector<float>   *mc_E;
  vector<float>   *mc_pt;
  vector<float>   *mc_eta;
  vector<float>   *mc_phi;
  vector<float>   *mc_m;
  vector<int>     *mc_status;
  vector<int>     *mc_barcode;
  vector<int>     *mc_pdgId;
  vector<float>   *mc_charge;
  vector<vector<int> > *mc_parent_index;
  vector<vector<int> > *mc_child_index;
  

  Int_t           trig_EF_el_n;
  vector<float>   *trig_EF_el_eta;
  vector<float>   *trig_EF_el_phi;
  vector<int>     *trig_EF_el_EF_e24vhi_medium1;
  vector<int>     *trig_EF_el_EF_e60_medium1;
  vector<int>     *trig_EF_trigmuonef_EF_mu24i_tight;
  vector<int>     *trig_EF_trigmuonef_EF_mu36_tight;
  
  Int_t           trig_EF_trigmuonef_n;
  vector<int>     *trig_EF_trigmuonef_track_n;
  vector<vector<float> > *trig_EF_trigmuonef_track_CB_pt;
  vector<vector<float> > *trig_EF_trigmuonef_track_CB_eta;
  vector<vector<float> > *trig_EF_trigmuonef_track_CB_phi;

  //Branches
  //Fat jets
  map<int, TBranch*> b_fatjet_n;
  map<int, TBranch*> b_fatjet_pt;
  map<int, TBranch*> b_fatjet_eta;
  map<int, TBranch*> b_fatjet_phi;
  map<int, TBranch*> b_fatjet_m;
  map<int, TBranch*> b_fatjet_constscale_pt;
  map<int, TBranch*> b_fatjet_constscale_eta;
  map<int, TBranch*> b_fatjet_constscale_phi;
  map<int, TBranch*> b_fatjet_constscale_m;
  map<int, TBranch*> b_fatjet_ActiveArea;
  map<int, TBranch*> b_fatjet_ActiveArea_px;
  map<int, TBranch*> b_fatjet_ActiveArea_py;
  map<int, TBranch*> b_fatjet_ActiveArea_pz;
  map<int, TBranch*> b_fatjet_ActiveArea_E;

  map<int, TBranch*> b_fatjet_SPLIT12;
  map<int, TBranch*> b_fatjet_SPLIT23;
  map<int, TBranch*> b_fatjet_ZCUT12;
  map<int, TBranch*> b_fatjet_ZCUT23;
  map<int, TBranch*> b_fatjet_WIDTH;
  map<int, TBranch*> b_fatjet_PlanarFlow;
  map<int, TBranch*> b_fatjet_Angularity;
  map<int, TBranch*> b_fatjet_PullMag;
  map<int, TBranch*> b_fatjet_PullPhi;

  map<int, TBranch*> b_fatjet_Tau1;
  map<int, TBranch*> b_fatjet_Tau2;
  map<int, TBranch*> b_fatjet_Tau3;
  map<int, TBranch*> b_fatjet_nbtag;
  map<int, TBranch*> b_fatjet_nbtag_geom;
  map<int, TBranch*> b_fatjet_MCclass;
  map<int, TBranch*> b_fatjet_orig_ind;
  map<int, TBranch*> b_fatjet_assoc_ind;

  map<int, TBranch*> b_fatjet_constit_n;
  map<int, TBranch*> b_fatjet_constit_index;

  //Jet
   TBranch        *b_jet_n;   
   TBranch        *b_jet_pt;   
   TBranch        *b_jet_eta;   
   TBranch        *b_jet_phi;   
   TBranch        *b_jet_E;   
   TBranch        *b_jet_jvf;   
   TBranch        *b_jet_trueflav;   
   TBranch        *b_jet_MV1;   
   TBranch        *b_jet_MV1c;   
   TBranch        *b_jet_nTrk_pv0_1GeV;   
   TBranch        *b_jet_trackWIDTH_pv0_1GeV;   
  
   TBranch        *b_jet_constit_n;   
   TBranch        *b_jet_constit_index;   


   //   TBranch   *b_el_n;
   //TBranch   *b_el_E;
   //TBranch   *b_el_pt;
   //TBranch   *b_el_m;
   //TBranch   *b_el_eta;
   //TBranch   *b_el_phi;
   //TBranch   *b_el_charge;

   TBranch   *b_el_Et;
   TBranch   *b_el_author;
   TBranch   *b_el_tightPP;
   TBranch   *b_el_trackz0pvunbiased;
   TBranch   *b_el_tracketa;
   TBranch   *b_el_trackphi;
   TBranch   *b_el_cl_E;
   TBranch   *b_el_cl_pt;
   TBranch   *b_el_cl_eta;
   TBranch   *b_el_cl_phi;
   TBranch   *b_el_OQ;
   TBranch   *b_el_etap;
   TBranch   *b_el_etas2;
   TBranch   *b_el_MI10_max40_ptsum;


   //TBranch   *b_mu_n;
   //TBranch   *b_mu_E;
   //TBranch   *b_mu_pt;
   //TBranch   *b_mu_m;
   // TBranch   *b_mu_eta;
   //TBranch   *b_mu_phi;
   //TBranch   *b_mu_charge;
   TBranch   *b_mu_author;
   TBranch   *b_mu_tight;
   TBranch   *b_mu_nPixHits;
   TBranch   *b_mu_nSCTHits;
   TBranch   *b_mu_nTRTHits;
   TBranch   *b_mu_nPixHoles;
   TBranch   *b_mu_nSCTHoles;
   TBranch   *b_mu_nTRTOutliers;
   TBranch   *b_mu_nPixelDeadSensors;
   TBranch   *b_mu_nSCTDeadSensors;
   TBranch   *b_mu_id_z0_exPV;
   TBranch   *b_mu_MI10_max40_ptsum;


   TBranch        *b_larError;   
   TBranch        *b_tileError;   
   TBranch        *b_coreFlags;   

   TBranch        *b_runNumber;   
   TBranch        *b_eventNumber;   
   TBranch        *b_channelNumber;   
   TBranch        *b_mu;   
   TBranch        *b_vxp_n;   
   TBranch        *b_vxp_nTracks;   
   TBranch        *b_vxp_type;   

   TBranch          *b_EF_mu24i_tight;
   TBranch          *b_EF_mu36_tight;
   TBranch          *b_EF_e24vhi_medium1;
   TBranch          *b_EF_e60_medium1;

   
   TBranch        *b_lep_pt;   
   TBranch        *b_lep_eta;   
   TBranch        *b_lep_phi;   
   TBranch        *b_lep_E;   
   TBranch        *b_lep_charge;   

   TBranch* b_MET_et;
   TBranch* b_MET_phi;


   TBranch        *b_mc_n;   
   TBranch        *b_mc_E;   
   TBranch        *b_mc_pt;   
   TBranch        *b_mc_eta;   
   TBranch        *b_mc_phi;   
   TBranch        *b_mc_m;   
   TBranch        *b_mc_status;   
   TBranch        *b_mc_barcode;   
   TBranch        *b_mc_pdgId;   
   TBranch        *b_mc_charge;   
   TBranch        *b_mc_parent_index;   
   TBranch        *b_mc_child_index;   


   TBranch        *b_mcevt_weight;   
   TBranch        *b_mcevt_pdf1;   
   TBranch        *b_mcevt_pdf2;   
   TBranch        *b_mcevt_pdf_id1;   
   TBranch        *b_mcevt_pdf_id2;   
   TBranch        *b_mcevt_pdf_scale;   
   TBranch        *b_mcevt_pdf_x1;   
   TBranch        *b_mcevt_pdf_x2;   

   TBranch        *b_cl_n;   
   TBranch        *b_cl_pt;   
   TBranch        *b_cl_eta;   
   TBranch        *b_cl_phi;   
   TBranch        *b_cl_centermag;   


   TBranch   *b_trk_n;   
   TBranch   *b_trk_pt;   
   TBranch   *b_trk_eta;   
   TBranch   *b_trk_d0_wrtPV;   
   TBranch   *b_trk_z0_wrtPV;   
   TBranch   *b_trk_phi_wrtPV;   
   TBranch   *b_trk_theta_wrtPV;   
   TBranch   *b_trk_qoverp_wrtPV;   
   TBranch   *b_trk_chi2;   
   TBranch   *b_trk_ndof;  
   TBranch   *b_trk_nPixHits;  
   TBranch   *b_trk_nSCTHits;  




   TBranch        *b_trig_EF_el_n;   
   TBranch        *b_trig_EF_el_eta;   
   TBranch        *b_trig_EF_el_phi;   
   TBranch        *b_trig_EF_el_EF_e24vhi_medium1;   
   TBranch        *b_trig_EF_el_EF_e60_medium1;   
   TBranch        *b_trig_EF_trigmuonef_EF_mu24i_tight;
   TBranch        *b_trig_EF_trigmuonef_EF_mu36_tight;

   TBranch        *b_trig_EF_trigmuonef_n;
   TBranch        *b_trig_EF_trigmuonef_track_n;
   TBranch        *b_trig_EF_trigmuonef_track_CB_pt;
   TBranch        *b_trig_EF_trigmuonef_track_CB_eta;
   TBranch        *b_trig_EF_trigmuonef_track_CB_phi;


  //00000000000000000000000000000000000000000000000000000000000
  //OUTPUT TREE BRANCHES
  //Parton branches
  double t_p_leptop_pt; double t_p_leptop_eta; double t_p_leptop_phi; double t_p_leptop_m;
  double t_p_hadtop_pt; double t_p_hadtop_eta; double t_p_hadtop_phi; double t_p_hadtop_m;
  double t_p_higgs_pt; double t_p_higgs_eta; double t_p_higgs_phi; double t_p_higgs_m;

  double t_p_leptop_hard_pt; double t_p_leptop_hard_eta; double t_p_leptop_hard_phi; double t_p_leptop_hard_m;
  double t_p_hadtop_hard_pt; double t_p_hadtop_hard_eta; double t_p_hadtop_hard_phi; double t_p_hadtop_hard_m;
  double t_p_higgs_hard_pt; double t_p_higgs_hard_eta; double t_p_higgs_hard_phi; double t_p_higgs_hard_m;

  double t_p_leptop_b_pt; double t_p_leptop_b_eta; double t_p_leptop_b_phi; double t_p_leptop_b_m;
  double t_p_leptop_W_pt; double t_p_leptop_W_eta; double t_p_leptop_W_phi; double t_p_leptop_W_m;
  double t_p_leptop_lep_pt; double t_p_leptop_lep_eta; double t_p_leptop_lep_phi; double t_p_leptop_lep_m; int t_p_leptop_lep_flav;
  double t_p_leptop_nu_pt; double t_p_leptop_nu_eta; double t_p_leptop_nu_phi; double t_p_leptop_nu_m;

  double t_p_hadtop_b_pt; double t_p_hadtop_b_eta; double t_p_hadtop_b_phi; double t_p_hadtop_b_m;
  double t_p_hadtop_W_pt; double t_p_hadtop_W_eta; double t_p_hadtop_W_phi; double t_p_hadtop_W_m;
  double t_p_hadtop_q1_pt; double t_p_hadtop_q1_eta; double t_p_hadtop_q1_phi; double t_p_hadtop_q1_m; int t_p_hadtop_q1_flav;
  double t_p_hadtop_q2_pt; double t_p_hadtop_q2_eta; double t_p_hadtop_q2_phi; double t_p_hadtop_q2_m; int t_p_hadtop_q2_flav;

  double t_p_higgs_b1_pt; double t_p_higgs_b1_eta; double t_p_higgs_b1_phi; double t_p_higgs_b1_m;
  double t_p_higgs_b2_pt; double t_p_higgs_b2_eta; double t_p_higgs_b2_phi; double t_p_higgs_b2_m;

  //Jet branches
  int t_j_leptop_b_ind;
  double t_j_leptop_lep_pt; double t_j_leptop_lep_eta; double t_j_leptop_lep_phi; double t_j_leptop_lep_m;
  int t_j_leptop_lep_flav; double t_j_leptop_lep_charge;
  double t_j_leptop_nu_pt; double t_j_leptop_nu_phi;
   
  int t_j_hadtop_b_ind;
  int t_j_hadtop_q1_ind;
  int t_j_hadtop_q2_ind;

  int t_j_higgs_b1_ind;
  int t_j_higgs_b2_ind;

  double t_pj_leptop_b_dr; 
  double t_pj_leptop_lep_dr; 
  double t_pj_leptop_nu_dphi; 

  double t_pj_hadtop_b_dr; 
  double t_pj_hadtop_q1_dr; 
  double t_pj_hadtop_q2_dr; 

  double t_pj_higgs_b1_dr; 
  double t_pj_higgs_b2_dr; 


  int t_j_hadW_cand_ind;
  int t_j_hadtop_nolepb_cand_ind;
  int t_j_hadtop_truth_cand_ind;
  int t_j_hadtop_Wbhtt_cand_ind;
  int t_j_hadtop_Wbtrim_cand_ind;

  //vectors containing selected jets 
  vector<float>* t_jets_pt;
  vector<float>* t_jets_eta;
  vector<float>* t_jets_phi;
  vector<float>* t_jets_m;
  vector<float>* t_jets_MV1;
  vector<int>* t_jets_ntrack;
  vector<float>* t_jets_trackwidth;
  vector<float>* t_jets_charge;
  /////NEEDED FOR TRF COMPUTATION////
  vector<float>* t_jets_trueflavor;
  //vectors containing selected fatjets
  map<int, int> t_fatjets_n;
  map<int, vector<float>* > t_fatjets_pt;
  map<int, vector<float>* > t_fatjets_eta;
  map<int, vector<float>* > t_fatjets_phi;
  map<int, vector<float>* > t_fatjets_m;
  map<int, vector<float>* > t_fatjets_constscale_pt;
  map<int, vector<float>* > t_fatjets_constscale_eta;
  map<int, vector<float>* > t_fatjets_constscale_phi;
  map<int, vector<float>* > t_fatjets_constscale_m;
  map<int, vector<float>* > t_fatjets_ActiveArea;
  map<int, vector<float>* > t_fatjets_ActiveArea_px;
  map<int, vector<float>* > t_fatjets_ActiveArea_py;
  map<int, vector<float>* > t_fatjets_ActiveArea_pz;
  map<int, vector<float>* > t_fatjets_ActiveArea_E;

  map<int, vector<float>* > t_fatjets_SPLIT12;
  map<int, vector<float>* > t_fatjets_SPLIT23;
  map<int, vector<float>* > t_fatjets_ZCUT12;
  map<int, vector<float>* > t_fatjets_ZCUT23;
  map<int, vector<float>* > t_fatjets_WIDTH;
  map<int, vector<float>* > t_fatjets_PlanarFlow;
  map<int, vector<float>* > t_fatjets_Angularity;
  map<int, vector<float>* > t_fatjets_PullMag;
  map<int, vector<float>* > t_fatjets_PullPhi;
  map<int, vector<float>* > t_fatjets_Tau1;
  map<int, vector<float>* > t_fatjets_Tau2;
  map<int, vector<float>* > t_fatjets_Tau3;
  map<int, vector<int>* > t_fatjets_nbtag;
  map<int, vector<int>* > t_fatjets_nbtag_geom;
  map<int, vector<int>* > t_fatjets_MCclass;
  map<int, vector<int>* > t_fatjets_orig_ind;
  map<int, vector<int>* > t_fatjets_assoc_ind;
  map<int, vector<int>* > t_fatjets_truth_label;

  double t_Eventshape_rhoKt4LC;
  double t_Eventshape_rhoKt6LC;
  int t_nvtx;
  double t_mu;
  double t_mcevt_weight;
  double t_RWPDFWeight;
  int t_trig_pass_el;
  int t_trig_pass_mu;
  int t_trig_match_el;
  int t_trig_match_mu;
  int t_HF_class;


  //00000000000000000000000000000000000000000000000000000000000
  void BookTH1D(const char* name, const char* title, double binsize, double xlow, double xup,
		const char* xtitle="", const char* ytitle="", int lw=2, int lc=1);

  void BookTH1D(const char* name, const char* title, int nbins, double* xedges,
		const char* xtitle="", const char* ytitle="", int lw=2, int lc=1);

  void BookTH2D(const char* name, const char* title, double xbinsize, double xlow, double xup,
		double ybinsize, double ylow, double yup,
		const char* xtitle="", const char* ytitle="", int lw=2, int lc=1);

  void BookTH2D(const char* name, const char* title,
		int nxbins, double* xedges, int nybins, double* yedges,
		const char* xtitle="", const char* ytitle="", int lw=2, int lc=1);

  void BookTH2D(const char* name, const char* title, double xbinsize, double xlow, double xup,
		int nybins, double* yedges,
		const char* xtitle="", const char* ytitle="", int lw=2, int lc=1);

  void FillTH1D(string hname, double val);
  void FillTH2D(string hname, double val1, double val2);
  //00000000000000000000000000000000000000000000000000000000000

  bool m_is_ID_top(int ID){ return (abs(ID) == 6);}
  bool m_is_ID_higgs(int ID){ return (abs(ID) == 9000006);}
  bool m_is_ID_W(int ID){ return (abs(ID) == 24);}
  bool m_is_ID_gluon(int ID){ return (abs(ID) == 9 || abs(ID) == 21);}
  bool m_is_ID_el(int ID){ return (abs(ID) == 11);}
  bool m_is_ID_mu(int ID){ return (abs(ID) == 13);}
  bool m_is_ID_tau(int ID){ return (abs(ID) == 15);}
  bool m_is_ID_lep(int ID){return (m_is_ID_el(ID) || m_is_ID_mu(ID) || m_is_ID_tau(ID));}
  bool m_is_ID_nu(int ID){ return (abs(ID) == 12 || abs(ID) == 14 || abs(ID) == 16);}
  bool m_is_ID_lq(int ID){ return (abs(ID) >= 1 && abs(ID) <= 4 );}
  bool m_is_ID_b(int ID){ return (abs(ID) == 5);}
  bool m_is_ID_q(int ID){return (m_is_ID_lq(ID) || m_is_ID_b(ID) || m_is_ID_top(ID));}

  bool m_is_indx_top(int indx){ return m_is_ID_top(mc_pdgId->at(indx));}
  bool m_is_indx_higgs(int indx){ return m_is_ID_higgs(mc_pdgId->at(indx));}
  bool m_is_indx_W(int indx){ return m_is_ID_W(mc_pdgId->at(indx));}
  bool m_is_indx_gluon(int indx){ return m_is_ID_gluon(mc_pdgId->at(indx));}
  bool m_is_indx_el(int indx){ return m_is_ID_el(mc_pdgId->at(indx));}
  bool m_is_indx_mu(int indx){ return m_is_ID_mu(mc_pdgId->at(indx));}
  bool m_is_indx_tau(int indx){ return m_is_ID_tau(mc_pdgId->at(indx));}
  bool m_is_indx_lep(int indx){return m_is_ID_lep(mc_pdgId->at(indx));}
  bool m_is_indx_nu(int indx){ return m_is_ID_nu(mc_pdgId->at(indx));}
  bool m_is_indx_lq(int indx){ return m_is_ID_lq(mc_pdgId->at(indx));}
  bool m_is_indx_b(int indx){ return m_is_ID_b(mc_pdgId->at(indx));}
  bool m_is_indx_q(int indx){ return m_is_ID_q(mc_pdgId->at(indx));}

  bool m_is_indx_leptop(int indx);
  bool m_is_indx_hadtop(int indx);
  bool m_is_indx_lepW(int indx);
  bool m_is_indx_hadW(int indx);
  bool m_is_indx_Hbb(int indx);

  //00000000000000000000000000000000000000
  bool m_is_stat_top(int ID, int i_gen);
  bool m_is_stat_higgs(int ID, int i_gen);
  bool m_is_stat_W(int ID, int i_gen);
  bool m_is_stat_lep(int ID, int i_gen);
  bool m_is_stat_nu(int ID, int i_gen);
  bool m_is_stat_lq(int ID, int i_gen);
  bool m_is_stat_b(int ID, int i_gen);

  bool m_is_indx_stat_top(int indx, int i_gen);
  bool m_is_indx_stat_higgs(int indx, int i_gen);
  bool m_is_indx_stat_W(int indx, int i_gen);
  bool m_is_indx_stat_lep(int indx, int i_gen);
  bool m_is_indx_stat_nu(int indx, int i_gen);
  bool m_is_indx_stat_lq(int indx, int i_gen);
  bool m_is_indx_stat_b(int indx, int i_gen);

  //
  bool m_is_stat_hard_top(int ID, int i_gen);
  bool m_is_stat_hard_higgs(int ID, int i_gen);

  bool m_is_indx_stat_hard_top(int indx, int i_gen);
  bool m_is_indx_stat_hard_higgs(int indx, int i_gen);



  //00000000000000000000000000000000000000000000000000000000000

  int InitEventBranches();
  int InitLeptonBranches();
  int InitMETBranches();
  int InitJetBranches(string sj);
  int InitFatJetBranches(int sfj_key);

  int InitEventOutputBranches();
  int InitPartonOutputBranches();
  int InitJetOutputBranches();
  int InitFatJetOutputBranches(int sfj_key);

  int InitPartonHistograms();
  int InitJetHistograms();
  int InitCutFlowHistograms();

  void MCParticlesDump();
  void MakeMCBarcodeMap();
  int MatchPartonJets();
  
  int FillPartonTLVs();
  int ClearEventTLVs();
  
  int FillPartonOutputBranches();
  int FillLeptonOutputBranches();
  int FillJetOutputBranches();
  int FillMatchedJetOutputBranches();
  
  int FillPartonHistograms();
  int FillJetHistograms();
  int FillMatchedJetHistograms();
  
  void FillCells();

  int JetSelection();
  int FatJetSelection(int sfj_key);

  float calculateJetCharge(int ind);
  //double EventPileupDensity(double radius);
  // double CalcRWPDFWeight();
  int ClassifyMCJets(vector<int> part_ind); 
  int MakeJetCollection(string s_rebuild, vector<int> v_sj_key, double fj_ptmin, double fj_etamax, double fj_ptmin_groom
			, bool doCalib=false, bool callSubstruct=true, bool ghost_match_mc=false
			, bool ghost_match_b=false, float JESsyst=1.,vector<PseudoJet>* vp_inputs=NULL, int i_excl=0);

 
  int MakePregroomingInputCollection(string s_pgcol);
  vector<PseudoJet> FillJetInputCollection( int b_pregroom, int input_type, int signal_type, vector<int>* vp_veto=NULL );

  double kT_Split(const PseudoJet &jet, int degree);
  double kT_Split(vector<PseudoJet> constits, int degree);
  double kT_Split(const ClusterSequence &cs, int degree);

  double kT_Zcut(const PseudoJet &jet, int degree);
  double kT_Zcut(vector<PseudoJet> constits, double mass, int degree);
  double kT_Zcut(const ClusterSequence &cs, double mass, int degree);
  double kT_Zcut(double split, double mass);

  double Angularity(const PseudoJet &jet);
  double PlanarFlow(const PseudoJet &jet);
  double Width(const PseudoJet &jet);

  double Angularity(vector<PseudoJet> constits);
  double PlanarFlow(vector<PseudoJet> constits);
  double Width(vector<PseudoJet> constits);

  double Tau(const PseudoJet &jet, int degree, double jet_rad);
  double Tau(vector<PseudoJet> constits, int degree, double jet_rad);
  double Tau(vector<PseudoJet> constits, const ClusterSequence &cs, int degree, double jet_rad);

  bool IsLepMatchedToGen(int i_gen);
  int FindMatchedJetToGen(int i_gen);
  int FindMatchedFatJetToGen(int i_gen, int fj_ID);
  bool IsLepMatchedToGen(TLorentzVector* tlv_p);
  int FindMatchedJetToGen(TLorentzVector* tlv_p);
  int FindMatchedFatJetToGen(TLorentzVector* tlv_p, int fj_ID);
  int AssignGenPartIndx(int i_gen);

  int LeptonSelection();
  int OneLeptonSelection();





  int MatchRecoElectronToTruth(TLorentzVector tlv_p_lep, double drcut=0.15);
  int MatchRecoMuonToTruth(TLorentzVector tlv_p_lep, double drcut=0.15);
  int SteerTruthMatchLep();


  int ElectronCutFlow(int ind, string s_hist="el_lepsel_cutflow_hist");
  int MuonCutFlow(int ind, string s_hist="mu_lepsel_cutflow_hist");

  int ElectronSelection();
  int MuonSelection();

  int ElectronJetOverlapRemoval();
  int RemoveElectronWithJetOverlap();
  int RemoveMuonWithJetOverlap();

  bool MatchElectronTrigger(int ind);
  bool MatchMuonTrigger(int ind);
  bool ef_mu24Hypo(float eta, float pt);
  bool ef_mu36Hypo(float eta, float pt);


  int ReconstructEvent(int i_opt); 

  //static void PyDelClusterSequenceTA(void*);
  
  //static PyObject *globalInclCsTA;
  //static PyObject *globalExclCsTA;
    
  //static vector<PseudoJet> inclCsFunctionTA(ClusterSequenceArea*);
  //static vector<PseudoJet> exclCsFunctionTA(ClusterSequenceArea*, int);

  //PyObject* pName;
  //PyObject* pModule;  
  //PyObject* setRhoMuNvertex;

  /*
  map<string, int> m_int_branchnames;
  map<string, double> m_doble_branchnames;
  map<string, float> m_float_branchnames;

  map<string, vector<int> > m_vecint_branchnames
  map<string, vector<double> > m_vecdouble_branchnames;
  map<string, vector<float> > m_vecfloat_branchnames;
  */

  
  void JetTruthFlavorPJ(vector<PseudoJet> jets,vector<int> *jet_truth_flavor,float DR);
  void JetTruthFlavor(vector<float> *jets_pt,vector<float> *jets_eta,vector<float> *jets_phi,vector<float> *jets_E,vector<int> *jet_truth_flavor,float DR);
  void JetTruthFlavorM(vector<float> *jets_pt,vector<float> *jets_eta,vector<float> *jets_phi,vector<float> *jets_M,vector<int> *jet_truth_flavor,float DR);


  ////ClassifyHF stuff///
  GeneratorInfo::input_str td;
  void SetTopData(GeneratorInfo::input_str *td);




  ////////LEPTON SELECTION/////
  
  int TruthElectronSelection();
  int TruthMuonSelection();
  int TruthElectronJetOverlapRemoval(int sfj_key,vector<int> &jettoremove);
  int RemoveTruthElectronWithJetOverlap(int sfj_key);
  int RemoveTruthMuonWithJetOverlap(int sfj_key);
  vector<int> m_ankt2_e_overlap_ind; //indices of jets to be removed due to overlap with electron
  vector<int> m_ankt4_e_overlap_ind; //indices of jets to be removed due to overlap with electron
  

  void RecoMET();
  float m_reco_met_px;
  float m_reco_met_py;


};


#endif

