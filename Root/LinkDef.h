#include "EventRecoAnalysis/EventRecoAnalysis.h"


#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class vector<vector<int> >+;
#pragma link C++ class vector<vector<double> >+;
#pragma link C++ class vector<vector<float> >+;

#pragma link C++ class EventRecoAnalysis+;


#pragma link C++ nestedclass;

#endif
