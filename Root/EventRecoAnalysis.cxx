/*
  Trisha Farooque, 25.09.2014
  Sequential Event reconstruction
  Under construction
Modi

*/

#include "EventRecoAnalysis/EventRecoAnalysis.h"
#define ETALOW -5
#define ETAHIGH 5.
#define ETABINS 100
#define PHIBINS 63 
#define PIGREC 3.1415926536

using namespace std;
using namespace fastjet;

//PyObject* EventRecoAnalysis::globalInclCsTA=0;
//PyObject* EventRecoAnalysis::globalExclCsTA=0;
    

const float EventRecoAnalysis::GeV = 1000.;
const double EventRecoAnalysis::MASS_W = 80.4*1000.;
const double EventRecoAnalysis::MASS_TOP = 172.*1000.;

const float EventRecoAnalysis::m_jet_ptmin = 25.*1000.;
const float EventRecoAnalysis::m_jet_etamax = 2.5;
const float EventRecoAnalysis::m_jet_mv1cut = 0.7892; //70% WP
const float EventRecoAnalysis::m_fatjet_ptmin_tight = 50.*1000.;
const float EventRecoAnalysis::m_fatjet_ptmin_loose = 50.*1000.;
const float EventRecoAnalysis::m_fatjet_etamax = 6.;
const float EventRecoAnalysis::m_el_ptmin = 25.*1000.;
const float EventRecoAnalysis::m_el_etamax = 2.5;
const float EventRecoAnalysis::m_mu_ptmin = 25.*1000.;
const float EventRecoAnalysis::m_mu_etamax = 2.5;
const float EventRecoAnalysis::m_tau_ptmin = 5.*1000.;
const float EventRecoAnalysis::m_tau_etamax = 5.;

const float EventRecoAnalysis::m_lep_ptmin = 25.*1000.;
const float EventRecoAnalysis::m_lep_etamax = 2.5;

const float EventRecoAnalysis::m_e_j_or_dr = 0.2;
const float EventRecoAnalysis::m_j_e_or_dr = 0.4;
const float EventRecoAnalysis::m_j_mu_or_dr = 0.4;

const double EventRecoAnalysis::m_beam_nrg = 4.E6;
const double EventRecoAnalysis::m_beam_nrg_RW = 7.E6;

const double EventRecoAnalysis::m_ptbinsize = 25.; const double EventRecoAnalysis::m_ptbinmin = 20.; const double EventRecoAnalysis::m_ptbinmax = 1000.;
const double EventRecoAnalysis::m_mbinsize = 10.; const double EventRecoAnalysis::m_mbinmin = 0.; const double EventRecoAnalysis::m_mbinmax = 500.;
const double EventRecoAnalysis::m_etabinsize = 0.1; const double EventRecoAnalysis::m_etabinmin = -3.5; const double EventRecoAnalysis::m_etabinmax = 3.5;
const double EventRecoAnalysis::m_phibinsize = 0.1; const double EventRecoAnalysis::m_phibinmin = -3.5; const double EventRecoAnalysis::m_phibinmax = 3.5;

const double EventRecoAnalysis::m_rspnsbinsize = 0.02; const double EventRecoAnalysis::m_rspnsbinmin = 0.; const double EventRecoAnalysis::m_rspnsbinmax = 2.;
const double EventRecoAnalysis::m_drbinsize = 0.02; const double EventRecoAnalysis::m_drbinmin=0.; const double EventRecoAnalysis::m_drbinmax=5.;
const double EventRecoAnalysis::m_ssbinsize = 0.02; const double EventRecoAnalysis::m_ssbinmin=0.; const double EventRecoAnalysis::m_ssbinmax=5.;

const int EventRecoAnalysis::WHIST=0x1<<0;
const int EventRecoAnalysis::WTREE=0x1<<1;

const int EventRecoAnalysis::I_PYTHIA8 = 1;
const int EventRecoAnalysis::I_PYTHIA6 = 2;
const int EventRecoAnalysis::I_HERWIG = 3;
const int EventRecoAnalysis::I_MADPYTH = 4;


const int EventRecoAnalysis::ELE = 0;
const int EventRecoAnalysis::MUON = 1;
const int EventRecoAnalysis::TAU = 2;

const int EventRecoAnalysis::I_LEPTOP_B = 1;
const int EventRecoAnalysis::I_HADTOP_B = 2;
const int EventRecoAnalysis::I_HADTOP_Q1 = 3;
const int EventRecoAnalysis::I_HADTOP_Q2 = 4;
const int EventRecoAnalysis::I_HIGGS_B1 = 5;
const int EventRecoAnalysis::I_HIGGS_B2 = 6;
const int EventRecoAnalysis::I_EXTRA_HF = 7;

const int EventRecoAnalysis::MC_GHOST_SHIFT = 900;
const int EventRecoAnalysis::B_GHOST_SHIFT = 9000;

const int EventRecoAnalysis::I_1P0B = 100; 
const int EventRecoAnalysis::I_BL = 101; 
const int EventRecoAnalysis::I_1P1B = 102; 
const int EventRecoAnalysis::I_Q1Q2 = 103; 
const int EventRecoAnalysis::I_2P1BCOMB = 104; 
const int EventRecoAnalysis::I_2P1BPART = 105; 
const int EventRecoAnalysis::I_2P2BCOMB = 106; 
const int EventRecoAnalysis::I_B1B2 = 107;
const int EventRecoAnalysis::I_BHQ1Q2 = 108; 
const int EventRecoAnalysis::I_3P1BCOMB = 109; 
const int EventRecoAnalysis::I_3P2BCOMB = 110; 
const int EventRecoAnalysis::I_3P3BCOMB = 111; 
const int EventRecoAnalysis::I_GT3P = 112;
const int EventRecoAnalysis::I_NOP = 113;

const double EventRecoAnalysis::m_drmatch_l = 99.;
const double EventRecoAnalysis::m_drmatch_j = 99.;
const double EventRecoAnalysis::m_drmatch_fj = 99.;

const int EventRecoAnalysis::I_PREGROOM_INPUT = -1;
const int EventRecoAnalysis::inTOPO = 1;
const int EventRecoAnalysis::inTRK = 2;
const int EventRecoAnalysis::inTRUTH = 3;
const int EventRecoAnalysis::inMITRU = 4;

const int EventRecoAnalysis::rc_ak4_15 = 0x1 << 0;
//const int EventRecoAnalysis::rc_ak4_jvf5 = 0x1 << 1;
const int EventRecoAnalysis::rc_ak4_elj = 0x1 << 1;
const int EventRecoAnalysis::rc_el_cl = 0x1 << 2;
const int EventRecoAnalysis::rc_ak4_nonb = 0x1 << 3;
const int EventRecoAnalysis::rc_ak4_b = 0x1 << 4;
const int EventRecoAnalysis::rc_ak4_nolepb = 0x1 << 5;
const int EventRecoAnalysis::rc_ak4_vetolist = 0x1 << 6;


////Check 18/03////

const string EventRecoAnalysis::s_nolepb = "RebuildCamKt8MitruSplitFilt";
const string EventRecoAnalysis::s_nolepb_trim = "RebuildCamKt8MitruTrim";
const string EventRecoAnalysis::s_nolepb_akt = "RebuildAntiKt2Mitru";
const string EventRecoAnalysis::s_nolepb_akt4 = "RebuildAntiKt4Mitru";
const string EventRecoAnalysis::s_nolepb_R6 = "RebuildCamKt6MitruSplitFilt";
const string EventRecoAnalysis::s_nolepb_trim_R6 = "RebuildCamKt6MitruTrim";
const string EventRecoAnalysis::s_nolepb_R10 = "RebuildCamKt10MitruSplitFilt";
const string EventRecoAnalysis::s_nolepb_trim_R10 = "RebuildCamKt10MitruTrim";


//const string EventRecoAnalysis::s_vetoht_nolepb_bdrs = "RebuildCamKt8MitruVetoHadtopSplitFilt";

/*
const string EventRecoAnalysis::s_nolepb = "RebuildCamKt8TruthSplitFilt";
const string EventRecoAnalysis::s_nolepb_trim = "RebuildCamKt8TruthTrim";

const string EventRecoAnalysis::s_nolepb_akt = "RebuildAntiKt2Truth";
const string EventRecoAnalysis::s_nolepb_R6 = "RebuildCamKt6TruthSplitFilt";
const string EventRecoAnalysis::s_nolepb_trim_R6 = "RebuildCamKt6TruthTrim";
const string EventRecoAnalysis::s_nolepb_R10 = "RebuildCamKt10TruthSplitFilt";
const string EventRecoAnalysis::s_nolepb_trim_R10 = "RebuildCamKt10TruthTrim";
*/




const string EventRecoAnalysis::s_truth = "RebuildCamKt8TruthSplitFilt";
const string EventRecoAnalysis::s_truth_trim = "RebuildCamKt8TruthTrim";

const string EventRecoAnalysis::s_truth_akt = "RebuildAntiKt2Truth";
const string EventRecoAnalysis::s_truth_R6 = "RebuildCamKt6TruthSplitFilt";
const string EventRecoAnalysis::s_truth_trim_R6 = "RebuildCamKt6TruthTrim";
const string EventRecoAnalysis::s_truth_R10 = "RebuildCamKt10TruthSplitFilt";
const string EventRecoAnalysis::s_truth_trim_R10 = "RebuildCamKt10TruthTrim";

//const string EventRecoAnalysis::s_vetoht_truth_bdrs = "RebuildCamKt8TruthVetoHadtopSplitFilt";



const int EventRecoAnalysis::i_nolepb = 0;
const int EventRecoAnalysis::i_nolepb_trim = 1;

const int EventRecoAnalysis::i_nolepb_akt = 2;
const int EventRecoAnalysis::i_nolepb_R6 = 3;
const int EventRecoAnalysis::i_nolepb_trim_R6 = 4;
const int EventRecoAnalysis::i_nolepb_R10 = 5;
const int EventRecoAnalysis::i_nolepb_trim_R10 = 6;
//const int EventRecoAnalysis::i_vetoht_nolepb_bdrs = 3;
const int EventRecoAnalysis::i_nolepb_akt4 = 7;

const int EventRecoAnalysis::i_truth = 7;
const int EventRecoAnalysis::i_truth_trim = 8;

const int EventRecoAnalysis::i_truth_akt = 9;

const int EventRecoAnalysis::i_truth_R6 = 10;
const int EventRecoAnalysis::i_truth_trim_R6 = 11;

const int EventRecoAnalysis::i_truth_R10 = 12;
const int EventRecoAnalysis::i_truth_trim_R10 = 13;
//const int EventRecoAnalysis::i_vetoht_truth_bdrs = 7;


const int EventRecoAnalysis::sigLC = 1;
const int EventRecoAnalysis::sigEM = 0;

const double EventRecoAnalysis::kappa = 0.5;
const double EventRecoAnalysis::c_track_pt = 1000.;
const double EventRecoAnalysis::c_track_deltaR = 0.35;
const int EventRecoAnalysis::c_track_Nmin = 1;
const double EventRecoAnalysis::c_track_d0 = 2.;
const double EventRecoAnalysis::c_track_z0 = 10.;
const double EventRecoAnalysis::c_track_chi2 = 2.5;
const int EventRecoAnalysis::c_track_nPixHits = 1;
const int EventRecoAnalysis::c_track_nSCTHits = 1;


float EventRecoAnalysis::calculateJetCharge(int ind){
  float charge = -999.;
  TVector3 tlv_jet;
  tlv_jet.SetPtEtaPhi(jet_pt->at(ind), jet_eta->at(ind), jet_phi->at(ind));
  TVector3 jet_unit_vector = tlv_jet.Unit();
  //Loop over track container. 
  float qsum = 0.; float sum = 0.;
  for(int i = 0; i < trk_n; i++){
    //------------------------------------------
    //Track selection
    if(trk_pt->at(i) < c_track_pt) continue;

    if(trk_chi2->at(i) / ((float)trk_ndof->at(i)) > c_track_chi2) continue;

    if(fabs(trk_d0_wrtPV->at(i)) > c_track_d0) continue;

    if(fabs(trk_z0_wrtPV->at(i) * sin(trk_theta_wrtPV->at(i))) > c_track_z0) continue;

    if(trk_nPixHits->at(i) < c_track_nPixHits) continue;

    if(trk_nSCTHits->at(i) < c_track_nSCTHits) continue;
    //------------------------------------------

    //Match to jet
    TVector3 tlv_trk;
    tlv_trk.SetPtEtaPhi(trk_pt->at(i), trk_eta->at(i), trk_phi_wrtPV->at(i));
    if(tlv_jet.DeltaR(tlv_trk) > c_track_deltaR) continue;
    float psum = pow(fabs(tlv_trk * jet_unit_vector), kappa);
    sum += psum;
    float qcheck = trk_qoverp_wrtPV->at(i) * trk_pt->at(i);
    qsum += (qcheck > 0) ? psum : -psum;
  }

  if(sum > 0){ charge = qsum/sum; }

  return charge;

}

//public section
EventRecoAnalysis::EventRecoAnalysis(int ds_ID, vector<string> ds_fileloc, int b_io, string s_out, string s_tree,int nfirst, int nlast, int doJES){
  /*
   cout<<"Inside constructor 0 &ds_ID = "<<&ds_ID<<endl;
   cout<<"Inside constructor 0 &ds_fileloc = "<<&ds_fileloc<<endl;
   cout<<"Inside constructor 0 &s_out = "<<&s_out<<" s_out = "<<s_out<<"s_out.length() = "<<s_out.length()<<endl;
   cout<<"Inside constructor 0 &v_fatjetcols = "<<&v_fatjetcols<<endl;
   cout<<"Inside constructor 0 &s_jetcol = "<<&s_jetcol<<endl;
   cout<<"Inside constructor 0 &s_tree = "<<&s_tree<<endl;
   cout<<"Inside constructor 0 nfirst = "<<nfirst<<endl;
   cout<<"Inside constructor 0 nlast = "<<nlast<<endl;
  */
   cout<<"2 s_out.length() = "<<s_out.length()<<" s_out.capacity() = "<<s_out.capacity()<<endl;
 
  m_h1d.clear(); m_h2d.clear();
  m_fatjetcol_IDs.clear();

  m_stree.clear();
  m_fileloc.clear();
  m_ds_ID.clear();
  m_sout_hist.clear();
  m_sout_tree.clear();

  m_jetcol_ID.clear();

  m_jet_ind.clear();
  m_el_ind.clear();
  m_mu_ind.clear();
  m_jet_e_overlap_ind.clear();
  m_el_jet_overlap_ind.clear();
  m_mu_jet_overlap_ind.clear();



  ////////

  m_ankt2_e_overlap_ind.clear();
  m_ankt4_e_overlap_ind.clear();
  m_reco_met_px=0;
  m_reco_met_py=0;
  ////////

  m_chain = 0;
  m_outtree = 0;
  m_outfile_tree = 0;
  m_outfile_hist = 0;

  m_nfirst=nfirst;
  m_nlast=nlast;
  if(doJES==0){m_doJES=1.;}
  else if(doJES==1){m_doJES=1.05;}
  else if(doJES==-1){m_doJES=0.95;}
  
 
  m_nev = 0;
  m_ev_wgt = 0.;
  m_PDF_set = 0;
  //m_pdfTool = 0;
  //m_calib_Tool = 0;

  m_mc_bc_ind.clear();
  m_fatjet_ind.clear();

  //pModule = 0;
  //setRhoMuNvertex=0;

  m_p_leptop_lep_flav = -1;
  m_j_leptop_lep_flav = -1;

  m_tlv_p_top = 0;
  m_tlv_p_atop = 0;

  m_tlv_p_leptop = 0;
  m_tlv_p_hadtop = 0;

  m_tlv_p_higgs = 0;
  m_tlv_p_ttbar = 0;

  //
  m_tlv_p_top_hard = 0;
  m_tlv_p_atop_hard = 0;

  m_tlv_p_leptop_hard = 0;
  m_tlv_p_hadtop_hard = 0;

  m_tlv_p_higgs_hard = 0;
  m_tlv_p_ttbar_hard = 0;


  m_tlv_p_leptop_b = 0;
  m_tlv_p_leptop_W = 0;
  m_tlv_p_leptop_lep = 0;
  m_tlv_p_leptop_nu = 0;

  m_tlv_p_hadtop_b = 0;
  m_tlv_p_hadtop_W = 0;

  m_tlv_p_hadtop_q1 = 0;
  m_tlv_p_hadtop_q2 = 0;

  m_tlv_p_higgs_b1 = 0;
  m_tlv_p_higgs_b2 = 0;

  //00000000000000000000000000000000000000000000000000000000000000000000000000000
  //00000000000000000000000000000000000000000000000000000000000
  //Input branch list
  //Fat jets
  fatjet_n.clear();
  fatjet_pt.clear();
  fatjet_eta.clear();
  fatjet_phi.clear();
  fatjet_m.clear();
  fatjet_constscale_pt.clear();
  fatjet_constscale_eta.clear();
  fatjet_constscale_phi.clear();
  fatjet_constscale_m.clear();
  fatjet_ActiveArea.clear();
  fatjet_ActiveArea_px.clear();
  fatjet_ActiveArea_py.clear();
  fatjet_ActiveArea_pz.clear();
  fatjet_ActiveArea_E.clear();

  fatjet_SPLIT12.clear();
  fatjet_SPLIT23.clear();
  fatjet_ZCUT12.clear();
  fatjet_ZCUT23.clear();
  fatjet_WIDTH.clear();
  fatjet_PlanarFlow.clear();
  fatjet_Angularity.clear();
  fatjet_PullMag.clear();
  fatjet_PullPhi.clear();
  fatjet_Tau1.clear();
  fatjet_Tau2.clear();
  fatjet_Tau3.clear();
  fatjet_nbtag.clear();
  fatjet_nbtag_geom.clear();
  fatjet_MCclass.clear();
  fatjet_orig_ind.clear();
  fatjet_assoc_ind.clear();

  fatjet_constit_n.clear();
  fatjet_constit_index.clear();


  el_n = 0;
  el_E = 0;
  el_Et = 0;
  el_pt = 0;
  el_m = 0;
  el_eta = 0;
  el_phi = 0;
  el_charge = 0;
  el_author = 0;
  el_tightPP = 0;
  el_trackz0pvunbiased = 0;
  el_tracketa = 0;
  el_trackphi = 0;
  el_cl_E = 0;
  el_cl_pt = 0;
  el_cl_eta = 0;
  el_cl_phi = 0;
  el_OQ = 0;
  el_etap = 0;
  el_etas2 = 0;
  el_MI10_max40_ptsum = 0;


  mu_n = 0;
  mu_E = 0;
  mu_pt = 0;
  mu_m = 0;
  mu_eta = 0;
  mu_phi = 0;
  mu_charge = 0;
  mu_author = 0;
  mu_tight = 0;
  mu_nPixHits = 0;
  mu_nSCTHits = 0;
  mu_nTRTHits = 0;
  mu_nPixHoles = 0;
  mu_nSCTHoles = 0;
  mu_nTRTOutliers = 0;
  mu_nPixelDeadSensors = 0;
  mu_nSCTDeadSensors = 0;
  mu_id_z0_exPV = 0;
  mu_MI10_max40_ptsum = 0;

  jet_n = 0;
  jet_pt = 0; 
  jet_eta = 0; 
  jet_phi = 0; 
  jet_E = 0; 
  jet_jvf = 0; 
  jet_trueflav = 0; 
  jet_MV1 = 0;  
  jet_nTrk_pv0_1GeV = 0;
  jet_trackWIDTH_pv0_1GeV = 0;
  jet_constit_n = 0; 
  jet_constit_index = 0; 
  
  larError = 0;
  tileError = 0;
  coreFlags = 0;

  runNumber = 0;
  eventNumber = 0;
  channelNumber = 0;
  mu = 0;
  vxp_n = 0;
  vxp_nTracks = 0;
  vxp_type = 0;


  trig_EF_el_n = 0;  
  trig_EF_el_eta = 0; 
  trig_EF_el_phi = 0; 
  trig_EF_el_EF_e24vhi_medium1 = 0;
  trig_EF_el_EF_e60_medium1 = 0; 
  trig_EF_trigmuonef_EF_mu24i_tight = 0;
  trig_EF_trigmuonef_EF_mu36_tight = 0;

  trig_EF_trigmuonef_n = 0;
  trig_EF_trigmuonef_track_n = 0;
  trig_EF_trigmuonef_track_CB_pt = 0;
  trig_EF_trigmuonef_track_CB_eta = 0;
  trig_EF_trigmuonef_track_CB_phi = 0;

  lep_pt = 0;
  lep_eta = 0;
  lep_phi = 0;
  lep_E = 0;
  lep_charge = 0;
  lep_ind = -1;

  MET_et = 0;
  MET_phi = 0;


  //Topoclusters
  cl_n = 0;
  cl_pt = 0;
  cl_eta = 0;
  cl_phi = 0;
  cl_centermag = 0;

  //Cells
  cell_n = 0;
  cell_pt = 0;
  cell_eta = 0;
  cell_phi = 0;
  cell_E=0;
  /*
  minij_n = 0;
  minij_pt = 0;
  minij_eta = 0;
  minij_phi = 0;
  minij_centermag = 0;
  */
  //MC particles
  mcevt_weight = 0;
  mcevt_pdf1 = 0;
  mcevt_pdf2 = 0;
  mcevt_pdf_id1 = 0;
  mcevt_pdf_id2 = 0;
  mcevt_pdf_scale = 0;
  mcevt_pdf_x1 = 0;
  mcevt_pdf_x2 = 0;
  
  mc_n = 0;
  mc_E = 0;
  mc_pt = 0;
  mc_eta = 0;
  mc_phi = 0;
  mc_m = 0;
  mc_status = 0;
  mc_barcode = 0;
  mc_pdgId = 0;
  mc_charge = 0;
  mc_parent_index = 0;
  mc_child_index = 0;

  //00000000000000000000000000000000000000000000000000000000000000000000000000000
  b_fatjet_n.clear();
  b_fatjet_pt.clear();
  b_fatjet_eta.clear();
  b_fatjet_phi.clear();
  b_fatjet_m.clear();
  b_fatjet_constscale_pt.clear();
  b_fatjet_constscale_eta.clear();
  b_fatjet_constscale_phi.clear();
  b_fatjet_constscale_m.clear();
  b_fatjet_ActiveArea.clear();
  b_fatjet_ActiveArea_px.clear();
  b_fatjet_ActiveArea_py.clear();
  b_fatjet_ActiveArea_pz.clear();
  b_fatjet_ActiveArea_E.clear();

  b_fatjet_SPLIT12.clear();
  b_fatjet_SPLIT23.clear();
  b_fatjet_ZCUT12.clear();
  b_fatjet_ZCUT23.clear();
  b_fatjet_WIDTH.clear();
  b_fatjet_PlanarFlow.clear();
  b_fatjet_Angularity.clear();
  b_fatjet_PullMag.clear();
  b_fatjet_PullPhi.clear();

  b_fatjet_Tau1.clear();
  b_fatjet_Tau2.clear();
  b_fatjet_Tau3.clear();
  b_fatjet_nbtag.clear();
  b_fatjet_nbtag_geom.clear();
  b_fatjet_MCclass.clear();
  b_fatjet_orig_ind.clear();
  b_fatjet_assoc_ind.clear();

  b_fatjet_constit_n.clear();
  b_fatjet_constit_index.clear();


  /*
  b_el_n = 0;
  b_el_E = 0;
  b_el_pt = 0;
  b_el_m = 0;
  b_el_eta = 0;
  b_el_phi = 0;
  b_el_charge = 0;
  */
  b_el_Et = 0;
  b_el_author = 0;
  b_el_tightPP = 0;
  b_el_trackz0pvunbiased = 0;
  b_el_tracketa = 0;
  b_el_trackphi = 0;
  b_el_cl_E = 0;
  b_el_cl_pt = 0;
  b_el_cl_eta = 0;
  b_el_cl_phi = 0;
  b_el_OQ = 0;
  b_el_etap = 0;
  b_el_etas2 = 0;
  b_el_MI10_max40_ptsum = 0;

  /*
  b_mu_n = 0;
  b_mu_E = 0;
  b_mu_pt = 0;
  b_mu_m = 0;
  b_mu_eta = 0;
  b_mu_phi = 0;
  b_mu_charge = 0;
  */
  b_mu_author = 0;
  b_mu_tight = 0;
  b_mu_nPixHits = 0;
  b_mu_nSCTHits = 0;
  b_mu_nTRTHits = 0;
  b_mu_nPixHoles = 0;
  b_mu_nSCTHoles = 0;
  b_mu_nTRTOutliers = 0;
  b_mu_nPixelDeadSensors = 0;
  b_mu_nSCTDeadSensors = 0;
  b_mu_id_z0_exPV = 0;
  b_mu_MI10_max40_ptsum = 0;

  //Jet
  b_jet_n = 0; 
  b_jet_pt = 0;
  b_jet_eta = 0; 
  b_jet_phi = 0;
  b_jet_E = 0; 
  b_jet_jvf = 0; 
  b_jet_trueflav = 0;
  b_jet_MV1 = 0;
  b_jet_nTrk_pv0_1GeV = 0;
  b_jet_trackWIDTH_pv0_1GeV = 0;
  b_jet_constit_n = 0;  
  b_jet_constit_index = 0; 


  b_larError = 0;
  b_tileError = 0;
  b_coreFlags = 0;

  b_runNumber = 0; 
  b_eventNumber = 0; 
  b_channelNumber = 0;
  b_mu = 0; 
  b_vxp_n = 0;  
  b_vxp_nTracks = 0; 
  b_vxp_type = 0;

  b_EF_mu24i_tight = 0;
  b_EF_mu36_tight = 0;
  b_EF_e24vhi_medium1 = 0;
  b_EF_e60_medium1 = 0;

  b_lep_pt = 0;  
  b_lep_eta = 0; 
  b_lep_phi = 0; 
  b_lep_E = 0;  
  b_lep_charge = 0;  

  b_MET_et = 0;
  b_MET_phi = 0;


  b_mc_n = 0; 
  b_mc_E = 0; 
  b_mc_pt = 0;
  b_mc_eta = 0;
  b_mc_phi = 0;
  b_mc_m = 0; 
  b_mc_status = 0; 
  b_mc_barcode = 0; 
  b_mc_pdgId = 0;  
  b_mc_charge = 0;  
  b_mc_parent_index = 0; 
  b_mc_child_index = 0;  


  b_mcevt_weight = 0;  
  b_mcevt_pdf1 = 0;   
  b_mcevt_pdf2 = 0;   
  b_mcevt_pdf_id1 = 0;  
  b_mcevt_pdf_id2 = 0;   
  b_mcevt_pdf_scale = 0; 
  b_mcevt_pdf_x1 = 0;  
  b_mcevt_pdf_x2 = 0; 

  b_cl_n = 0;  
  b_cl_pt = 0; 
  b_cl_eta = 0;  
  b_cl_phi = 0;  
  b_cl_centermag = 0; 


  b_trig_EF_el_n = 0;  
  b_trig_EF_el_eta = 0; 
  b_trig_EF_el_phi = 0; 
  b_trig_EF_el_EF_e24vhi_medium1 = 0;  
  b_trig_EF_el_EF_e60_medium1 = 0;  
  b_trig_EF_trigmuonef_EF_mu24i_tight = 0;
  b_trig_EF_trigmuonef_EF_mu36_tight = 0;

  b_trig_EF_trigmuonef_n = 0;
  b_trig_EF_trigmuonef_track_n = 0;
  b_trig_EF_trigmuonef_track_CB_pt = 0;
  b_trig_EF_trigmuonef_track_CB_eta = 0;
  b_trig_EF_trigmuonef_track_CB_phi = 0;



  //00000000000000000000000000000000000000000000000000000000000000000000000000000
  t_Eventshape_rhoKt4LC = 0.;
  t_Eventshape_rhoKt6LC = 0.;
  t_nvtx = 0;
  t_mu = 0.;
  t_RWPDFWeight = 0.;
  t_HF_class=0;
  t_trig_pass_el = 0;
  t_trig_pass_mu = 0;
  t_trig_match_el = 0;
  t_trig_match_mu = 0;


  t_p_leptop_pt = -1.; t_p_leptop_eta = -1.; t_p_leptop_phi = -1.; t_p_leptop_m = -1.;
  t_p_hadtop_pt = -1.; t_p_hadtop_eta = -1.; t_p_hadtop_phi = -1.; t_p_hadtop_m = -1.;
  t_p_higgs_pt = -1.; t_p_higgs_eta = -1.; t_p_higgs_phi = -1.; t_p_higgs_m = -1.;

  t_p_leptop_hard_pt = -1.; t_p_leptop_hard_eta = -1.; t_p_leptop_hard_phi = -1.; t_p_leptop_hard_m = -1.;
  t_p_hadtop_hard_pt = -1.; t_p_hadtop_hard_eta = -1.; t_p_hadtop_hard_phi = -1.; t_p_hadtop_hard_m = -1.;
  t_p_higgs_hard_pt = -1.; t_p_higgs_hard_eta = -1.; t_p_higgs_hard_phi = -1.; t_p_higgs_hard_m = -1.;

  t_p_leptop_b_pt = -1.; t_p_leptop_b_eta = -1.; t_p_leptop_b_phi = -1.; t_p_leptop_b_m = -1.;
  t_p_leptop_W_pt = -1.; t_p_leptop_W_eta = -1.; t_p_leptop_W_phi = -1.; t_p_leptop_W_m = -1.;
  t_p_leptop_lep_pt = -1.; t_p_leptop_lep_eta = -1.; t_p_leptop_lep_phi = -1.; t_p_leptop_lep_m = -1.;
  t_p_leptop_nu_pt = -1.; t_p_leptop_nu_eta = -1.; t_p_leptop_nu_phi = -1.; t_p_leptop_nu_m = -1.;

  t_p_hadtop_b_pt = -1.; t_p_hadtop_b_eta = -1.; t_p_hadtop_b_phi = -1.; t_p_hadtop_b_m = -1.;
  t_p_hadtop_W_pt = -1.; t_p_hadtop_W_eta = -1.; t_p_hadtop_W_phi = -1.; t_p_hadtop_W_m = -1.;
  t_p_hadtop_q1_pt = -1.; t_p_hadtop_q1_eta = -1.; t_p_hadtop_q1_phi = -1.; t_p_hadtop_q1_m = -1.; t_p_hadtop_q1_flav = -1; 
  t_p_hadtop_q2_pt = -1.; t_p_hadtop_q2_eta = -1.; t_p_hadtop_q2_phi = -1.; t_p_hadtop_q2_m = -1.; t_p_hadtop_q2_flav = -1;

  t_p_higgs_b1_pt = -1.; t_p_higgs_b1_eta = -1.; t_p_higgs_b1_phi = -1.; t_p_higgs_b1_m = -1.;
  t_p_higgs_b2_pt = -1.; t_p_higgs_b2_eta = -1.; t_p_higgs_b2_phi = -1.; t_p_higgs_b2_m = -1.;

  t_j_leptop_b_ind = -1;
  t_j_leptop_lep_pt = -1.; t_j_leptop_lep_eta = -1.; t_j_leptop_lep_phi = -1.; t_j_leptop_lep_m = -1.;
  t_j_leptop_lep_charge = -99.;

  t_j_leptop_nu_pt = -1.; t_j_leptop_nu_phi = -1.;

  t_j_hadtop_b_ind = -1;
  t_j_hadtop_q1_ind = -1;
  t_j_hadtop_q2_ind = -1;

  t_j_higgs_b1_ind = -1;
  t_j_higgs_b2_ind = -1;
  
  t_p_leptop_lep_flav = -1;
  t_j_leptop_lep_flav = -1;
  
  t_pj_leptop_b_dr = -1.;
  t_pj_leptop_lep_dr = -1.;
  t_pj_leptop_nu_dphi = -1.;
  
  t_pj_hadtop_b_dr = -1.;
  t_pj_hadtop_q1_dr = -1.;
  t_pj_hadtop_q2_dr = -1.;

  t_pj_higgs_b1_dr = -1.;
  t_pj_higgs_b2_dr = -1.;

  t_j_hadW_cand_ind = -1;
  t_j_hadtop_nolepb_cand_ind = -1;
  t_j_hadtop_truth_cand_ind = -1;
  t_j_hadtop_Wbhtt_cand_ind = -1;
  t_j_hadtop_Wbtrim_cand_ind = -1;

  
  t_jets_pt = 0;
  t_jets_eta = 0;
  t_jets_phi = 0;
  t_jets_m = 0;
  t_jets_MV1 = 0;
  t_jets_trueflavor = 0;
  t_jets_ntrack = 0;
  t_jets_trackwidth = 0;
  t_jets_charge = 0;

  //vectors containing selected fatjets
  t_fatjets_n.clear();
  t_fatjets_pt.clear();
  t_fatjets_eta.clear();
  t_fatjets_phi.clear();
  t_fatjets_m.clear();
  t_fatjets_constscale_pt.clear();
  t_fatjets_constscale_eta.clear();
  t_fatjets_constscale_phi.clear();
  t_fatjets_constscale_m.clear();
  t_fatjets_ActiveArea.clear();
  t_fatjets_ActiveArea_px.clear();
  t_fatjets_ActiveArea_py.clear();
  t_fatjets_ActiveArea_pz.clear();
  t_fatjets_ActiveArea_E.clear();

  t_fatjets_SPLIT12.clear();
  t_fatjets_SPLIT23.clear();
  t_fatjets_ZCUT12.clear();
  t_fatjets_ZCUT23.clear();
  t_fatjets_WIDTH.clear();
  t_fatjets_PlanarFlow.clear();
  t_fatjets_Angularity.clear();
  t_fatjets_PullMag.clear();
  t_fatjets_PullPhi.clear();
  t_fatjets_Tau1.clear();
  t_fatjets_Tau2.clear();
  t_fatjets_Tau3.clear();
  t_fatjets_nbtag.clear();
  t_fatjets_nbtag_geom.clear();
  t_fatjets_MCclass.clear();
  t_fatjets_orig_ind.clear();
  t_fatjets_assoc_ind.clear();
  t_fatjets_truth_label.clear();

  //m_isSignal = 0;
  // cout<<"Inside constructor 1"<<endl;
  m_stree = s_tree;
  // cout<<"Inside constructor 2"<<endl;
  //m_ds_ID = ds_ID;
  if(ds_ID == 161882){
    m_ds_ID = "ttH";
    m_isSignal = 2;
  }
  else if(ds_ID == 181107){
    m_ds_ID = "ttbb";
    m_isSignal = 1;
  }
  else if(ds_ID == 117050){
    m_ds_ID = "tt_Pythia";
    m_isSignal = 1;
  } 
  else{
    m_ds_ID = "QCD";
    m_isSignal = 0;
  }
  // cout<<"Inside constructor 3"<<endl;
  m_IO = b_io;

  m_chain = new TChain(m_stree.c_str());

  // cout<<"Inside constructor 5"<<endl;
  m_fileloc = ds_fileloc;
  // cout<<"Inside constructor 6"<<endl;
  //m_outtree = new TTree();
   cout<<" WTREE="<< (m_IO & WTREE)<<" WHIST="<< (m_IO & WHIST) <<endl;
  if(m_IO & WHIST){
    m_sout_hist = Form("%s_%s_HIST.root",m_ds_ID.c_str(),s_out.c_str());
     cout<<"Output HISTO file name = "<<m_sout_hist.c_str()<<endl;
    m_outfile_hist = TFile::Open(m_sout_hist.c_str(), "RECREATE");
  }
  if(m_IO & WTREE){
    m_sout_tree = Form("%s_%s_TREE.root",m_ds_ID.c_str(),s_out.c_str());
     cout<<"Output TREE file name = "<<m_sout_tree.c_str()<<endl;
    m_outfile_tree = TFile::Open(m_sout_tree.c_str(), "RECREATE");
    m_outtree = new TTree("TruthAnaTree","TruthAnaTree");

  }
  m_jetcol_ID = "jet_AntiKt4TruthJets";

  m_pregroom_jetcol = "jet_AntiKt4TruthJets";
  m_pregroom_jetID = I_PREGROOM_INPUT;

  m_fatjetcol_IDs[i_nolepb] = s_nolepb;
  m_fatjetcol_IDs[i_nolepb_trim] = s_nolepb_trim;
  
  m_fatjetcol_IDs[i_nolepb_akt] = s_nolepb_akt;
  m_fatjetcol_IDs[i_nolepb_R6] = s_nolepb_R6;
  m_fatjetcol_IDs[i_nolepb_trim_R6] = s_nolepb_trim_R6;
  m_fatjetcol_IDs[i_nolepb_R10] = s_nolepb_R10;
  m_fatjetcol_IDs[i_nolepb_trim_R10] = s_nolepb_trim_R10;
  m_fatjetcol_IDs[i_nolepb_akt4] = s_nolepb_akt4;
  //m_fatjetcol_IDs[i_vetoht_nolepb_bdrs] = s_vetoht_nolepb_bdrs;
  /*
  m_fatjetcol_IDs[i_truth] = s_truth;
  m_fatjetcol_IDs[i_truth_trim] = s_truth_trim;
  
  m_fatjetcol_IDs[i_truth_akt] = s_truth_akt;
  m_fatjetcol_IDs[i_truth_R6] = s_truth_R6;
  m_fatjetcol_IDs[i_truth_trim_R6] = s_truth_trim_R6;
  m_fatjetcol_IDs[i_truth_R10] = s_truth_R10;
  m_fatjetcol_IDs[i_truth_trim_R10] = s_truth_trim_R10;
  */
  //m_fatjetcol_IDs[i_vetoht_truth_bdrs] = s_vetoht_truth_bdrs;

  /*
  m_fatjetcol_IDs[i_nolepbnob] = s_nolepbnob;
  m_fatjetcol_IDs[i_nolepbnob_bdrs] = s_nolepbnob_bdrs;

  m_fatjetcol_IDs[i_wbcand] = s_wbcand;
  m_fatjetcol_IDs[i_wbcand_trim] = s_wbcand_trim;
  m_fatjetcol_IDs[i_wbcand_htt] = s_wbcand_htt;

 

  m_fatjetcol_IDs[i_vetoht_wbhtt] = s_vetoht_wbhtt;
  m_fatjetcol_IDs[i_vetoht_wbhtt_bdrs] = s_vetoht_wbhtt_bdrs;

  m_fatjetcol_IDs[i_vetoht_wbtrim] = s_vetoht_wbtrim;
  m_fatjetcol_IDs[i_vetoht_wbtrim_bdrs] = s_vetoht_wbtrim_bdrs;
  */


  //map<string, vector<int> > m_rebuild_jetcol_IDs; //to optimise jet rebuilding

  int k_fj=0;
  m_rebuild_jetcol_IDs.clear();

  vector<int> v_ca8nolepb; v_ca8nolepb.clear();
  v_ca8nolepb.push_back(i_nolepb);
  v_ca8nolepb.push_back(i_nolepb_trim);
  //v_ca15nolepb.push_back(i_nolepb_htt);
  m_rebuild_jetcol_IDs[s_nolepb] = v_ca8nolepb;

  vector<int> v_akt2nolepb; v_akt2nolepb.clear(); 
  v_akt2nolepb.push_back(i_nolepb_akt);
  //v_ca8vetoht_nolepb.push_back(i_vetoht_nolepb_bdrs);
  m_rebuild_jetcol_IDs[s_nolepb_akt] = v_akt2nolepb;


  vector<int> v_ca6nolepb; v_ca6nolepb.clear();
  v_ca6nolepb.push_back(i_nolepb_R6);
  v_ca6nolepb.push_back(i_nolepb_trim_R6);
  //v_ca15nolepb.push_back(i_nolepb_htt);
  m_rebuild_jetcol_IDs[s_nolepb_R6] = v_ca6nolepb;

  vector<int> v_ca10nolepb; v_ca10nolepb.clear();
  v_ca10nolepb.push_back(i_nolepb_R10);
  v_ca10nolepb.push_back(i_nolepb_trim_R10);
  //v_ca15nolepb.push_back(i_nolepb_htt);
  m_rebuild_jetcol_IDs[s_nolepb_R10] = v_ca10nolepb;


  vector<int> v_akt4nolepb; v_akt4nolepb.clear(); 
  v_akt4nolepb.push_back(i_nolepb_akt4);
  //v_ca8vetoht_nolepb.push_back(i_vetoht_nolepb_bdrs);
  m_rebuild_jetcol_IDs[s_nolepb_akt4] = v_akt4nolepb;

  /*
  vector<int> v_ca8truth; v_ca8truth.clear();
  v_ca8truth.push_back(i_truth);
  v_ca8truth.push_back(i_truth_trim);
  m_rebuild_jetcol_IDs[s_truth] = v_ca8truth;

  vector<int> v_akt2truth; v_akt2truth.clear(); 
  v_akt2truth.push_back(i_truth_akt);
  //v_ca8vetoht_truth.push_back(i_vetoht_truth_bdrs);
  m_rebuild_jetcol_IDs[s_truth_akt] = v_akt2truth;
  */

  return;
}

EventRecoAnalysis::~EventRecoAnalysis(){
  // cout<<"fChain="<<fChain<<" ffriendChain1="<<ffriendChain1<<" ffriendChain2="<<ffriendChain2<<endl;
  // cout<<" outanatree="<<outanatree<<" outtree="<<outtree<<" outfile="<<outfile<<endl;

  if(m_IO & WTREE){

    delete t_jets_pt;
    delete t_jets_eta;
    delete t_jets_phi;
    delete t_jets_m;
    delete t_jets_MV1;
    delete t_jets_trueflavor;
    delete t_jets_ntrack;
    delete t_jets_trackwidth;
    delete t_jets_charge;


    //delete pointers, then clear the map
    for(map<int, string>::iterator j_it = m_fatjetcol_IDs.begin(); j_it != m_fatjetcol_IDs.end(); ++j_it){
      delete t_fatjets_pt[j_it->first];
      delete t_fatjets_eta[j_it->first];
      delete t_fatjets_phi[j_it->first];
      delete t_fatjets_m[j_it->first];
      delete t_fatjets_constscale_pt[j_it->first];
      delete t_fatjets_constscale_eta[j_it->first];
      delete t_fatjets_constscale_phi[j_it->first];
      delete t_fatjets_constscale_m[j_it->first];
      delete t_fatjets_ActiveArea[j_it->first];
      delete t_fatjets_ActiveArea_px[j_it->first];
      delete t_fatjets_ActiveArea_py[j_it->first];
      delete t_fatjets_ActiveArea_pz[j_it->first];
      delete t_fatjets_ActiveArea_E[j_it->first];
      
      delete t_fatjets_SPLIT12[j_it->first];
      delete t_fatjets_SPLIT23[j_it->first];
      delete t_fatjets_ZCUT12[j_it->first];
      delete t_fatjets_ZCUT23[j_it->first];
      delete t_fatjets_WIDTH[j_it->first];
      delete t_fatjets_PlanarFlow[j_it->first];
      delete t_fatjets_Angularity[j_it->first];
      delete t_fatjets_PullMag[j_it->first];
      delete t_fatjets_PullPhi[j_it->first];
      delete t_fatjets_Tau1[j_it->first];
      delete t_fatjets_Tau2[j_it->first];
      delete t_fatjets_Tau3[j_it->first];
      delete t_fatjets_nbtag[j_it->first];
      delete t_fatjets_nbtag_geom[j_it->first];
      delete t_fatjets_MCclass[j_it->first];
      delete t_fatjets_orig_ind[j_it->first];
      delete t_fatjets_assoc_ind[j_it->first];
      delete t_fatjets_truth_label[j_it->first];
      
      delete fatjet_nbtag[j_it->first];
      delete fatjet_nbtag_geom[j_it->first];
      delete fatjet_MCclass[j_it->first];
      delete fatjet_orig_ind[j_it->first];
      delete fatjet_assoc_ind[j_it->first];

      
      delete fatjet_pt[j_it->first];
      delete fatjet_eta[j_it->first];
      delete fatjet_phi[j_it->first];
      delete fatjet_m[j_it->first];
      delete fatjet_constscale_pt[j_it->first];
      delete fatjet_constscale_eta[j_it->first];
      delete fatjet_constscale_phi[j_it->first];
      delete fatjet_constscale_m[j_it->first];
      delete fatjet_ActiveArea[j_it->first];
      delete fatjet_ActiveArea_px[j_it->first];
      delete fatjet_ActiveArea_py[j_it->first];
      delete fatjet_ActiveArea_pz[j_it->first];
      delete fatjet_ActiveArea_E[j_it->first];
      delete fatjet_SPLIT12[j_it->first];
      delete fatjet_SPLIT23[j_it->first];
      delete fatjet_ZCUT12[j_it->first];
      delete fatjet_ZCUT23[j_it->first];
      delete fatjet_WIDTH[j_it->first];
      delete fatjet_PlanarFlow[j_it->first];
      delete fatjet_Angularity[j_it->first];
      delete fatjet_PullMag[j_it->first];
      delete fatjet_PullPhi[j_it->first];
      delete fatjet_Tau1[j_it->first];
      delete fatjet_Tau2[j_it->first];
      delete fatjet_Tau3[j_it->first];
      delete fatjet_constit_n[j_it->first];
      delete fatjet_constit_index[j_it->first];

      
    }
    
    t_fatjets_n.clear();
    t_fatjets_pt.clear();
    t_fatjets_eta.clear();
    t_fatjets_phi.clear();
    t_fatjets_m.clear();
    t_fatjets_constscale_pt.clear();
    t_fatjets_constscale_eta.clear();
    t_fatjets_constscale_phi.clear();
    t_fatjets_constscale_m.clear();
    t_fatjets_ActiveArea.clear();
    t_fatjets_ActiveArea_px.clear();
    t_fatjets_ActiveArea_py.clear();
    t_fatjets_ActiveArea_pz.clear();
    t_fatjets_ActiveArea_E.clear();
    
    t_fatjets_SPLIT12.clear();
    t_fatjets_SPLIT23.clear();
    t_fatjets_ZCUT12.clear();
    t_fatjets_ZCUT23.clear();
    t_fatjets_WIDTH.clear();
    t_fatjets_PlanarFlow.clear();
    t_fatjets_Angularity.clear();
    t_fatjets_PullMag.clear();
    t_fatjets_PullPhi.clear();
    t_fatjets_Tau1.clear();
    t_fatjets_Tau2.clear();
    t_fatjets_Tau3.clear();
    t_fatjets_nbtag.clear();
    t_fatjets_nbtag_geom.clear();
    t_fatjets_MCclass.clear();
    t_fatjets_orig_ind.clear();
    t_fatjets_assoc_ind.clear();
    t_fatjets_truth_label.clear();
  }

  m_fatjetcol_IDs.clear();

  m_stree.clear();
  m_fileloc.clear();
  m_ds_ID.clear();
  m_sout_hist.clear();
  m_sout_tree.clear();

  m_jetcol_ID.clear();
  m_rebuild_jetcol_IDs.clear();

  m_jet_ind.clear();
  m_el_ind.clear();
  m_mu_ind.clear();
  m_jet_e_overlap_ind.clear();
  m_el_jet_overlap_ind.clear();
  m_mu_jet_overlap_ind.clear();

  ////////

  m_ankt2_e_overlap_ind.clear();
  m_ankt4_e_overlap_ind.clear();

  ////////
 
  m_mc_bc_ind.clear();
  m_fatjet_ind.clear();

  fatjet_n.clear();
  fatjet_pt.clear();
  fatjet_eta.clear();
  fatjet_phi.clear();
  fatjet_m.clear();
  fatjet_constscale_pt.clear();
  fatjet_constscale_eta.clear();
  fatjet_constscale_phi.clear();
  fatjet_constscale_m.clear();
  fatjet_ActiveArea.clear();
  fatjet_ActiveArea_px.clear();
  fatjet_ActiveArea_py.clear();
  fatjet_ActiveArea_pz.clear();
  fatjet_ActiveArea_E.clear();

  fatjet_SPLIT12.clear();
  fatjet_SPLIT23.clear();
  fatjet_ZCUT12.clear();
  fatjet_ZCUT23.clear();
  fatjet_WIDTH.clear();
  fatjet_PlanarFlow.clear();
  fatjet_Angularity.clear();
  fatjet_PullMag.clear();
  fatjet_PullPhi.clear();
  fatjet_Tau1.clear();
  fatjet_Tau2.clear();
  fatjet_Tau3.clear();
  fatjet_nbtag.clear();
  fatjet_nbtag_geom.clear();
  fatjet_MCclass.clear();
  fatjet_orig_ind.clear();
  fatjet_assoc_ind.clear();

  fatjet_constit_n.clear();
  fatjet_constit_index.clear();

  ClearEventTLVs();
  delete m_outfile_tree;
  delete m_outfile_hist;
  delete m_chain;
  //delete m_pdfTool;
  //delete m_calib_Tool;
  //delete pName;
  //delete pModule;
  //delete setRhoMuNvertex;
  delete cell_pt;
  delete cell_eta;
  delete cell_phi;
  delete cell_E;


  delete el_E;
  delete el_pt;
  delete el_m;
  delete el_eta;
  delete el_phi;
  delete el_charge;
  
  delete mu_E;
  delete mu_pt;
  delete mu_m;
  delete mu_eta;
  delete mu_phi;
  delete mu_charge;




  return;
}

int EventRecoAnalysis::Init(){
  int sc=0;
   cout<<"Initialising"<<endl;

  for(vector<string>::iterator vs_it = m_fileloc.begin(); vs_it != m_fileloc.end(); ++vs_it){
    m_chain->Add(vs_it->c_str());
  }
  m_nev = m_chain->GetEntries();

  if(m_ds_ID == "ttH"){ m_PDF_set = 10042; } //CTEQ6L1
  else if(m_ds_ID == "tt_Pythia"){ m_PDF_set = 10800; } //CT10
  else{ m_PDF_set = 0; }
  if(m_beam_nrg_RW > 0.){ cout<<"NO PDF Tool"<<endl;}//m_pdfTool = new PDFTool(m_beam_nrg, m_beam_nrg_RW/m_beam_nrg, -1, m_PDF_set); } 

  m_tlv_p_top_hard = new TLorentzVector();
  m_tlv_p_atop_hard = new TLorentzVector();

  m_tlv_p_leptop_hard = new TLorentzVector();
  m_tlv_p_hadtop_hard = new TLorentzVector();

  m_tlv_p_higgs_hard = new TLorentzVector();
  m_tlv_p_ttbar_hard = new TLorentzVector();
  //
  m_tlv_p_top = new TLorentzVector();
  m_tlv_p_atop = new TLorentzVector();

  m_tlv_p_leptop = new TLorentzVector();
  m_tlv_p_hadtop = new TLorentzVector();

  m_tlv_p_higgs = new TLorentzVector();
  m_tlv_p_ttbar = new TLorentzVector();

  m_tlv_p_leptop_b = new TLorentzVector();
  m_tlv_p_leptop_W = new TLorentzVector();
  m_tlv_p_leptop_lep = new TLorentzVector();
  m_tlv_p_leptop_nu = new TLorentzVector();

  m_tlv_p_hadtop_b = new TLorentzVector();
  m_tlv_p_hadtop_W = new TLorentzVector();
  m_tlv_p_hadtop_q1 = new TLorentzVector();
  m_tlv_p_hadtop_q2 = new TLorentzVector();

  m_tlv_p_higgs_b1 = new TLorentzVector();
  m_tlv_p_higgs_b2 = new TLorentzVector();

  sc = InitEventBranches();
  sc += InitJetBranches(m_jetcol_ID);
  for(map<int, string>::iterator j_it = m_fatjetcol_IDs.begin(); j_it != m_fatjetcol_IDs.end(); ++j_it){
    sc += InitFatJetBranches(j_it->first);
  }

  //0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  
  cell_pt=new vector<float>;
  cell_eta=new vector<float>;
  cell_phi=new vector<float>;
  cell_E=new vector<float>;


  ////LEPTON////

  el_pt=new vector<float>;
  el_eta=new vector<float>;
  el_phi=new vector<float>;
  el_E=new vector<float>;
  el_m=new vector<float>;
  el_charge=new vector<float>;


  mu_pt=new vector<float>;
  mu_eta=new vector<float>;
  mu_phi=new vector<float>;
  mu_E=new vector<float>;
  mu_m=new vector<float>;
  mu_charge=new vector<float>;

  m_reco_met_px=0;
  m_reco_met_py=0;
  //////////////



  if(m_IO & WHIST){
    sc += InitPartonHistograms();
    sc += InitJetHistograms();

    sc+= InitCutFlowHistograms();
     cout<<"Initialised"<<endl;
  }
  
  if(m_IO & WTREE){
    InitPartonOutputBranches();
    InitJetOutputBranches();
    for(map<int, string>::iterator j_it = m_fatjetcol_IDs.begin(); j_it != m_fatjetcol_IDs.end(); ++j_it){
      sc += InitFatJetOutputBranches(j_it->first);
    }
    InitEventOutputBranches();
  }

  //TPython::Exec( "import calib_functions" );
  //pModule = PyImport_AddModule( (char*)"calib_functions" );

  /*
  Py_Initialize();
   cout<<"1 pName = "<<pName<<" pModule = "<<pModule<<endl;
  pName = PyString_FromString("calib_functions");
  pModule = PyImport_Import(pName);
  Py_DECREF(pName);
   cout<<"4 pName = "<<pName<<" pModule = "<<pModule<<endl;
  */
  /*
  // Load Functions from the Python Module
  if (pModule != NULL) {
     cout<<"pmodule not NULL"<<endl;
    globalInclCsTA    = PyObject_GetAttrString(pModule, "ClusterInclusiveWithCalib");
    globalExclCsTA    = PyObject_GetAttrString(pModule, "ClusterExclusiveWithCalib");
    setRhoMuNvertex = PyObject_GetAttrString(pModule, "SetRhoMuNvertex");
  }
  else{
     cout<<"pmodule is NULL"<<endl;
  }    
  */
  return sc;
}

int EventRecoAnalysis::Loop(){
  int loop=0;
   cout<<"start loop m_nev = "<<m_nev<<endl;
  //int n=nev;
  //int n=2;
  //for(int i=0; i<100; i++){
   if(m_nlast==-1){m_nlast=m_nev;}
   for(int i=m_nfirst; i<m_nlast; i++){
     //if(i>2){break;}
     //if(i<570){continue;}
     //if(i<157002){break;}
     //if(i>4594){break;}
     loop=Process(i);
   }
   
  return loop;
}


int EventRecoAnalysis::Process(long entry){

  //Find truth particles
  //Select jets in acceptance region
  //Match truth partices to jets
  //Fill histograms
  if(entry%10==0)
    cout<<"Entry = "<<entry<<endl;

  //cout<<endl<<endl<<endl
  //    <<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
  //    <<"Entry = "<<entry
  //   <<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;

  m_chain->GetEntry(entry); // cout<<" eventNumber = "<<eventNumber<<endl;
  //if(eventNumber != 293004 || eventNumber != 293080 ){return 0;}
 
  //MCParticlesDump();
  //return 0;
  
  //MCParticlesDump();
  //return 0;
  
  //Set all the particle and jet indices initially to -1
  m_ip_top_hard = -1;
  m_ip_atop_hard = -1;
  m_ip_leptop_hard = -1;
  m_ip_hadtop_hard = -1;
  m_ip_higgs_hard = -1;

  m_ip_top = -1;
  m_ip_atop = -1;
  m_ip_leptop = -1;
  m_ip_hadtop = -1;
  m_ip_higgs = -1;

  m_ip_top_b = -1;
  m_ip_top_W = -1;
  m_ip_atop_b = -1;
  m_ip_atop_W = -1;


  m_ip_leptop_b = -1;
  m_ip_leptop_W = -1;
  m_ip_leptop_lep = -1;
  m_ip_leptop_nu = -1;

  m_ip_hadtop_b = -1;
  m_ip_hadtop_W = -1;
  m_ip_hadtop_q1 = -1;
  m_ip_hadtop_q2 = -1;

  m_ip_higgs_b1 = -1;
  m_ip_higgs_b2 = -1;

  //jet match indices
  m_ij_leptop_b = -1;

  m_ij_hadtop_b = -1;
  m_ij_hadtop_q1 = -1;
  m_ij_hadtop_q2 = -1;

  m_ij_higgs_b1 = -1;
  m_ij_higgs_b2 = -1;

  m_ifj_hadtop.clear();
  m_ifj_higgs.clear();

  m_ij_hadW_cand = -1;
  m_ij_hadtop_nolepb_cand = -1;
  m_ij_hadtop_truth_cand = -1;
  m_ij_hadtop_Wbhtt_cand = -1;
  m_ij_hadtop_Wbtrim_cand = -1;

  //--------------------------------------------------------------
  m_ij_ind_leptop_b = -1;

  m_ij_ind_hadtop_b = -1;
  m_ij_ind_hadtop_q1 = -1;
  m_ij_ind_hadtop_q2 = -1;

  m_ij_ind_higgs_b1 = -1;
  m_ij_ind_higgs_b2 = -1;

  m_ifj_ind_hadtop.clear();
  m_ifj_ind_higgs.clear();

  m_ij_ind_hadW_cand = -1;
  m_ij_ind_hadtop_nolepb_cand = -1;
  m_ij_ind_hadtop_truth_cand = -1;
  m_ij_ind_hadtop_Wbhtt_cand = -1;
  m_ij_ind_hadtop_Wbtrim_cand = -1;

  //--------------------------------------------------------------



  m_jet_ind.clear();
  m_el_ind.clear();
  m_mu_ind.clear();
  m_jet_e_overlap_ind.clear();
  m_el_jet_overlap_ind.clear();
  m_mu_jet_overlap_ind.clear();

  ////////

  m_ankt2_e_overlap_ind.clear();
  m_ankt4_e_overlap_ind.clear();
  m_reco_met_px=0;
  m_reco_met_py=0;
  ////////



  m_mc_bc_ind.clear();
  m_fatjet_ind.clear();

  m_p_leptop_lep_flav = -1;
  m_j_leptop_lep_flav = -1;

  t_Eventshape_rhoKt4LC = 0.;
  t_Eventshape_rhoKt6LC = 0.;
  t_nvtx = 0;
  t_mu = 0.;
  t_mcevt_weight = 0.;
  t_RWPDFWeight = 0.;
  t_HF_class=0;
  t_trig_pass_el = 0;
  t_trig_pass_mu = 0;
  t_trig_match_el = 0;
  t_trig_match_mu = 0;
 
  t_p_leptop_pt = -1.; t_p_leptop_eta = -1.; t_p_leptop_phi = -1.; t_p_leptop_m = -1.;
  t_p_hadtop_pt = -1.; t_p_hadtop_eta = -1.; t_p_hadtop_phi = -1.; t_p_hadtop_m = -1.;
  t_p_higgs_pt = -1.; t_p_higgs_eta = -1.; t_p_higgs_phi = -1.; t_p_higgs_m = -1.;

  t_p_leptop_b_pt = -1.; t_p_leptop_b_eta = -1.; t_p_leptop_b_phi = -1.; t_p_leptop_b_m = -1.;
  t_p_leptop_W_pt = -1.; t_p_leptop_W_eta = -1.; t_p_leptop_W_phi = -1.; t_p_leptop_W_m = -1.;
  t_p_leptop_lep_pt = -1.; t_p_leptop_lep_eta = -1.; t_p_leptop_lep_phi = -1.; t_p_leptop_lep_m = -1.; t_p_leptop_lep_flav = -1;
  t_p_leptop_nu_pt = -1.; t_p_leptop_nu_eta = -1.; t_p_leptop_nu_phi = -1.; t_p_leptop_nu_m = -1.;

  t_p_hadtop_b_pt = -1.; t_p_hadtop_b_eta = -1.; t_p_hadtop_b_phi = -1.; t_p_hadtop_b_m = -1.;
  t_p_hadtop_W_pt = -1.; t_p_hadtop_W_eta = -1.; t_p_hadtop_W_phi = -1.; t_p_hadtop_W_m = -1.;
  t_p_hadtop_q1_pt = -1.; t_p_hadtop_q1_eta = -1.; t_p_hadtop_q1_phi = -1.; t_p_hadtop_q1_m = -1.; t_p_hadtop_q1_flav = -1;
  t_p_hadtop_q2_pt = -1.; t_p_hadtop_q2_eta = -1.; t_p_hadtop_q2_phi = -1.; t_p_hadtop_q2_m = -1.; t_p_hadtop_q2_flav = -1;

  t_p_higgs_b1_pt = -1.; t_p_higgs_b1_eta = -1.; t_p_higgs_b1_phi = -1.; t_p_higgs_b1_m = -1.;
  t_p_higgs_b2_pt = -1.; t_p_higgs_b2_eta = -1.; t_p_higgs_b2_phi = -1.; t_p_higgs_b2_m = -1.;

  t_j_leptop_b_ind = -1;
  t_j_leptop_lep_pt = -1.; t_j_leptop_lep_eta = -1.; t_j_leptop_lep_phi = -1.; t_j_leptop_lep_m = -1.; t_j_leptop_lep_flav = -1;
  t_j_leptop_lep_charge = -99.;
  t_j_leptop_nu_pt = -1.; t_j_leptop_nu_phi = -1.;

  t_j_hadtop_b_ind = -1;
  t_j_hadtop_q1_ind = -1;
  t_j_hadtop_q2_ind = -1;

  t_j_higgs_b1_ind = -1;
  t_j_higgs_b2_ind = -1;
  
  t_pj_leptop_b_dr = -1.;
  t_pj_leptop_lep_dr = -1.;
  t_pj_leptop_nu_dphi = -1.;

  t_pj_hadtop_b_dr = -1.;
  t_pj_hadtop_q1_dr = -1.;
  t_pj_hadtop_q2_dr = -1.;

  t_pj_higgs_b1_dr = -1.;
  t_pj_higgs_b2_dr = -1.;

  t_j_hadW_cand_ind = -1;
  t_j_hadtop_nolepb_cand_ind = -1;
  t_j_hadtop_truth_cand_ind = -1;
  t_j_hadtop_Wbhtt_cand_ind = -1;
  t_j_hadtop_Wbtrim_cand_ind = -1;
  
  int sc=0;

  //MakeMCBarcodeMap();
  //tlv_test = new TLorentzVector();
  //sc += AssignGenPartIndx_Pythia_debug();

  int i_gen=-1;
  if(m_ds_ID == "ttH"){i_gen=I_MADPYTH;}
  else if(m_ds_ID == "ttbb"){i_gen=I_MADPYTH;}
  else if(m_ds_ID == "tt_Pythia"){i_gen=I_PYTHIA6;} 

  t_Eventshape_rhoKt4LC = 1;//EventPileupDensity(0.4);
  t_Eventshape_rhoKt6LC = 1;//EventPileupDensity(0.6);
  t_RWPDFWeight = 1;//CalcRWPDFWeight();
  t_mu = mu;
  t_mcevt_weight = mcevt_weight->at(0).at(0);

  m_ev_wgt = mcevt_weight->at(0).at(0);


  ClassifyHF m_HFtest;
  SetTopData(&td);
  bool tmp_HF=false;
  int code;int ext_code;int prompt_code;
  tmp_HF=m_HFtest.classify(&td,&code,&ext_code,0,0,&prompt_code);

  t_HF_class=ext_code;
  
/*
  double rho=0.; int n_vertex=1; double mu=0.;
  
  PyObject *argli_setrmn = Py_BuildValue( "(ddi)", rho, mu, n_vertex );
  PyObject_CallObject( setRhoMuNvertex, argli_setrmn );
  Py_DECREF( argli_setrmn );     
  */
  sc = AssignGenPartIndx(i_gen);

  ////FILL CELLS FOR CALO RECONSTRUCTION////
  FillCells();
  //////////////////////////////////////////
  
  if(sc < -1 || m_ip_leptop_lep < 0){return 0;}
  if(m_IO & WTREE){

    t_jets_pt->clear();
    t_jets_eta->clear();
    t_jets_phi->clear();
    t_jets_m->clear();
    t_jets_MV1->clear();
    t_jets_trueflavor->clear();
    t_jets_ntrack->clear();
    t_jets_trackwidth->clear();
    t_jets_charge->clear();
  }



  
  if(m_isSignal>0){
    sc += FillPartonTLVs();
    sc += MatchPartonJets();
    //tlv_test->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  }
  

  //FATJET RECONSTRUCTION
  if(m_pregroom_jetcol.find("Rebuild") != string::npos){MakePregroomingInputCollection(m_pregroom_jetcol); }

  bool _doCalib = false; 
  bool _callSubstruct = true;
  bool _match_mc = true;
  bool _match_b = false;

  int njet_nolepb = 0;
  int njet_nolepb_trim = 0;
  
  MakeJetCollection(s_nolepb, m_rebuild_jetcol_IDs[s_nolepb]
		    , 30.*1000, m_fatjet_etamax, 30.*1000, _doCalib, _callSubstruct, _match_mc, _match_b,m_doJES);

  njet_nolepb = fatjet_n[i_nolepb];
  njet_nolepb_trim = fatjet_n[i_nolepb_trim];

  //cout<<"njet_nolepb = "<<njet_nolepb<<" njet_nolepb_trim = "<<njet_nolepb_trim<<endl;



  int njet_nolepb_akt = 0;
  
  MakeJetCollection(s_nolepb_akt, m_rebuild_jetcol_IDs[s_nolepb_akt]
                    , 5.*1000,m_fatjet_etamax , 5.*1000, _doCalib, _callSubstruct, _match_mc, _match_b,m_doJES);

  njet_nolepb_akt= fatjet_n[i_nolepb_akt];

  //cout<<"njet_nolepb_akt = "<<njet_nolepb_akt<<endl;





  int njet_nolepb_akt4 = 0;
  
  MakeJetCollection(s_nolepb_akt4, m_rebuild_jetcol_IDs[s_nolepb_akt4]
                    , 5.*1000,m_fatjet_etamax , 5.*1000, _doCalib, _callSubstruct, _match_mc, _match_b,m_doJES);

  njet_nolepb_akt4= fatjet_n[i_nolepb_akt4];

  //cout<<"njet_nolepb_akt4 = "<<njet_nolepb_akt4<<endl;



  int njet_nolepb_R6 = 0;
  int njet_nolepb_trim_R6 = 0;
  
  MakeJetCollection(s_nolepb_R6, m_rebuild_jetcol_IDs[s_nolepb_R6]
		    , 30.*1000, m_fatjet_etamax, 30.*1000, _doCalib, _callSubstruct, _match_mc, _match_b,m_doJES);

  njet_nolepb_R6 = fatjet_n[i_nolepb_R6];
  njet_nolepb_trim_R6 = fatjet_n[i_nolepb_trim_R6];
  
  //cout<<"njet_nolepb_R6 = "<<njet_nolepb_R6<<" njet_nolepb_trim_R6 = "<<njet_nolepb_trim_R6<<endl;



  int njet_nolepb_R10 = 0;
  int njet_nolepb_trim_R10 = 0;
  
  MakeJetCollection(s_nolepb_R10, m_rebuild_jetcol_IDs[s_nolepb_R10]
		    , 30.*1000, m_fatjet_etamax, 30.*1000, _doCalib, _callSubstruct, _match_mc, _match_b,m_doJES);

  njet_nolepb_R10 = fatjet_n[i_nolepb_R10];
  njet_nolepb_trim_R10 = fatjet_n[i_nolepb_trim_R10];
  
  //cout<<"njet_nolepb_R10 = "<<njet_nolepb_R10<<" njet_nolepb_trim_R10 = "<<njet_nolepb_trim_R10<<endl;
  
  
  RecoMET();
  
  

  int nel = 0; int nmu = 0;
  if(m_jetcol_ID.find("Truth") != string::npos){
    /*
      lep_pt = mc_pt->at(m_ip_leptop_lep);
      lep_eta = mc_eta->at(m_ip_leptop_lep);
      lep_phi = mc_phi->at(m_ip_leptop_lep);
      double lep_pmag = mc_pt->at(m_ip_leptop_lep)*cosh(mc_eta->at(m_ip_leptop_lep));
      double lep_mass = mc_m->at(m_ip_leptop_lep);
      lep_E = sqrt(lep_pmag*lep_pmag + lep_mass*lep_mass);
      lep_charge = mc_charge->at(m_ip_leptop_lep);
    */
    
    //cout<<"LEPTON FLAVOR = "<<m_p_leptop_lep_flav<<endl;
    //cout<<"NEL = "<<el_n<<endl;
    //cout<<"NMU = "<<mu_n<<endl;
    
    if(m_IO & WHIST){
      if(m_p_leptop_lep_flav==0){
	m_h1d["el_event_cutflow_hist"]->Fill(1);
      }
      if(m_p_leptop_lep_flav==1){
	m_h1d["mu_event_cutflow_hist"]->Fill(1);
      }
      if(m_p_leptop_lep_flav==2){
	m_h1d["tau_event_cutflow_hist"]->Fill(1);
      }
    }




    nel = TruthElectronSelection();
    nmu = TruthMuonSelection();
    
    if(m_IO & WHIST){
      if(m_p_leptop_lep_flav==0){
	if(nel >= 1){m_h1d["el_event_cutflow_hist"]->Fill(2);}
	if(nmu >= 1){m_h1d["el_event_cutflow_hist"]->Fill(3);}
      }
      if(m_p_leptop_lep_flav==1){
	if(nel >= 1){m_h1d["mu_event_cutflow_hist"]->Fill(2);}
	if(nmu >= 1){m_h1d["mu_event_cutflow_hist"]->Fill(3);}
      }
      if(m_p_leptop_lep_flav==2){
	if(nel >= 1){m_h1d["tau_event_cutflow_hist"]->Fill(2);}
	if(nmu >= 1){m_h1d["tau_event_cutflow_hist"]->Fill(3);}
      }
    }


    //cout<<"NEL = "<<nel<<endl;
    //cout<<"NMU = "<<nmu<<endl;
    
    TruthElectronJetOverlapRemoval(i_nolepb_akt,m_ankt2_e_overlap_ind);
    TruthElectronJetOverlapRemoval(i_nolepb_akt4,m_ankt4_e_overlap_ind);


    m_njet = JetSelection();


    for(map<int, string>::iterator j_it = m_fatjetcol_IDs.begin(); j_it != m_fatjetcol_IDs.end(); ++j_it){
    // cout<<"sfj_key = "<<j_it->first<<" sfj = "<<j_it->second<<endl;
    t_fatjets_n[j_it->first] = 0;
    t_fatjets_pt[j_it->first] = 0;
    t_fatjets_eta[j_it->first] = 0;
    t_fatjets_phi[j_it->first] = 0;
    t_fatjets_m[j_it->first] = 0;
    t_fatjets_constscale_pt[j_it->first] = 0;
    t_fatjets_constscale_eta[j_it->first] = 0;
    t_fatjets_constscale_phi[j_it->first] = 0;
    t_fatjets_constscale_m[j_it->first] = 0;
    t_fatjets_ActiveArea[j_it->first] = 0;
    t_fatjets_ActiveArea_px[j_it->first] = 0;
    t_fatjets_ActiveArea_py[j_it->first] = 0;
    t_fatjets_ActiveArea_pz[j_it->first] = 0;
    t_fatjets_ActiveArea_E[j_it->first] = 0;
      
    t_fatjets_SPLIT12[j_it->first] = 0;
    t_fatjets_SPLIT23[j_it->first] = 0;
    t_fatjets_ZCUT12[j_it->first] = 0;
    t_fatjets_ZCUT23[j_it->first] = 0;
    t_fatjets_WIDTH[j_it->first] = 0;
    t_fatjets_PlanarFlow[j_it->first] = 0;
    t_fatjets_Angularity[j_it->first] = 0;
    t_fatjets_PullMag[j_it->first] = 0;
    t_fatjets_PullPhi[j_it->first] = 0;
    t_fatjets_Tau1[j_it->first] = 0;
    t_fatjets_Tau2[j_it->first] = 0;
    t_fatjets_Tau3[j_it->first] = 0;
    t_fatjets_nbtag[j_it->first] = 0;
    t_fatjets_nbtag_geom[j_it->first] = 0;
    t_fatjets_MCclass[j_it->first] = 0;
    t_fatjets_orig_ind[j_it->first] = 0;
    t_fatjets_assoc_ind[j_it->first] = 0;
    t_fatjets_truth_label[j_it->first] = 0;
      
    int nfjet = FatJetSelection(j_it->first);
  }





    RemoveTruthElectronWithJetOverlap(i_nolepb_akt4);

    vector<int> m_el_ind_copy = m_el_ind;
    m_el_ind.clear();
    
    for(int k = 0; k < (int) m_el_ind_copy.size(); k++){
      bool bcopy = true;
      vector<int>::iterator eit = m_el_jet_overlap_ind.begin();
      while(eit != m_el_jet_overlap_ind.end()){
	if(k == *eit){ 
	  eit = m_el_jet_overlap_ind.erase(eit); 
	  bcopy = false; 
	  break;
	}
	else{eit++;}
      }
      
      if(bcopy){ m_el_ind.push_back(m_el_ind_copy.at(k)); }
      
    }
    nel = m_el_ind.size();
    


   

    RemoveTruthMuonWithJetOverlap(i_nolepb_akt4);


    vector<int> m_mu_ind_copy = m_mu_ind;
    m_mu_ind.clear();
    
    for(int k = 0; k < (int) m_mu_ind_copy.size(); k++){
      bool bcopy = true;
      vector<int>::iterator eit = m_mu_jet_overlap_ind.begin();
      while(eit != m_mu_jet_overlap_ind.end()){
	if(k == *eit){ 
	  eit = m_mu_jet_overlap_ind.erase(eit); 
	  bcopy = false; 
	  break;
	}
	else{eit++;}
      }
      
      if(bcopy){ m_mu_ind.push_back(m_mu_ind_copy.at(k)); }
      
    }
    nmu = m_mu_ind.size();


    if(m_IO & WHIST){
      if(m_p_leptop_lep_flav==0){
	if(nel >= 1){m_h1d["el_event_cutflow_hist"]->Fill(4);}
	if(nmu >= 1){m_h1d["el_event_cutflow_hist"]->Fill(5);}
	
	if(nel == 1){m_h1d["el_event_cutflow_hist"]->Fill(6);}
	if(nmu == 1){m_h1d["el_event_cutflow_hist"]->Fill(7);}
	
	if( (nel == 1) && (nmu == 0) ){
	  m_h1d["el_event_cutflow_hist"]->Fill(8);
	}
	if( (nel == 0) && (nmu == 1) ){
	  m_h1d["el_event_cutflow_hist"]->Fill(9);
	}
	
      }

      if(m_p_leptop_lep_flav==1){
	if(nel >= 1){m_h1d["mu_event_cutflow_hist"]->Fill(4);}
	if(nmu >= 1){m_h1d["mu_event_cutflow_hist"]->Fill(5);}
	
	if(nel == 1){m_h1d["mu_event_cutflow_hist"]->Fill(6);}
	if(nmu == 1){m_h1d["mu_event_cutflow_hist"]->Fill(7);}
	
	if( (nel == 1) && (nmu == 0) ){
	  m_h1d["mu_event_cutflow_hist"]->Fill(8);
	}
	if( (nel == 0) && (nmu == 1) ){
	  m_h1d["mu_event_cutflow_hist"]->Fill(9);
	}
	
      }

      if(m_p_leptop_lep_flav==2){
	if(nel >= 1){m_h1d["tau_event_cutflow_hist"]->Fill(4);}
	if(nmu >= 1){m_h1d["tau_event_cutflow_hist"]->Fill(5);}
	
	if(nel == 1){m_h1d["tau_event_cutflow_hist"]->Fill(6);}
	if(nmu == 1){m_h1d["tau_event_cutflow_hist"]->Fill(7);}
	
	if( (nel == 1) && (nmu == 0) ){
	  m_h1d["tau_event_cutflow_hist"]->Fill(8);
	}
	if( (nel == 0) && (nmu == 1) ){
	  m_h1d["tau_event_cutflow_hist"]->Fill(9);
	}
	
      }

    }


    m_nlep = OneLeptonSelection();
    if(m_nlep != 1){return -1;}
    //if(m_j_leptop_lep_flav == MUON){nmu = 1; nel = 0;}
    //else if(m_j_leptop_lep_flav == ELE){nmu = 0; nel = 1;}
  
    
  }
  else{
    
    SteerTruthMatchLep();
    
    nel = ElectronSelection();
    if(m_IO & WHIST){
      if(nel >= 1){m_h1d["el_event_cutflow_hist"]->Fill(6);}
    }
    nmu = MuonSelection();
    if(m_IO & WHIST){
      if(nmu >= 1){m_h1d["mu_event_cutflow_hist"]->Fill(6);}
    }
    ElectronJetOverlapRemoval();
    m_njet = JetSelection();
   


    for(map<int, string>::iterator j_it = m_fatjetcol_IDs.begin(); j_it != m_fatjetcol_IDs.end(); ++j_it){
      // cout<<"sfj_key = "<<j_it->first<<" sfj = "<<j_it->second<<endl;
      t_fatjets_n[j_it->first] = 0;
      t_fatjets_pt[j_it->first] = 0;
      t_fatjets_eta[j_it->first] = 0;
      t_fatjets_phi[j_it->first] = 0;
      t_fatjets_m[j_it->first] = 0;
      t_fatjets_constscale_pt[j_it->first] = 0;
      t_fatjets_constscale_eta[j_it->first] = 0;
      t_fatjets_constscale_phi[j_it->first] = 0;
      t_fatjets_constscale_m[j_it->first] = 0;
      t_fatjets_ActiveArea[j_it->first] = 0;
      t_fatjets_ActiveArea_px[j_it->first] = 0;
      t_fatjets_ActiveArea_py[j_it->first] = 0;
      t_fatjets_ActiveArea_pz[j_it->first] = 0;
      t_fatjets_ActiveArea_E[j_it->first] = 0;
      
      t_fatjets_SPLIT12[j_it->first] = 0;
      t_fatjets_SPLIT23[j_it->first] = 0;
      t_fatjets_ZCUT12[j_it->first] = 0;
      t_fatjets_ZCUT23[j_it->first] = 0;
      t_fatjets_WIDTH[j_it->first] = 0;
      t_fatjets_PlanarFlow[j_it->first] = 0;
      t_fatjets_Angularity[j_it->first] = 0;
      t_fatjets_PullMag[j_it->first] = 0;
      t_fatjets_PullPhi[j_it->first] = 0;
      t_fatjets_Tau1[j_it->first] = 0;
      t_fatjets_Tau2[j_it->first] = 0;
      t_fatjets_Tau3[j_it->first] = 0;
      t_fatjets_nbtag[j_it->first] = 0;
      t_fatjets_nbtag_geom[j_it->first] = 0;
      t_fatjets_MCclass[j_it->first] = 0;
      t_fatjets_orig_ind[j_it->first] = 0;
      t_fatjets_assoc_ind[j_it->first] = 0;
      t_fatjets_truth_label[j_it->first] = 0;
      
      int nfjet = FatJetSelection(j_it->first);
    }
    








 
    RemoveElectronWithJetOverlap();
    if(m_el_jet_overlap_ind.size() > 0){
      m_el_ind.erase(m_el_ind.begin() + m_el_jet_overlap_ind.at(0));
      nel--;
    }
    
    RemoveMuonWithJetOverlap();
    if(m_mu_jet_overlap_ind.size() > 0){
      m_mu_ind.erase(m_mu_ind.begin() + m_mu_jet_overlap_ind.at(0));
      nmu--;
    }
    
    if(m_IO & WHIST){
      if(nel >= 1){m_h1d["el_event_cutflow_hist"]->Fill(7);}
      if(nmu >= 1){m_h1d["mu_event_cutflow_hist"]->Fill(7);}
      
      if(nel == 1){m_h1d["el_event_cutflow_hist"]->Fill(8);}
      if(nmu == 1){m_h1d["mu_event_cutflow_hist"]->Fill(8);}
      
      if( (nel == 1) && (nmu == 0) ){
	m_h1d["el_event_cutflow_hist"]->Fill(9);
      }
      if( (nel == 0) && (nmu == 1) ){
	m_h1d["mu_event_cutflow_hist"]->Fill(9);
      }
    }
    
    m_nlep = LeptonSelection();
    if(m_nlep != 1){return -1;}
    
    if(!(m_IO & WTREE)){return 0;}
    
  }//if not truth selection
  
  











  
 /*
  //Find the best hadtop candidate
  int iclos_i = -1; double deltamin = 99.;
  for(int j = 0; j < fatjet_n[i_nolepb_htt]; j++){
    if( (fatjet_constscale_m[i_nolepb_htt]->at(j) < 140.*GeV) || (fatjet_constscale_m[i_nolepb_htt]->at(j) > 210.*GeV) ){continue;}
    double deltacur = fabs(fatjet_constscale_m[i_nolepb_htt]->at(j) - MASS_TOP)/GeV;

    if( deltacur > deltamin ){continue;}
    iclos_i = j; deltamin = deltacur;
  }
  m_ij_hadtop_nolepb_cand = iclos_i;
  
  vector<int>* v_veto_hgs = new vector<int>; v_veto_hgs->clear();
  vector<PseudoJet>* v_const_in_hgs = new vector<PseudoJet>; v_const_in_hgs->clear();
  //-----------------
  
  
  if(m_ij_hadtop_nolepb_cand >= 0){
    vector<int> v_const_ht_nolepb = fatjet_constit_index[i_nolepb_htt]->at(m_ij_hadtop_nolepb_cand);
    v_veto_hgs = &v_const_ht_nolepb;
    
    
    *v_const_in_hgs = FillJetInputCollection( 0, inMITRU, sigLC, v_veto_hgs);
     

    MakeJetCollection(s_vetoht_nolepb, m_rebuild_jetcol_IDs[s_vetoht_nolepb]
		      , m_fatjet_ptmin_loose, m_fatjet_etamax, m_fatjet_ptmin_loose, _doCalib, _callSubstruct, _match_mc, _match_b, v_const_in_hgs);
    
    v_const_in_hgs->clear();
  }



  */

  /*
  int njet_truth = 0;
  int njet_truth_trim = 0;
  MakeJetCollection(s_truth, m_rebuild_jetcol_IDs[s_truth]
		    , m_fatjet_ptmin_tight, m_fatjet_etamax, m_fatjet_ptmin_loose, _doCalib, _callSubstruct, _match_mc, _match_b);

  njet_truth = fatjet_n[i_truth];
  njet_truth_trim = fatjet_n[i_truth_trim];


  //cout<<"njet_truth = "<<njet_truth<<" njet_truth_trim = "<<njet_truth_trim<<endl;

  int njet_truth_akt = 0;
  
  MakeJetCollection(s_truth_akt, m_rebuild_jetcol_IDs[s_truth_akt]
		    , 5.*1000, 2.5, 5.*1000, _doCalib, _callSubstruct, _match_mc, _match_b);
  
  njet_truth_akt= fatjet_n[i_truth_akt];
  */
  //cout<<"njet_truth_akt = "<<njet_truth_akt<<endl;
  /*

  //Find the best hadtop candidate
  int iclos_i2 = -1; double deltamin2 = 99.;
  for(int j = 0; j < fatjet_n[i_truth_htt]; j++){
    if( (fatjet_constscale_m[i_truth_htt]->at(j) < 140.*GeV) || (fatjet_constscale_m[i_truth_htt]->at(j) > 210.*GeV) ){continue;}
    double deltacur = fabs(fatjet_constscale_m[i_truth_htt]->at(j) - MASS_TOP)/GeV;

    if( deltacur > deltamin ){continue;}
    iclos_i2 = j; deltamin2 = deltacur;
  }
  m_ij_hadtop_truth_cand = iclos_i2;

  vector<int>* v_veto_higgs = new vector<int>; v_veto_higgs->clear();
  vector<PseudoJet>* v_const_in_higgs = new vector<PseudoJet>; v_const_in_higgs->clear();
  //-----------------
  
  
  if(m_ij_hadtop_truth_cand >= 0){
    vector<int> v_const_ht_truth = fatjet_constit_index[i_truth_htt]->at(m_ij_hadtop_truth_cand);
    v_veto_higgs = &v_const_ht_truth;
    
    
    *v_const_in_higgs = FillJetInputCollection( 0, inTRUTH, sigLC, v_veto_higgs);
     

    MakeJetCollection(s_vetoht_truth, m_rebuild_jetcol_IDs[s_vetoht_truth]
		      , m_fatjet_ptmin_loose, m_fatjet_etamax, m_fatjet_ptmin_loose, _doCalib, _callSubstruct, _match_mc, _match_b, v_const_in_higgs);
    
    v_const_in_higgs->clear();
  }

  */
 

  
  
  
  /*

  //---------------------------------
  //Find appropriate indices
  //hadW : i_nolepbnob_bdrs
  //hadtop_nolepb : i_nolepb_htt
  //hadtop_Wbcand_trim : i_wbcand_trim
  //hadtop_Wbcand_HTT : i_wbcand_htt
  //---------------------------------

  if(m_ij_hadtop_nolepb_cand >= 0){
    for(int i = 0; i < m_fatjet_ind[i_nolepb_htt].size(); i++){
      if( m_fatjet_ind[i_nolepb_htt].at(i) == m_ij_hadtop_nolepb_cand ){
	m_ij_ind_hadtop_nolepb_cand = i;
      }
    }
  }
  if(m_ij_hadtop_truth_cand >= 0){
    for(int i = 0; i < m_fatjet_ind[i_truth_htt].size(); i++){
      if( m_fatjet_ind[i_truth_htt].at(i) == m_ij_hadtop_truth_cand ){
	m_ij_ind_hadtop_truth_cand = i;
      }
    }
  }

  */

  /*
  if(m_ij_hadW_cand >= 0){
    for(int i = 0; i < m_fatjet_ind[i_nolepbnob_bdrs].size(); i++){
      if( m_fatjet_ind[i_nolepbnob_bdrs].at(i) == m_ij_hadW_cand ){
	m_ij_ind_hadW_cand = i;
      }
    }
  }

  

  if(m_ij_hadtop_Wbtrim_cand >= 0){
    for(int i = 0; i < m_fatjet_ind[i_wbcand_trim].size(); i++){
      if( m_fatjet_ind[i_wbcand_trim].at(i) == m_ij_hadtop_Wbtrim_cand ){
	m_ij_ind_hadtop_Wbtrim_cand = i;
      }
    }
  }

  if(m_ij_hadtop_Wbhtt_cand >= 0){
    for(int i = 0; i < m_fatjet_ind[i_wbcand_htt].size(); i++){
      if( m_fatjet_ind[i_wbcand_htt].at(i) == m_ij_hadtop_Wbhtt_cand ){
	m_ij_ind_hadtop_Wbhtt_cand = i;
      }
    }
  }
  */
  //cout<<"END entry = "<<entry<<endl<<endl;
  
  if(m_jetcol_ID.find("Truth") == string::npos){

    //Store trigger bits and trigger matching info
    if(EF_mu24i_tight || EF_mu36_tight){t_trig_pass_mu = 1;}
    if(EF_e24vhi_medium1 || EF_e60_medium1){t_trig_pass_el = 1;}
    
    if(nel == 1){
      t_trig_match_el = MatchElectronTrigger(lep_ind);    
    }
    
    if(nmu == 1){
      t_trig_match_mu = MatchMuonTrigger(lep_ind);
    }
  }

  
  if(m_IO & WHIST){  
    if(m_isSignal>0){
      sc += FillPartonHistograms();
      sc += FillMatchedJetHistograms();
    }
    sc += FillJetHistograms();
    //sc += ClearEventTLVs();
  }

  if(m_IO & WTREE){
    /*
      for(map<int, string>::iterator j_it = m_fatjetcol_IDs.begin(); j_it != m_fatjetcol_IDs.end(); ++j_it){
      sc += FillFatJetOutputBranches(j_it->first);
      }
    */

    if(m_isSignal>0){
      sc += FillPartonOutputBranches();
      sc += FillMatchedJetOutputBranches();
    }
    m_outtree->Fill();
  }
  
  // cout<<endl;
  //delete tlv_test;
  return 0;
  
}


int EventRecoAnalysis::Terminate(){
   cout<<"Wrap up"<<endl;
  if(m_IO & WHIST){
    //ClearEventTLVs();
    m_outfile_hist->cd();
    for(map<string,TH1D*>::iterator it=m_h1d.begin(); it!=m_h1d.end(); ++it){
      // cout<<"h1d="<<(it->first).c_str()<<endl;
      (it->second)->Write(); delete it->second; m_h1d.erase(it);
    }
    for(map<string,TH2D*>::iterator it=m_h2d.begin(); it!=m_h2d.end(); ++it){
    // cout<<"h2d="<<(it->first).c_str()<<endl;
      (it->second)->Write(); delete it->second; m_h2d.erase(it);
    }
    m_outfile_hist->Close();
  }
  if(m_IO & WTREE){
    m_outfile_tree->cd();
    m_outtree->Write();
    m_outfile_tree->Close();
  }
  /*
  Py_XDECREF(globalInclCsTA);
  Py_XDECREF(globalExclCsTA);
  Py_DECREF(pModule);
  Py_DECREF(setRhoMuNvertex);
  */
  //delete m_outfile_tree;
  //delete m_outfile_hist;
   cout<<"Done"<<endl;
  //delete m_chain;

  return 0;
}

//0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
//protected section
//0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
bool EventRecoAnalysis::MatchElectronTrigger(int ind){
  bool b_match = false;
  double oeta = el_tracketa->at(ind);
  double ophi = el_trackphi->at(ind);
  TLorentzVector otlv;
  otlv.SetPtEtaPhiM(1., oeta, ophi, 0.);
  
  for(int i = 0; i < trig_EF_el_n; i++){
    //check trigger bit
    if( (trig_EF_el_EF_e24vhi_medium1->at(i) <= 0) && (trig_EF_el_EF_e60_medium1->at(i) <= 0) ){continue;}
    
    double teta = trig_EF_el_eta->at(i);
    double tphi = trig_EF_el_phi->at(i);
    TLorentzVector ttlv;
    ttlv.SetPtEtaPhiM(1., teta, tphi, 0.);
    
    double dr = otlv.DeltaR(ttlv);
    if(dr < 0.15){b_match = true; break;}
  } 

  return b_match;

}

bool EventRecoAnalysis::MatchMuonTrigger(int ind){
  bool b_match = false;

  double opt = mu_pt->at(ind);
  double oeta = mu_eta->at(ind);
  double ophi = mu_phi->at(ind);
  TLorentzVector otlv;
  otlv.SetPtEtaPhiM(1., oeta, ophi, 0.);

  for(int i = 0; i < trig_EF_trigmuonef_n; i++){
    //First check that the trigger bit is passed
    bool pass24trig = trig_EF_trigmuonef_EF_mu24i_tight->at(i) > 0;
    bool pass36trig = trig_EF_trigmuonef_EF_mu36_tight->at(i) > 0;
    if(!( pass24trig || pass36trig )){continue;}
    //Check that at least one track passes the pT hypothesis and matches the offline muon
    for(int itrk = 0; itrk < trig_EF_trigmuonef_track_n->at(i); itrk++){

      double tpt = trig_EF_trigmuonef_track_CB_pt->at(i).at(itrk) / GeV;
      double teta = trig_EF_trigmuonef_track_CB_eta->at(i).at(itrk);
      double tphi = trig_EF_trigmuonef_track_CB_phi->at(i).at(itrk);

      //To pass:
      //(pass 24 && pass 24 hypo) || (pass 36 && pass 36 hypo)
      //Negation:
      //!(pass 24 && pass 24 hypo) && !(pass 36 && pass 36 hypo)
      //(!pass 24 || !pass24hypo) && (!pass 36 || !pass 36 hypo)

      //bool pass24Hypo = pass24trig && ef_mu24Hypo(tpt, teta);
      //bool pass36Hypo = pass36trig && ef_mu36Hypo(tpt, teta);

      bool pass24Hypo = ef_mu24Hypo(fabs(teta), tpt);
      bool pass36Hypo = ef_mu36Hypo(fabs(teta), tpt);

      // cout<<" itrk = "<<itrk<<" tpt = "<<tpt<<" teta = "<<teta<<" pass24Hypo = "<<pass24Hypo<<" pass36Hypo = "<<pass36Hypo<<endl;
      if(! ( (pass24trig && pass24Hypo) || (pass36trig && pass36Hypo) ) ){continue;}
      //if( (trig_EF_trigmuonef_EF_mu24i_tight->at(i) > 0 && !ef_mu24Hypo(tpt, teta))
      //  && (trig_EF_trigmuonef_EF_mu36_tight->at(i) > 0 && !ef_mu36Hypo(tpt, teta)) ){continue;}

      TLorentzVector ttlv;
      ttlv.SetPtEtaPhiM(1., teta, tphi, 0.);
      
      double dr = otlv.DeltaR(ttlv);
       if(dr < 0.15){b_match = true; break;}
    }
    if(b_match){break;}
  }

  return b_match;
}


//trig_EF_trigmuonef
bool EventRecoAnalysis::ef_mu24Hypo(float eta, float pt) {
  
  if(eta < 1.05) {
    if(pt > 23.34) return true;
  } 
  else if(eta < 1.5) {
    if(pt > 23.19) return true;
  } 
  else if(eta < 2.0) {
    if(pt > 23.14) return true;
  } 
  else {
    if(pt > 23.06) return true;
  }
  
  return false;
}

//trig_EF_trigmuonef
bool EventRecoAnalysis::ef_mu36Hypo(float eta, float pt) {
  
  if(eta < 1.05) {
    if(pt > 34.96) return true;
  }
  else if(eta < 1.5) {
    if(pt > 34.78) return true;
  }
  else if(eta < 2.0) {
    if(pt > 34.69) return true;
  }
  else {
    if(pt > 34.63) return true;
  }
  
  return false;
}
  
  


int EventRecoAnalysis::InitEventOutputBranches(){
  //m_outtree->Branch("t_nvtx", &t_nvtx, "t_nvtx/I");
  m_outtree->Branch("t_mcevt_weight", &t_mcevt_weight, "t_mcevt_weight/D");
  //m_outtree->Branch("t_RWPDFWeight", &t_RWPDFWeight, "t_RWPDFWeight/D");
  m_outtree->Branch("t_HF_class", &t_HF_class, "t_HF_class/I");
  /*
  m_outtree->Branch("t_trig_pass_el", &t_trig_pass_el, "t_trig_pass_el/I");
  m_outtree->Branch("t_trig_pass_mu", &t_trig_pass_mu, "t_trig_pass_mu/I");
  m_outtree->Branch("t_trig_match_el", &t_trig_match_el, "t_trig_match_el/I");
  m_outtree->Branch("t_trig_match_mu", &t_trig_match_mu, "t_trig_match_mu/I");

  m_outtree->Branch("t_mu", &t_mu, "t_mu/D");
  m_outtree->Branch("t_Eventshape_rhoKt4LC", &t_Eventshape_rhoKt4LC, "t_Eventshape_rhoKt4LC/D");
  m_outtree->Branch("t_Eventshape_rhoKt6LC", &t_Eventshape_rhoKt6LC, "t_Eventshape_rhoKt6LC/D");
  */


  return 0;
}
int EventRecoAnalysis::InitPartonOutputBranches(){
  m_outtree->Branch("t_p_leptop_pt", &t_p_leptop_pt, "t_p_leptop_pt/D");
  m_outtree->Branch("t_p_leptop_eta", &t_p_leptop_eta, "t_p_leptop_eta/D");
  m_outtree->Branch("t_p_leptop_phi", &t_p_leptop_phi, "t_p_leptop_phi/D");
  m_outtree->Branch("t_p_leptop_m", &t_p_leptop_m, "t_p_leptop_m/D");

  m_outtree->Branch("t_p_leptop_hard_pt", &t_p_leptop_hard_pt, "t_p_leptop_hard_pt/D");
  m_outtree->Branch("t_p_leptop_hard_eta", &t_p_leptop_hard_eta, "t_p_leptop_hard_eta/D");
  m_outtree->Branch("t_p_leptop_hard_phi", &t_p_leptop_hard_phi, "t_p_leptop_hard_phi/D");
  m_outtree->Branch("t_p_leptop_hard_m", &t_p_leptop_hard_m, "t_p_leptop_hard_m/D");

  m_outtree->Branch("t_p_leptop_b_pt", &t_p_leptop_b_pt, "t_p_leptop_b_pt/D");
  m_outtree->Branch("t_p_leptop_b_eta", &t_p_leptop_b_eta, "t_p_leptop_b_eta/D");
  m_outtree->Branch("t_p_leptop_b_phi", &t_p_leptop_b_phi, "t_p_leptop_b_phi/D");
  m_outtree->Branch("t_p_leptop_b_m", &t_p_leptop_b_m, "t_p_leptop_b_m/D");

  m_outtree->Branch("t_p_leptop_W_pt", &t_p_leptop_W_pt, "t_p_leptop_W_pt/D");
  m_outtree->Branch("t_p_leptop_W_eta", &t_p_leptop_W_eta, "t_p_leptop_W_eta/D");
  m_outtree->Branch("t_p_leptop_W_phi", &t_p_leptop_W_phi, "t_p_leptop_W_phi/D");
  m_outtree->Branch("t_p_leptop_W_m", &t_p_leptop_W_m, "t_p_leptop_W_m/D");

  m_outtree->Branch("t_p_leptop_lep_pt", &t_p_leptop_lep_pt, "t_p_leptop_lep_pt/D");
  m_outtree->Branch("t_p_leptop_lep_eta", &t_p_leptop_lep_eta, "t_p_leptop_lep_eta/D");
  m_outtree->Branch("t_p_leptop_lep_phi", &t_p_leptop_lep_phi, "t_p_leptop_lep_phi/D");
  m_outtree->Branch("t_p_leptop_lep_m", &t_p_leptop_lep_m, "t_p_leptop_lep_m/D");
  m_outtree->Branch("t_p_leptop_lep_flav", &t_p_leptop_lep_flav, "t_p_leptop_lep_flav/I");

  m_outtree->Branch("t_p_leptop_nu_pt", &t_p_leptop_nu_pt, "t_p_leptop_nu_pt/D");
  m_outtree->Branch("t_p_leptop_nu_eta", &t_p_leptop_nu_eta, "t_p_leptop_nu_eta/D");
  m_outtree->Branch("t_p_leptop_nu_phi", &t_p_leptop_nu_phi, "t_p_leptop_nu_phi/D");
  m_outtree->Branch("t_p_leptop_nu_m", &t_p_leptop_nu_m, "t_p_leptop_nu_m/D");


  m_outtree->Branch("t_p_hadtop_pt", &t_p_hadtop_pt, "t_p_hadtop_pt/D");
  m_outtree->Branch("t_p_hadtop_eta", &t_p_hadtop_eta, "t_p_hadtop_eta/D");
  m_outtree->Branch("t_p_hadtop_phi", &t_p_hadtop_phi, "t_p_hadtop_phi/D");
  m_outtree->Branch("t_p_hadtop_m", &t_p_hadtop_m, "t_p_hadtop_m/D");

  m_outtree->Branch("t_p_hadtop_hard_pt", &t_p_hadtop_hard_pt, "t_p_hadtop_hard_pt/D");
  m_outtree->Branch("t_p_hadtop_hard_eta", &t_p_hadtop_hard_eta, "t_p_hadtop_hard_eta/D");
  m_outtree->Branch("t_p_hadtop_hard_phi", &t_p_hadtop_hard_phi, "t_p_hadtop_hard_phi/D");
  m_outtree->Branch("t_p_hadtop_hard_m", &t_p_hadtop_hard_m, "t_p_hadtop_hard_m/D");

  m_outtree->Branch("t_p_hadtop_b_pt", &t_p_hadtop_b_pt, "t_p_hadtop_b_pt/D");
  m_outtree->Branch("t_p_hadtop_b_eta", &t_p_hadtop_b_eta, "t_p_hadtop_b_eta/D");
  m_outtree->Branch("t_p_hadtop_b_phi", &t_p_hadtop_b_phi, "t_p_hadtop_b_phi/D");
  m_outtree->Branch("t_p_hadtop_b_m", &t_p_hadtop_b_m, "t_p_hadtop_b_m/D");

  m_outtree->Branch("t_p_hadtop_W_pt", &t_p_hadtop_W_pt, "t_p_hadtop_W_pt/D");
  m_outtree->Branch("t_p_hadtop_W_eta", &t_p_hadtop_W_eta, "t_p_hadtop_W_eta/D");
  m_outtree->Branch("t_p_hadtop_W_phi", &t_p_hadtop_W_phi, "t_p_hadtop_W_phi/D");
  m_outtree->Branch("t_p_hadtop_W_m", &t_p_hadtop_W_m, "t_p_hadtop_W_m/D");

  m_outtree->Branch("t_p_hadtop_q1_pt", &t_p_hadtop_q1_pt, "t_p_hadtop_q1_pt/D");
  m_outtree->Branch("t_p_hadtop_q1_eta", &t_p_hadtop_q1_eta, "t_p_hadtop_q1_eta/D");
  m_outtree->Branch("t_p_hadtop_q1_phi", &t_p_hadtop_q1_phi, "t_p_hadtop_q1_phi/D");
  m_outtree->Branch("t_p_hadtop_q1_m", &t_p_hadtop_q1_m, "t_p_hadtop_q1_m/D");
  m_outtree->Branch("t_p_hadtop_q1_flav", &t_p_hadtop_q1_flav, "t_p_hadtop_q1_flav/I");

  m_outtree->Branch("t_p_hadtop_q2_pt", &t_p_hadtop_q2_pt, "t_p_hadtop_q2_pt/D");
  m_outtree->Branch("t_p_hadtop_q2_eta", &t_p_hadtop_q2_eta, "t_p_hadtop_q2_eta/D");
  m_outtree->Branch("t_p_hadtop_q2_phi", &t_p_hadtop_q2_phi, "t_p_hadtop_q2_phi/D");
  m_outtree->Branch("t_p_hadtop_q2_m", &t_p_hadtop_q2_m, "t_p_hadtop_q2_m/D");
  m_outtree->Branch("t_p_hadtop_q2_flav", &t_p_hadtop_q2_flav, "t_p_hadtop_q2_flav/I");


  m_outtree->Branch("t_p_higgs_pt", &t_p_higgs_pt, "t_p_higgs_pt/D");
  m_outtree->Branch("t_p_higgs_eta", &t_p_higgs_eta, "t_p_higgs_eta/D");
  m_outtree->Branch("t_p_higgs_phi", &t_p_higgs_phi, "t_p_higgs_phi/D");
  m_outtree->Branch("t_p_higgs_m", &t_p_higgs_m, "t_p_higgs_m/D");

  m_outtree->Branch("t_p_higgs_hard_pt", &t_p_higgs_hard_pt, "t_p_higgs_hard_pt/D");
  m_outtree->Branch("t_p_higgs_hard_eta", &t_p_higgs_hard_eta, "t_p_higgs_hard_eta/D");
  m_outtree->Branch("t_p_higgs_hard_phi", &t_p_higgs_hard_phi, "t_p_higgs_hard_phi/D");
  m_outtree->Branch("t_p_higgs_hard_m", &t_p_higgs_hard_m, "t_p_higgs_hard_m/D");

  m_outtree->Branch("t_p_higgs_b1_pt", &t_p_higgs_b1_pt, "t_p_higgs_b1_pt/D");
  m_outtree->Branch("t_p_higgs_b1_eta", &t_p_higgs_b1_eta, "t_p_higgs_b1_eta/D");
  m_outtree->Branch("t_p_higgs_b1_phi", &t_p_higgs_b1_phi, "t_p_higgs_b1_phi/D");
  m_outtree->Branch("t_p_higgs_b1_m", &t_p_higgs_b1_m, "t_p_higgs_b1_m/D");

  m_outtree->Branch("t_p_higgs_b2_pt", &t_p_higgs_b2_pt, "t_p_higgs_b2_pt/D");
  m_outtree->Branch("t_p_higgs_b2_eta", &t_p_higgs_b2_eta, "t_p_higgs_b2_eta/D");
  m_outtree->Branch("t_p_higgs_b2_phi", &t_p_higgs_b2_phi, "t_p_higgs_b2_phi/D");
  m_outtree->Branch("t_p_higgs_b2_m", &t_p_higgs_b2_m, "t_p_higgs_b2_m/D");

  return 0;
}


int EventRecoAnalysis::InitJetOutputBranches(){
  m_outtree->Branch("t_j_leptop_b_ind", &t_j_leptop_b_ind, "t_j_leptop_b_ind/I");
  m_outtree->Branch("t_pj_leptop_b_dr", &t_pj_leptop_b_dr, "t_pj_leptop_b_dr/D");
  

  m_outtree->Branch("t_j_leptop_lep_pt", &t_j_leptop_lep_pt, "t_j_leptop_lep_pt/D");
  m_outtree->Branch("t_j_leptop_lep_eta", &t_j_leptop_lep_eta, "t_j_leptop_lep_eta/D");
  m_outtree->Branch("t_j_leptop_lep_phi", &t_j_leptop_lep_phi, "t_j_leptop_lep_phi/D");
  m_outtree->Branch("t_j_leptop_lep_m", &t_j_leptop_lep_m, "t_j_leptop_lep_m/D");
  m_outtree->Branch("t_j_leptop_lep_flav", &t_j_leptop_lep_flav, "t_j_leptop_lep_flav/I");
  m_outtree->Branch("t_j_leptop_lep_charge", &t_j_leptop_lep_charge, "t_j_leptop_lep_charge/D");

  m_outtree->Branch("t_j_leptop_nu_pt", &t_j_leptop_nu_pt, "t_j_leptop_nu_pt/D");
  m_outtree->Branch("t_j_leptop_nu_phi", &t_j_leptop_nu_phi, "t_j_leptop_nu_phi/D");

  m_outtree->Branch("t_pj_leptop_lep_dr", &t_pj_leptop_lep_dr, "t_pj_leptop_lep_dr/D");
  m_outtree->Branch("t_pj_leptop_nu_dphi", &t_pj_leptop_nu_dphi, "t_pj_leptop_nu_dphi/D");
  
  m_outtree->Branch("t_j_hadtop_b_ind", &t_j_hadtop_b_ind, "t_j_hadtop_b_ind/I");
  m_outtree->Branch("t_pj_hadtop_b_dr", &t_pj_hadtop_b_dr, "t_pj_hadtop_b_dr/D");

  m_outtree->Branch("t_j_hadtop_q1_ind", &t_j_hadtop_q1_ind, "t_j_hadtop_q1_ind/I");
  m_outtree->Branch("t_pj_hadtop_q1_dr", &t_pj_hadtop_q1_dr, "t_pj_hadtop_q1_dr/D");

  m_outtree->Branch("t_j_hadtop_q2_ind", &t_j_hadtop_q2_ind, "t_j_hadtop_q2_ind/I");
  m_outtree->Branch("t_pj_hadtop_q2_dr", &t_pj_hadtop_q2_dr, "t_pj_hadtop_q2_dr/D");

  m_outtree->Branch("t_j_higgs_b1_ind", &t_j_higgs_b1_ind, "t_j_higgs_b1_ind/I");
  m_outtree->Branch("t_pj_higgs_b1_dr", &t_pj_higgs_b1_dr, "t_pj_higgs_b1_dr/D");

  m_outtree->Branch("t_j_higgs_b2_ind", &t_j_higgs_b2_ind, "t_j_higgs_b2_ind/I");
  m_outtree->Branch("t_pj_higgs_b2_dr", &t_pj_higgs_b2_dr, "t_pj_higgs_b2_dr/D");
  /*
  m_outtree->Branch("t_j_hadtop_nolepb_cand_ind", &t_j_hadtop_nolepb_cand_ind, "t_j_hadtop_nolepb_cand_ind/I");
  m_outtree->Branch("t_j_hadtop_truth_cand_ind", &t_j_hadtop_truth_cand_ind, "t_j_hadtop_truth_cand_ind/I");
  m_outtree->Branch("t_j_hadtop_Wbhtt_cand_ind", &t_j_hadtop_Wbhtt_cand_ind, "t_j_hadtop_Wbhtt_cand_ind/I");
  m_outtree->Branch("t_j_hadtop_Wbtrim_cand_ind", &t_j_hadtop_Wbtrim_cand_ind, "t_j_hadtop_Wbtrim_cand_ind/I");
  m_outtree->Branch("t_j_hadW_cand_ind", &t_j_hadW_cand_ind, "t_j_hadW_cand_ind/I");
  */


  t_jets_pt = new vector<float>;
  t_jets_eta = new vector<float>;
  t_jets_phi = new vector<float>;
  t_jets_m = new vector<float>;
  t_jets_MV1 = new vector<float>;
  t_jets_trueflavor=new vector<float>;
  t_jets_ntrack = new vector<int>;
  t_jets_trackwidth = new vector<float>;
  t_jets_charge = new vector<float>;

  m_outtree->Branch("t_jets_pt", &t_jets_pt);
  m_outtree->Branch("t_jets_eta", &t_jets_eta);
  m_outtree->Branch("t_jets_phi", &t_jets_phi);
  m_outtree->Branch("t_jets_m", &t_jets_m);
  m_outtree->Branch("t_jets_MV1", &t_jets_MV1);
  m_outtree->Branch("t_jets_trueflavor", &t_jets_trueflavor);
  /*
  m_outtree->Branch("t_jets_ntrack", &t_jets_ntrack);
  m_outtree->Branch("t_jets_trackwidth", &t_jets_trackwidth);
  m_outtree->Branch("t_jets_charge", &t_jets_charge);
  */

  return 0;
}

int EventRecoAnalysis::InitFatJetOutputBranches(int sfj_key){
  string sfj = m_fatjetcol_IDs[sfj_key];

  t_fatjets_n[sfj_key] = 0;
  t_fatjets_pt[sfj_key] = new vector<float>;
  t_fatjets_eta[sfj_key] = new vector<float>;
  t_fatjets_phi[sfj_key] = new vector<float>;
  t_fatjets_m[sfj_key] = new vector<float>;
  t_fatjets_constscale_pt[sfj_key] = new vector<float>;
  t_fatjets_constscale_eta[sfj_key] = new vector<float>;
  t_fatjets_constscale_phi[sfj_key] = new vector<float>;
  t_fatjets_constscale_m[sfj_key] = new vector<float>;
  t_fatjets_ActiveArea[sfj_key] = new vector<float>;
  t_fatjets_ActiveArea_px[sfj_key] = new vector<float>;
  t_fatjets_ActiveArea_py[sfj_key] = new vector<float>;
  t_fatjets_ActiveArea_pz[sfj_key] = new vector<float>;
  t_fatjets_ActiveArea_E[sfj_key] = new vector<float>;
      
  t_fatjets_SPLIT12[sfj_key] = new vector<float>;
  t_fatjets_SPLIT23[sfj_key] = new vector<float>;
  t_fatjets_ZCUT12[sfj_key] = new vector<float>;
  t_fatjets_ZCUT23[sfj_key] = new vector<float>;
  t_fatjets_WIDTH[sfj_key] = new vector<float>;
  t_fatjets_PlanarFlow[sfj_key] = new vector<float>;
  t_fatjets_Angularity[sfj_key] = new vector<float>;
  t_fatjets_PullMag[sfj_key] = new vector<float>;
  t_fatjets_PullPhi[sfj_key] = new vector<float>;
  t_fatjets_Tau1[sfj_key] = new vector<float>;
  t_fatjets_Tau2[sfj_key] = new vector<float>;
  t_fatjets_Tau3[sfj_key] = new vector<float>;
  t_fatjets_nbtag[sfj_key] = new vector<int>;
  t_fatjets_nbtag_geom[sfj_key] = new vector<int>;
  t_fatjets_MCclass[sfj_key] = new vector<int>;
  t_fatjets_orig_ind[sfj_key] = new vector<int>;
  t_fatjets_assoc_ind[sfj_key] = new vector<int>;
  t_fatjets_truth_label[sfj_key] = new vector<int>;


  m_outtree->Branch(Form("t_fatjets_%s_n",sfj.c_str()), &t_fatjets_n[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_pt",sfj.c_str()), &t_fatjets_pt[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_eta",sfj.c_str()), &t_fatjets_eta[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_phi",sfj.c_str()), &t_fatjets_phi[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_m",sfj.c_str()), &t_fatjets_m[sfj_key]);
  /*
  m_outtree->Branch(Form("t_fatjets_%s_constscale_pt",sfj.c_str()), &t_fatjets_constscale_pt[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_constscale_eta",sfj.c_str()), &t_fatjets_constscale_eta[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_constscale_phi",sfj.c_str()), &t_fatjets_constscale_phi[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_constscale_m",sfj.c_str()), &t_fatjets_constscale_m[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_ActiveArea",sfj.c_str()), &t_fatjets_ActiveArea[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_ActiveArea_px",sfj.c_str()), &t_fatjets_ActiveArea_px[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_ActiveArea_py",sfj.c_str()), &t_fatjets_ActiveArea_py[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_ActiveArea_pz",sfj.c_str()), &t_fatjets_ActiveArea_pz[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_ActiveArea_E",sfj.c_str()), &t_fatjets_ActiveArea_E[sfj_key]);
  */
  m_outtree->Branch(Form("t_fatjets_%s_SPLIT12",sfj.c_str()), &t_fatjets_SPLIT12[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_SPLIT23",sfj.c_str()), &t_fatjets_SPLIT23[sfj_key]);
  /*
  m_outtree->Branch(Form("t_fatjets_%s_ZCUT12",sfj.c_str()), &t_fatjets_ZCUT12[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_ZCUT23",sfj.c_str()), &t_fatjets_ZCUT23[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_WIDTH",sfj.c_str()), &t_fatjets_WIDTH[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_PlanarFlow",sfj.c_str()), &t_fatjets_PlanarFlow[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_Angularity",sfj.c_str()), &t_fatjets_Angularity[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_PullMag",sfj.c_str()), &t_fatjets_PullMag[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_PullPhi",sfj.c_str()), &t_fatjets_PullPhi[sfj_key]);
  */
  m_outtree->Branch(Form("t_fatjets_%s_Tau1",sfj.c_str()), &t_fatjets_Tau1[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_Tau2",sfj.c_str()), &t_fatjets_Tau2[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_Tau3",sfj.c_str()), &t_fatjets_Tau3[sfj_key]);
  /*
  m_outtree->Branch(Form("t_fatjets_%s_nbtag",sfj.c_str()), &t_fatjets_nbtag[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_nbtag_geom",sfj.c_str()), &t_fatjets_nbtag_geom[sfj_key]);
  */
  m_outtree->Branch(Form("t_fatjets_%s_MCclass",sfj.c_str()), &t_fatjets_MCclass[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_orig_ind",sfj.c_str()), &t_fatjets_orig_ind[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_assoc_ind",sfj.c_str()), &t_fatjets_assoc_ind[sfj_key]);
  m_outtree->Branch(Form("t_fatjets_%s_truth_label",sfj.c_str()), &t_fatjets_truth_label[sfj_key]);


  sfj.clear();
  return 0;
}

int EventRecoAnalysis::InitEventBranches(){
  int b_stat = 0;

  b_stat += m_chain->SetBranchAddress("mc_n", &mc_n, &b_mc_n);
  b_stat += m_chain->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
  b_stat += m_chain->SetBranchAddress("mc_m", &mc_m, &b_mc_m);
  b_stat += m_chain->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
  b_stat += m_chain->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
  b_stat += m_chain->SetBranchAddress("mc_status", &mc_status, &b_mc_status);
  b_stat += m_chain->SetBranchAddress("mc_barcode", &mc_barcode, &b_mc_barcode);
  b_stat += m_chain->SetBranchAddress("mc_pdgId", &mc_pdgId, &b_mc_pdgId);
  b_stat += m_chain->SetBranchAddress("mc_charge", &mc_charge, &b_mc_charge);
  b_stat += m_chain->SetBranchAddress("mc_parent_index", &mc_parent_index, &b_mc_parent_index);
  b_stat += m_chain->SetBranchAddress("mc_child_index", &mc_child_index, &b_mc_child_index);

  b_stat += m_chain->SetBranchAddress("mcevt_weight", &mcevt_weight, &b_mcevt_weight);
  b_stat += m_chain->SetBranchAddress("mcevt_pdf1", &mcevt_pdf1, &b_mcevt_pdf1);
  b_stat += m_chain->SetBranchAddress("mcevt_pdf2", &mcevt_pdf2, &b_mcevt_pdf2);
  b_stat += m_chain->SetBranchAddress("mcevt_pdf_id1", &mcevt_pdf_id1, &b_mcevt_pdf_id1);
  b_stat += m_chain->SetBranchAddress("mcevt_pdf_id2", &mcevt_pdf_id2, &b_mcevt_pdf_id2);
  b_stat += m_chain->SetBranchAddress("mcevt_pdf_scale", &mcevt_pdf_scale, &b_mcevt_pdf_scale);
  b_stat += m_chain->SetBranchAddress("mcevt_pdf_x1", &mcevt_pdf_x1, &b_mcevt_pdf_x1);
  b_stat += m_chain->SetBranchAddress("mcevt_pdf_x2", &mcevt_pdf_x2, &b_mcevt_pdf_x2);

  b_stat += m_chain->SetBranchAddress("cl_n", &cl_n, &b_cl_n);
  b_stat += m_chain->SetBranchAddress("cl_lc_pt", &cl_pt, &b_cl_pt);
  b_stat += m_chain->SetBranchAddress("cl_centermag", &cl_centermag, &b_cl_centermag);
  b_stat += m_chain->SetBranchAddress("cl_lc_eta", &cl_eta, &b_cl_eta);
  b_stat += m_chain->SetBranchAddress("cl_lc_phi", &cl_phi, &b_cl_phi);

  b_stat += m_chain->SetBranchAddress("larError", &larError, &b_larError);
  b_stat += m_chain->SetBranchAddress("tileError", &tileError, &b_tileError);
  b_stat += m_chain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);

  b_stat += m_chain->SetBranchAddress("MET_RefFinal_et", &MET_et, &b_MET_et);
  b_stat += m_chain->SetBranchAddress("MET_RefFinal_phi", &MET_phi, &b_MET_phi);


  b_stat += m_chain->SetBranchAddress("RunNumber", &runNumber, &b_runNumber);
  b_stat += m_chain->SetBranchAddress("EventNumber", &eventNumber, &b_eventNumber);
  b_stat += m_chain->SetBranchAddress("mc_channel_number", &channelNumber, &b_channelNumber);
  b_stat += m_chain->SetBranchAddress("averageIntPerXing", &mu, &b_mu);
  b_stat += m_chain->SetBranchAddress("vxp_n", &vxp_n, &b_vxp_n);
  b_stat += m_chain->SetBranchAddress("vxp_nTracks", &vxp_nTracks, &b_vxp_nTracks);
  b_stat += m_chain->SetBranchAddress("vxp_type", &vxp_type, &b_vxp_type);

  b_stat += m_chain->SetBranchAddress("EF_mu24i_tight", &EF_mu24i_tight, &b_EF_mu24i_tight);
  b_stat += m_chain->SetBranchAddress("EF_mu36_tight", &EF_mu36_tight, &b_EF_mu36_tight);
  b_stat += m_chain->SetBranchAddress("EF_e24vhi_medium1", &EF_e24vhi_medium1, &b_EF_e24vhi_medium1);
  b_stat += m_chain->SetBranchAddress("EF_e60_medium1", &EF_e60_medium1, &b_EF_e60_medium1);

  b_stat += m_chain->SetBranchAddress("trig_EF_el_EF_e24vhi_medium1", &trig_EF_el_EF_e24vhi_medium1, &b_trig_EF_el_EF_e24vhi_medium1);
  b_stat += m_chain->SetBranchAddress("trig_EF_el_EF_e60_medium1", &trig_EF_el_EF_e60_medium1, &b_trig_EF_el_EF_e60_medium1);
  b_stat += m_chain->SetBranchAddress("trig_EF_el_n", &trig_EF_el_n, &b_trig_EF_el_n);
  b_stat += m_chain->SetBranchAddress("trig_EF_el_eta", &trig_EF_el_eta, &b_trig_EF_el_eta);
  b_stat += m_chain->SetBranchAddress("trig_EF_el_phi", &trig_EF_el_phi, &b_trig_EF_el_phi);

  b_stat += m_chain->SetBranchAddress("trig_EF_trigmuonef_EF_mu24i_tight", &trig_EF_trigmuonef_EF_mu24i_tight, &b_trig_EF_trigmuonef_EF_mu24i_tight);
  b_stat += m_chain->SetBranchAddress("trig_EF_trigmuonef_EF_mu36_tight", &trig_EF_trigmuonef_EF_mu36_tight, &b_trig_EF_trigmuonef_EF_mu36_tight);
  b_stat += m_chain->SetBranchAddress("trig_EF_trigmuonef_n", &trig_EF_trigmuonef_n, &b_trig_EF_trigmuonef_n);
  b_stat += m_chain->SetBranchAddress("trig_EF_trigmuonef_track_n", &trig_EF_trigmuonef_track_n, &b_trig_EF_trigmuonef_track_n);
  b_stat += m_chain->SetBranchAddress("trig_EF_trigmuonef_track_CB_pt", &trig_EF_trigmuonef_track_CB_pt, &b_trig_EF_trigmuonef_track_CB_pt);
  b_stat += m_chain->SetBranchAddress("trig_EF_trigmuonef_track_CB_eta", &trig_EF_trigmuonef_track_CB_eta, &b_trig_EF_trigmuonef_track_CB_eta);
  b_stat += m_chain->SetBranchAddress("trig_EF_trigmuonef_track_CB_phi", &trig_EF_trigmuonef_track_CB_phi, &b_trig_EF_trigmuonef_track_CB_phi);

  /*
  b_stat += m_chain->SetBranchAddress("el_n", &el_n, &b_el_n);
  b_stat += m_chain->SetBranchAddress("el_E", &el_E, &b_el_E);
  b_stat += m_chain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
  b_stat += m_chain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
  b_stat += m_chain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
  b_stat += m_chain->SetBranchAddress("el_m", &el_m, &b_el_m);
  b_stat += m_chain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
  */
  b_stat += m_chain->SetBranchAddress("el_Et", &el_Et, &b_el_Et);
  b_stat += m_chain->SetBranchAddress("el_author", &el_author, &b_el_author);
  b_stat += m_chain->SetBranchAddress("el_tightPP", &el_tightPP, &b_el_tightPP);
  b_stat += m_chain->SetBranchAddress("el_trackz0pvunbiased", &el_trackz0pvunbiased, &b_el_trackz0pvunbiased);
  b_stat += m_chain->SetBranchAddress("el_tracketa", &el_tracketa, &b_el_tracketa);
  b_stat += m_chain->SetBranchAddress("el_trackphi", &el_trackphi, &b_el_trackphi);
  b_stat += m_chain->SetBranchAddress("el_cl_E", &el_cl_E, &b_el_cl_E);
  b_stat += m_chain->SetBranchAddress("el_cl_pt", &el_cl_pt, &b_el_cl_pt);
  b_stat += m_chain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
  b_stat += m_chain->SetBranchAddress("el_cl_phi", &el_cl_phi, &b_el_cl_phi);
  b_stat += m_chain->SetBranchAddress("el_OQ", &el_OQ, &b_el_OQ);
  b_stat += m_chain->SetBranchAddress("el_etap", &el_etap, &b_el_etap);
  b_stat += m_chain->SetBranchAddress("el_etas2", &el_etas2, &b_el_etas2);
  b_stat += m_chain->SetBranchAddress("el_MI10_max40_ptsum", &el_MI10_max40_ptsum, &b_el_MI10_max40_ptsum);

  /*
  b_stat += m_chain->SetBranchAddress("mu_n", &mu_n, &b_mu_n);
  b_stat += m_chain->SetBranchAddress("mu_E", &mu_E, &b_mu_E);
  b_stat += m_chain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
  b_stat += m_chain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
  b_stat += m_chain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
  b_stat += m_chain->SetBranchAddress("mu_m", &mu_m, &b_mu_m);
  b_stat += m_chain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
  */
  b_stat += m_chain->SetBranchAddress("mu_author", &mu_author, &b_mu_author);
  b_stat += m_chain->SetBranchAddress("mu_tight", &mu_tight, &b_mu_tight);
  b_stat += m_chain->SetBranchAddress("mu_nPixHits", &mu_nPixHits, &b_mu_nPixHits);
  b_stat += m_chain->SetBranchAddress("mu_nSCTHits", &mu_nSCTHits, &b_mu_nSCTHits);
  b_stat += m_chain->SetBranchAddress("mu_nTRTHits", &mu_nTRTHits, &b_mu_nTRTHits);
  b_stat += m_chain->SetBranchAddress("mu_nPixHoles", &mu_nPixHoles, &b_mu_nPixHoles);
  b_stat += m_chain->SetBranchAddress("mu_nSCTHoles", &mu_nSCTHoles, &b_mu_nSCTHoles);
  b_stat += m_chain->SetBranchAddress("mu_nTRTOutliers", &mu_nTRTOutliers, &b_mu_nTRTOutliers);
  b_stat += m_chain->SetBranchAddress("mu_nPixelDeadSensors", &mu_nPixelDeadSensors, &b_mu_nPixelDeadSensors);
  b_stat += m_chain->SetBranchAddress("mu_nSCTDeadSensors", &mu_nSCTDeadSensors, &b_mu_nSCTDeadSensors);
  b_stat += m_chain->SetBranchAddress("mu_id_z0_exPV", &mu_id_z0_exPV, &b_mu_id_z0_exPV);
  b_stat += m_chain->SetBranchAddress("mu_MI10_max40_ptsum", &mu_MI10_max40_ptsum, &b_mu_MI10_max40_ptsum);
  
   cout<<"EVENT b_stat = "<<b_stat<<endl;
  return 0;
}

int EventRecoAnalysis::InitJetBranches(string sj){
  int b_stat = 0;

  b_stat += m_chain->SetBranchAddress(Form("%s_n",sj.c_str()), &jet_n, &b_jet_n);
  b_stat += m_chain->SetBranchAddress(Form("%s_pt",sj.c_str()), &jet_pt, &b_jet_pt);
  b_stat += m_chain->SetBranchAddress(Form("%s_eta",sj.c_str()), &jet_eta, &b_jet_eta);
  b_stat += m_chain->SetBranchAddress(Form("%s_phi",sj.c_str()), &jet_phi, &b_jet_phi);
  b_stat += m_chain->SetBranchAddress(Form("%s_E",sj.c_str()), &jet_E, &b_jet_E);
  b_stat += m_chain->SetBranchAddress(Form("%s_constit_n",sj.c_str()), &jet_constit_n, &b_jet_constit_n);
  b_stat += m_chain->SetBranchAddress(Form("%s_constit_index",sj.c_str()), &jet_constit_index, &b_jet_constit_index);
  if(sj.find("Truth")==string::npos){
    b_stat += m_chain->SetBranchAddress(Form("%s_jvtxf",sj.c_str()), &jet_jvf, &b_jet_jvf);
    b_stat += m_chain->SetBranchAddress(Form("%s_flavor_weight_MV1",sj.c_str()), &jet_MV1, &b_jet_MV1);
    b_stat += m_chain->SetBranchAddress(Form("%s_flavor_truth_label",sj.c_str()), &jet_trueflav, &b_jet_trueflav);
    b_stat += m_chain->SetBranchAddress(Form("%s_nTrk_pv0_1GeV",sj.c_str()), &jet_nTrk_pv0_1GeV, &b_jet_nTrk_pv0_1GeV);
    b_stat += m_chain->SetBranchAddress(Form("%s_trackWIDTH_pv0_1GeV",sj.c_str()), &jet_trackWIDTH_pv0_1GeV, &b_jet_trackWIDTH_pv0_1GeV);
  }
   cout<<"JET b_stat = "<<b_stat<<endl;
  return 0;
}



int EventRecoAnalysis::InitFatJetBranches(int sfj_key){
  string sfj = m_fatjetcol_IDs[sfj_key];

  fatjet_n[sfj_key] = 0;
  fatjet_pt[sfj_key] = 0;
  fatjet_eta[sfj_key] = 0;
  fatjet_phi[sfj_key] = 0;
  fatjet_m[sfj_key] = 0;
  fatjet_constscale_pt[sfj_key] = 0;
  fatjet_constscale_eta[sfj_key] = 0;
  fatjet_constscale_phi[sfj_key] = 0;
  fatjet_constscale_m[sfj_key] = 0;
  fatjet_ActiveArea[sfj_key] = 0;
  fatjet_ActiveArea_px[sfj_key] = 0;
  fatjet_ActiveArea_py[sfj_key] = 0;
  fatjet_ActiveArea_pz[sfj_key] = 0;
  fatjet_ActiveArea_E[sfj_key] = 0;

  fatjet_SPLIT12[sfj_key] = 0;
  fatjet_SPLIT23[sfj_key] = 0;
  fatjet_ZCUT12[sfj_key] = 0;
  fatjet_ZCUT23[sfj_key] = 0;
  fatjet_WIDTH[sfj_key] = 0;
  fatjet_PlanarFlow[sfj_key] = 0;
  fatjet_Angularity[sfj_key] = 0;
  fatjet_PullMag[sfj_key] = 0;
  fatjet_PullPhi[sfj_key] = 0;
  fatjet_Tau1[sfj_key] = 0;
  fatjet_Tau2[sfj_key] = 0;
  fatjet_Tau3[sfj_key] = 0;
  fatjet_nbtag[sfj_key] = 0;
  fatjet_nbtag_geom[sfj_key] = 0;
  fatjet_MCclass[sfj_key] = 0;
  fatjet_orig_ind[sfj_key] = 0;
  fatjet_assoc_ind[sfj_key] = 0;

  fatjet_constit_n[sfj_key] = 0;
  fatjet_constit_index[sfj_key] = 0;

  //
  b_fatjet_n[sfj_key] = 0;
  b_fatjet_pt[sfj_key] = 0;
  b_fatjet_eta[sfj_key] = 0;
  b_fatjet_phi[sfj_key] = 0;
  b_fatjet_m[sfj_key] = 0;
  b_fatjet_constscale_pt[sfj_key] = 0;
  b_fatjet_constscale_eta[sfj_key] = 0;
  b_fatjet_constscale_phi[sfj_key] = 0;
  b_fatjet_constscale_m[sfj_key] = 0;
  b_fatjet_ActiveArea[sfj_key] = 0;
  b_fatjet_ActiveArea_px[sfj_key] = 0;
  b_fatjet_ActiveArea_py[sfj_key] = 0;
  b_fatjet_ActiveArea_pz[sfj_key] = 0;
  b_fatjet_ActiveArea_E[sfj_key] = 0;

  b_fatjet_SPLIT12[sfj_key] = 0;
  b_fatjet_SPLIT23[sfj_key] = 0;
  b_fatjet_ZCUT12[sfj_key] = 0;
  b_fatjet_ZCUT23[sfj_key] = 0;
  b_fatjet_WIDTH[sfj_key] = 0;
  b_fatjet_PlanarFlow[sfj_key] = 0;
  b_fatjet_Angularity[sfj_key] = 0;
  b_fatjet_PullMag[sfj_key] = 0;
  b_fatjet_PullPhi[sfj_key] = 0;
  b_fatjet_Tau1[sfj_key] = 0;
  b_fatjet_Tau2[sfj_key] = 0;
  b_fatjet_Tau3[sfj_key] = 0;
  b_fatjet_nbtag[sfj_key] = 0;
  b_fatjet_nbtag_geom[sfj_key] = 0;
  b_fatjet_MCclass[sfj_key] = 0;
  b_fatjet_orig_ind[sfj_key] = 0;
  b_fatjet_assoc_ind[sfj_key] = 0;

  b_fatjet_constit_n[sfj_key] = 0;
  b_fatjet_constit_index[sfj_key] = 0;
  //

  if(sfj.find("Rebuild")==string::npos){
    m_chain->SetBranchAddress(Form("%s_n",sfj.c_str()), &fatjet_n[sfj_key], &b_fatjet_n[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_pt",sfj.c_str()), &fatjet_pt[sfj_key], &b_fatjet_pt[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_eta",sfj.c_str()), &fatjet_eta[sfj_key], &b_fatjet_eta[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_phi",sfj.c_str()), &fatjet_phi[sfj_key], &b_fatjet_phi[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_m",sfj.c_str()), &fatjet_m[sfj_key], &b_fatjet_m[sfj_key]);
    if( (sfj.find("Truth")==string::npos) && (sfj.find("Track")==string::npos) ){ 
      m_chain->SetBranchAddress(Form("%s_constscale_pt",sfj.c_str()), &fatjet_constscale_pt[sfj_key], &b_fatjet_constscale_pt[sfj_key]);
      m_chain->SetBranchAddress(Form("%s_constscale_eta",sfj.c_str()), &fatjet_constscale_eta[sfj_key], &b_fatjet_constscale_eta[sfj_key]);
      m_chain->SetBranchAddress(Form("%s_constscale_phi",sfj.c_str()), &fatjet_constscale_phi[sfj_key], &b_fatjet_constscale_phi[sfj_key]);
      m_chain->SetBranchAddress(Form("%s_constscale_m",sfj.c_str()), &fatjet_constscale_m[sfj_key], &b_fatjet_constscale_m[sfj_key]);
    }
    m_chain->SetBranchAddress(Form("%s_ActiveArea",sfj.c_str()), &fatjet_ActiveArea[sfj_key], &b_fatjet_ActiveArea[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_ActiveArea_px",sfj.c_str()), &fatjet_ActiveArea_px[sfj_key], &b_fatjet_ActiveArea_px[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_ActiveArea_py",sfj.c_str()), &fatjet_ActiveArea_py[sfj_key], &b_fatjet_ActiveArea_py[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_ActiveArea_pz",sfj.c_str()), &fatjet_ActiveArea_pz[sfj_key], &b_fatjet_ActiveArea_pz[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_ActiveArea_e",sfj.c_str()), &fatjet_ActiveArea_E[sfj_key], &b_fatjet_ActiveArea_E[sfj_key]);

    m_chain->SetBranchAddress(Form("%s_SPLIT12",sfj.c_str()), &fatjet_SPLIT12[sfj_key], &b_fatjet_SPLIT12[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_SPLIT23",sfj.c_str()), &fatjet_SPLIT23[sfj_key], &b_fatjet_SPLIT23[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_ZCUT12",sfj.c_str()), &fatjet_ZCUT12[sfj_key], &b_fatjet_ZCUT12[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_ZCUT23",sfj.c_str()), &fatjet_ZCUT23[sfj_key], &b_fatjet_ZCUT23[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_WIDTH",sfj.c_str()), &fatjet_WIDTH[sfj_key], &b_fatjet_WIDTH[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_PlanarFlow",sfj.c_str()), &fatjet_PlanarFlow[sfj_key], &b_fatjet_PlanarFlow[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_Angularity",sfj.c_str()), &fatjet_Angularity[sfj_key], &b_fatjet_Angularity[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_PullMag",sfj.c_str()), &fatjet_PullMag[sfj_key], &b_fatjet_PullMag[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_PullPhi",sfj.c_str()), &fatjet_PullPhi[sfj_key], &b_fatjet_PullPhi[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_Tau1",sfj.c_str()), &fatjet_Tau1[sfj_key], &b_fatjet_Tau1[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_Tau2",sfj.c_str()), &fatjet_Tau2[sfj_key], &b_fatjet_Tau2[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_Tau3",sfj.c_str()), &fatjet_Tau3[sfj_key], &b_fatjet_Tau3[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_constit_n",sfj.c_str()), &fatjet_constit_n[sfj_key], &b_fatjet_constit_n[sfj_key]);
    m_chain->SetBranchAddress(Form("%s_constit_index",sfj.c_str()), &fatjet_constit_index[sfj_key], &b_fatjet_constit_index[sfj_key]);

    fatjet_nbtag[sfj_key] = new vector<int>; fatjet_nbtag[sfj_key]->clear();
    fatjet_nbtag_geom[sfj_key] = new vector<int>; fatjet_nbtag_geom[sfj_key]->clear();
    fatjet_MCclass[sfj_key] = new vector<int>; fatjet_MCclass[sfj_key]->clear();
    fatjet_orig_ind[sfj_key] = new vector<int>; fatjet_orig_ind[sfj_key]->clear();
    fatjet_assoc_ind[sfj_key] = new vector<int>; fatjet_assoc_ind[sfj_key]->clear();

  }

  sfj.clear();
  return 0;
}

void EventRecoAnalysis::BookTH1D(const char* name, const char* title, double binsize, double xlow, double xup,
		       const char* xtitle, const char* ytitle, int lw, int lc){
  int nbins=(int)((xup-xlow)/binsize);
  TH1D* h1=new TH1D(name, title, nbins,xlow,xup);
  h1->GetXaxis()->SetTitle(xtitle); h1->GetYaxis()->SetTitle(ytitle);
  h1->SetLineWidth(lw); h1->SetLineColor(lc);
  h1->Sumw2();
  string s=name; m_h1d[s]=h1; s.clear();
  return;
}

void EventRecoAnalysis::BookTH1D(const char* name, const char* title, int nbins, double* xedges,
		       const char* xtitle, const char* ytitle, int lw, int lc){
  TH1D* h1=new TH1D(name, title, nbins,xedges);
  h1->GetXaxis()->SetTitle(xtitle); h1->GetYaxis()->SetTitle(ytitle);
  h1->SetLineWidth(lw); h1->SetLineColor(lc);
  h1->Sumw2();
  return;
  string s=name; m_h1d[s]=h1; s.clear();
}

void EventRecoAnalysis::BookTH2D(const char* name, const char* title, double xbinsize, double xlow, double xup,
		       double ybinsize, double ylow, double yup,
		       const char* xtitle, const char* ytitle, int lw, int lc){

  int nxbins=(int)((xup-xlow)/xbinsize);
  int nybins=(int)((yup-ylow)/ybinsize);

  TH2D* h2=new TH2D(name, title, nxbins,xlow,xup, nybins,ylow,yup);
  h2->GetXaxis()->SetTitle(xtitle); h2->GetYaxis()->SetTitle(ytitle);
  h2->SetLineWidth(lw); h2->SetLineColor(lc);
  h2->Sumw2();
  string s=name; m_h2d[s]=h2; s.clear();
  return;
}

void EventRecoAnalysis::BookTH2D(const char* name, const char* title,
		       int nxbins, double* xedges, int nybins, double* yedges,
		       const char* xtitle, const char* ytitle, int lw, int lc){

  TH2D* h2=new TH2D(name, title, nxbins,xedges, nybins,yedges);
  h2->GetXaxis()->SetTitle(xtitle); h2->GetYaxis()->SetTitle(ytitle);
  h2->SetLineWidth(lw); h2->SetLineColor(lc);
  h2->Sumw2();
  string s=name; m_h2d[s]=h2; s.clear();
  return;
}

void EventRecoAnalysis::BookTH2D(const char* name, const char* title, double xbinsize, double xlow, double xup,
		       int nybins, double* yedges,
		       const char* xtitle, const char* ytitle, int lw, int lc){

  int nxbins=(int)((xup-xlow)/xbinsize);

  TH2D* h2=new TH2D(name, title, nxbins,xlow,xup, nybins,yedges);
  h2->GetXaxis()->SetTitle(xtitle); h2->GetYaxis()->SetTitle(ytitle);
  h2->SetLineWidth(lw); h2->SetLineColor(lc);
  h2->Sumw2();
  string s=name; m_h2d[s]=h2; s.clear();
  return;
}


void EventRecoAnalysis::FillTH1D(string hname, double val){
  if(m_h1d[hname]!=0){m_h1d[hname]->Fill(val,m_ev_wgt);}
  else{m_h1d.erase(m_h1d.find(hname));  cout<<"Error: TH1D "<<hname<<" not found to fill"<<endl;}
  return;
}
void EventRecoAnalysis::FillTH2D(string hname, double val1, double val2){
  if(m_h2d[hname]!=0){m_h2d[hname]->Fill(val1,val2,m_ev_wgt);}
  else{m_h2d.erase(m_h2d.find(hname));  cout<<"Error: TH2D "<<hname<<" not found to fill"<<endl;}
  return;
}


int EventRecoAnalysis::InitPartonHistograms(){
  return 0;
}

int EventRecoAnalysis::InitJetHistograms(){
  return 0;
}

int EventRecoAnalysis::InitCutFlowHistograms(){

  BookTH1D("el_event_cutflow_hist", "", 1, 0, 11);
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "Truth");
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "N_{el} > 1");
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "N_{mu} > 1");
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "N_{el} > 1 after OVR");
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "N_{mu} > 1 after OVR");
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "N_{el} = 1");
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "N_{mu} = 1");
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "N_{el} = 1 and N_{mu} = 0");
  m_h1d["el_event_cutflow_hist"]->GetXaxis()->SetBinLabel(10, "N_{el} = 0 and N_{mu} = 1");

  BookTH1D("mu_event_cutflow_hist", "", 1, 0, 11);
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "Truth");
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "N_{el} > 1");
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "N_{mu} > 1");
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "N_{el} > 1 after OVR");
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "N_{mu} > 1 after OVR");
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "N_{el} = 1");
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "N_{mu} = 1");
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "N_{el} = 1 and N_{mu} = 0");
  m_h1d["mu_event_cutflow_hist"]->GetXaxis()->SetBinLabel(10, "N_{el} = 0 and N_{mu} = 1");

  BookTH1D("tau_event_cutflow_hist", "", 1, 0, 11);
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "Truth");
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "N_{el} > 1");
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "N_{mu} > 1");
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "N_{el} > 1 after OVR");
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "N_{mu} > 1 after OVR");
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "N_{el} = 1");
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "N_{mu} = 1");
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "N_{el} = 1 and N_{mu} = 0");
  m_h1d["tau_event_cutflow_hist"]->GetXaxis()->SetBinLabel(10, "N_{el} = 0 and N_{mu} = 1");


  BookTH1D("el_lepsel_cutflow_hist", "", 1, 0, 10);
  m_h1d["el_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["el_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "E_{T} > 25 GeV");
  m_h1d["el_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "|eta| < 2.47");
  m_h1d["el_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "el_author");
  m_h1d["el_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "tightPP");
  m_h1d["el_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "NOT 1.37<|eta|<1.52");
  m_h1d["el_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "|z_{0}| < 2 mm");
  m_h1d["el_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "deadOTX");
  m_h1d["el_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "MiniISO");


  BookTH1D("mu_lepsel_cutflow_hist", "", 1, 0, 12);
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "p_{T} > 25 GeV");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "|eta|<2.5");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "mu_author");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "tight");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "|z_{0}| < 2 mm");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "Pix Hits > 0");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "SCT Hits > 4");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "Holes < = 3");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(10, "TRTHits");
  m_h1d["mu_lepsel_cutflow_hist"]->GetXaxis()->SetBinLabel(11, "MiniISO");



  BookTH1D("truthmatch_ELE_lepsel_ELE_cutflow_hist", "", 1, 0, 10);
  m_h1d["truthmatch_ELE_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["truthmatch_ELE_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "E_{T} > 25 GeV");
  m_h1d["truthmatch_ELE_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "|eta| < 2.47");
  m_h1d["truthmatch_ELE_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "el_author");
  m_h1d["truthmatch_ELE_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "tightPP");
  m_h1d["truthmatch_ELE_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "NOT 1.37<|eta|<1.52");
  m_h1d["truthmatch_ELE_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "|z_{0}| < 2 mm");
  m_h1d["truthmatch_ELE_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "deadOTX");
  m_h1d["truthmatch_ELE_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "MiniISO");

  BookTH1D("truthmatch_MUON_lepsel_ELE_cutflow_hist", "", 1, 0, 10);
  m_h1d["truthmatch_MUON_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["truthmatch_MUON_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "E_{T} > 25 GeV");
  m_h1d["truthmatch_MUON_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "|eta| < 2.47");
  m_h1d["truthmatch_MUON_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "el_author");
  m_h1d["truthmatch_MUON_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "tightPP");
  m_h1d["truthmatch_MUON_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "NOT 1.37<|eta|<1.52");
  m_h1d["truthmatch_MUON_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "|z_{0}| < 2 mm");
  m_h1d["truthmatch_MUON_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "deadOTX");
  m_h1d["truthmatch_MUON_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "MiniISO");

  BookTH1D("truthmatch_TAU_lepsel_ELE_cutflow_hist", "", 1, 0, 10);
  m_h1d["truthmatch_TAU_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["truthmatch_TAU_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "E_{T} > 25 GeV");
  m_h1d["truthmatch_TAU_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "|eta| < 2.47");
  m_h1d["truthmatch_TAU_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "el_author");
  m_h1d["truthmatch_TAU_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "tightPP");
  m_h1d["truthmatch_TAU_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "NOT 1.37<|eta|<1.52");
  m_h1d["truthmatch_TAU_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "|z_{0}| < 2 mm");
  m_h1d["truthmatch_TAU_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "deadOTX");
  m_h1d["truthmatch_TAU_lepsel_ELE_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "MiniISO");


  BookTH1D("truthmatch_ELE_lepsel_MUON_cutflow_hist", "", 1, 0, 12);
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "p_{T} > 25 GeV");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "|eta|<2.5");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "mu_author");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "tight");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "|z_{0}| < 2 mm");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "Pix Hits > 0");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "SCT Hits > 4");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "Holes < = 3");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(10, "TRTHits");
  m_h1d["truthmatch_ELE_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(11, "MiniISO");

  BookTH1D("truthmatch_MUON_lepsel_MUON_cutflow_hist", "", 1, 0, 12);
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "p_{T} > 25 GeV");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "|eta|<2.5");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "mu_author");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "tight");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "|z_{0}| < 2 mm");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "Pix Hits > 0");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "SCT Hits > 4");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "Holes < = 3");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(10, "TRTHits");
  m_h1d["truthmatch_MUON_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(11, "MiniISO");

  BookTH1D("truthmatch_TAU_lepsel_MUON_cutflow_hist", "", 1, 0, 12);
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(1, "Total");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(2, "p_{T} > 25 GeV");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(3, "|eta|<2.5");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(4, "mu_author");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(5, "tight");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(6, "|z_{0}| < 2 mm");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(7, "Pix Hits > 0");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(8, "SCT Hits > 4");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(9, "Holes < = 3");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(10, "TRTHits");
  m_h1d["truthmatch_TAU_lepsel_MUON_cutflow_hist"]->GetXaxis()->SetBinLabel(11, "MiniISO");

  return 0;
}

int EventRecoAnalysis::FillPartonTLVs(){
  m_tlv_p_leptop_b->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);

  m_tlv_p_top->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_atop->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  
  m_tlv_p_leptop->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_hadtop->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  
  m_tlv_p_higgs->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_ttbar->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);

  //
  m_tlv_p_top_hard->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_atop_hard->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  
  m_tlv_p_leptop_hard->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_hadtop_hard->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  
  m_tlv_p_higgs_hard->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_ttbar_hard->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
 
  //000000000000000000000000000000000000000000
  m_tlv_p_leptop_b->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_leptop_W->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_leptop_lep->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_leptop_nu->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  
  m_tlv_p_hadtop_b->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_hadtop_W->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_hadtop_q1->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_hadtop_q2->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  
  m_tlv_p_higgs_b1->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);
  m_tlv_p_higgs_b2->SetPtEtaPhiM(-0.001,-10.,-1.,-1.);

  
  if(m_ip_top_hard>=0)
    m_tlv_p_top_hard->SetPtEtaPhiM(mc_pt->at(m_ip_top_hard)/GeV, mc_eta->at(m_ip_top_hard), mc_phi->at(m_ip_top_hard), mc_m->at(m_ip_top_hard)/GeV);
  if(m_ip_atop_hard>=0)
    m_tlv_p_atop_hard->SetPtEtaPhiM(mc_pt->at(m_ip_atop_hard)/GeV, mc_eta->at(m_ip_atop_hard), mc_phi->at(m_ip_atop_hard), mc_m->at(m_ip_atop_hard)/GeV);
  if(m_ip_leptop_hard>=0)
    m_tlv_p_leptop_hard->SetPtEtaPhiM(mc_pt->at(m_ip_leptop_hard)/GeV, mc_eta->at(m_ip_leptop_hard), mc_phi->at(m_ip_leptop_hard), mc_m->at(m_ip_leptop_hard)/GeV);
  if(m_ip_hadtop_hard>=0)
    m_tlv_p_hadtop_hard->SetPtEtaPhiM(mc_pt->at(m_ip_hadtop_hard)/GeV, mc_eta->at(m_ip_hadtop_hard), mc_phi->at(m_ip_hadtop_hard), mc_m->at(m_ip_hadtop_hard)/GeV);
  if(m_ip_higgs_hard>=0)
    m_tlv_p_higgs_hard->SetPtEtaPhiM(mc_pt->at(m_ip_higgs_hard)/GeV, mc_eta->at(m_ip_higgs_hard), mc_phi->at(m_ip_higgs_hard), mc_m->at(m_ip_higgs_hard)/GeV);

  *m_tlv_p_ttbar_hard = *m_tlv_p_leptop_hard + *m_tlv_p_hadtop_hard;


  //
  if(m_ip_top>=0)
    m_tlv_p_top->SetPtEtaPhiM(mc_pt->at(m_ip_top)/GeV, mc_eta->at(m_ip_top), mc_phi->at(m_ip_top), mc_m->at(m_ip_top)/GeV);
  if(m_ip_atop>=0)
    m_tlv_p_atop->SetPtEtaPhiM(mc_pt->at(m_ip_atop)/GeV, mc_eta->at(m_ip_atop), mc_phi->at(m_ip_atop), mc_m->at(m_ip_atop)/GeV);
  if(m_ip_leptop>=0)
    m_tlv_p_leptop->SetPtEtaPhiM(mc_pt->at(m_ip_leptop)/GeV, mc_eta->at(m_ip_leptop), mc_phi->at(m_ip_leptop), mc_m->at(m_ip_leptop)/GeV);
  if(m_ip_hadtop>=0)
    m_tlv_p_hadtop->SetPtEtaPhiM(mc_pt->at(m_ip_hadtop)/GeV, mc_eta->at(m_ip_hadtop), mc_phi->at(m_ip_hadtop), mc_m->at(m_ip_hadtop)/GeV);
  if(m_ip_higgs>=0)
    m_tlv_p_higgs->SetPtEtaPhiM(mc_pt->at(m_ip_higgs)/GeV, mc_eta->at(m_ip_higgs), mc_phi->at(m_ip_higgs), mc_m->at(m_ip_higgs)/GeV);

  *m_tlv_p_ttbar = *m_tlv_p_leptop + *m_tlv_p_hadtop;


  if(m_ip_leptop_b>=0)
    m_tlv_p_leptop_b->SetPtEtaPhiM(mc_pt->at(m_ip_leptop_b)/GeV, mc_eta->at(m_ip_leptop_b), mc_phi->at(m_ip_leptop_b), mc_m->at(m_ip_leptop_b)/GeV);
  if(m_ip_leptop_W>=0)
    m_tlv_p_leptop_W->SetPtEtaPhiM(mc_pt->at(m_ip_leptop_W)/GeV, mc_eta->at(m_ip_leptop_W), mc_phi->at(m_ip_leptop_W), mc_m->at(m_ip_leptop_W)/GeV);
  if(m_ip_leptop_lep>=0)
    m_tlv_p_leptop_lep->SetPtEtaPhiM(mc_pt->at(m_ip_leptop_lep)/GeV, mc_eta->at(m_ip_leptop_lep), mc_phi->at(m_ip_leptop_lep), mc_m->at(m_ip_leptop_lep)/GeV);
  if(m_ip_leptop_nu>=0)
    m_tlv_p_leptop_nu->SetPtEtaPhiM(mc_pt->at(m_ip_leptop_nu)/GeV, mc_eta->at(m_ip_leptop_nu), mc_phi->at(m_ip_leptop_nu), mc_m->at(m_ip_leptop_nu)/GeV);

  if(m_ip_hadtop_b>=0)
    m_tlv_p_hadtop_b->SetPtEtaPhiM(mc_pt->at(m_ip_hadtop_b)/GeV, mc_eta->at(m_ip_hadtop_b), mc_phi->at(m_ip_hadtop_b), mc_m->at(m_ip_hadtop_b)/GeV);
  if(m_ip_hadtop_W>=0)
    m_tlv_p_hadtop_W->SetPtEtaPhiM(mc_pt->at(m_ip_hadtop_W)/GeV, mc_eta->at(m_ip_hadtop_W), mc_phi->at(m_ip_hadtop_W), mc_m->at(m_ip_hadtop_W)/GeV);
  if(m_ip_hadtop_q1>=0)
    m_tlv_p_hadtop_q1->SetPtEtaPhiM(mc_pt->at(m_ip_hadtop_q1)/GeV, mc_eta->at(m_ip_hadtop_q1), mc_phi->at(m_ip_hadtop_q1), mc_m->at(m_ip_hadtop_q1)/GeV);
  if(m_ip_hadtop_q2>=0)
    m_tlv_p_hadtop_q2->SetPtEtaPhiM(mc_pt->at(m_ip_hadtop_q2)/GeV, mc_eta->at(m_ip_hadtop_q2), mc_phi->at(m_ip_hadtop_q2), mc_m->at(m_ip_hadtop_q2)/GeV);

  if(m_ip_higgs_b1>=0)
    m_tlv_p_higgs_b1->SetPtEtaPhiM(mc_pt->at(m_ip_higgs_b1)/GeV, mc_eta->at(m_ip_higgs_b1), mc_phi->at(m_ip_higgs_b1), mc_m->at(m_ip_higgs_b1)/GeV);
  if(m_ip_higgs_b2>=0)
    m_tlv_p_higgs_b2->SetPtEtaPhiM(mc_pt->at(m_ip_higgs_b2)/GeV, mc_eta->at(m_ip_higgs_b2), mc_phi->at(m_ip_higgs_b2), mc_m->at(m_ip_higgs_b2)/GeV);
  
  if(m_isSignal==1)
    *m_tlv_p_higgs = *m_tlv_p_higgs_b1 + *m_tlv_p_higgs_b2;

  return 0;
}


int EventRecoAnalysis::ClearEventTLVs(){
  delete m_tlv_p_top_hard;
  delete m_tlv_p_atop_hard;
  
  delete m_tlv_p_leptop_hard;
  delete m_tlv_p_hadtop_hard;
  
  delete m_tlv_p_higgs_hard;
  delete m_tlv_p_ttbar_hard;
  //
  delete m_tlv_p_top;
  delete m_tlv_p_atop;
  
  delete m_tlv_p_leptop;
  delete m_tlv_p_hadtop;
  
  delete m_tlv_p_higgs;
  delete m_tlv_p_ttbar;

  delete m_tlv_p_leptop_b;
  delete m_tlv_p_leptop_W;
  delete m_tlv_p_leptop_lep;
  delete m_tlv_p_leptop_nu;

  delete m_tlv_p_hadtop_b;
  delete m_tlv_p_hadtop_W;
  delete m_tlv_p_hadtop_q1;
  delete m_tlv_p_hadtop_q2;

  delete m_tlv_p_higgs_b1;
  delete m_tlv_p_higgs_b2;
  
  //0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  
  return 0;
}

int EventRecoAnalysis::FillLeptonOutputBranches(){
  return 0;
}

int EventRecoAnalysis::FillPartonOutputBranches(){
  t_p_leptop_hard_pt = m_tlv_p_leptop_hard->Pt(); 
  t_p_leptop_hard_eta = m_tlv_p_leptop_hard->Eta();
  t_p_leptop_hard_phi = m_tlv_p_leptop_hard->Phi();
  t_p_leptop_hard_m = m_tlv_p_leptop_hard->M(); 

  t_p_hadtop_hard_pt = m_tlv_p_hadtop_hard->Pt(); 
  t_p_hadtop_hard_eta = m_tlv_p_hadtop_hard->Eta();
  t_p_hadtop_hard_phi = m_tlv_p_hadtop_hard->Phi();
  t_p_hadtop_hard_m = m_tlv_p_hadtop_hard->M(); 

  t_p_higgs_hard_pt = m_tlv_p_higgs_hard->Pt(); 
  t_p_higgs_hard_eta = m_tlv_p_higgs_hard->Eta();
  t_p_higgs_hard_phi = m_tlv_p_higgs_hard->Phi();
  t_p_higgs_hard_m = m_tlv_p_higgs_hard->M(); 

  //
  t_p_leptop_pt = m_tlv_p_leptop->Pt(); 
  t_p_leptop_eta = m_tlv_p_leptop->Eta();
  t_p_leptop_phi = m_tlv_p_leptop->Phi();
  t_p_leptop_m = m_tlv_p_leptop->M(); 

  t_p_hadtop_pt = m_tlv_p_hadtop->Pt(); 
  t_p_hadtop_eta = m_tlv_p_hadtop->Eta();
  t_p_hadtop_phi = m_tlv_p_hadtop->Phi();
  t_p_hadtop_m = m_tlv_p_hadtop->M(); 

  t_p_higgs_pt = m_tlv_p_higgs->Pt(); 
  t_p_higgs_eta = m_tlv_p_higgs->Eta();
  t_p_higgs_phi = m_tlv_p_higgs->Phi();
  t_p_higgs_m = m_tlv_p_higgs->M(); 

  
  t_p_leptop_b_pt = m_tlv_p_leptop_b->Pt(); 
  t_p_leptop_b_eta = m_tlv_p_leptop_b->Eta();
  t_p_leptop_b_phi = m_tlv_p_leptop_b->Phi();
  t_p_leptop_b_m = m_tlv_p_leptop_b->M(); 

  t_p_leptop_W_pt = m_tlv_p_leptop_W->Pt(); 
  t_p_leptop_W_eta = m_tlv_p_leptop_W->Eta();
  t_p_leptop_W_phi = m_tlv_p_leptop_W->Phi();
  t_p_leptop_W_m = m_tlv_p_leptop_W->M(); 

  t_p_leptop_lep_pt = m_tlv_p_leptop_lep->Pt(); 
  t_p_leptop_lep_eta = m_tlv_p_leptop_lep->Eta();
  t_p_leptop_lep_phi = m_tlv_p_leptop_lep->Phi();
  t_p_leptop_lep_m = m_tlv_p_leptop_lep->M(); 
  t_p_leptop_lep_flav = m_p_leptop_lep_flav;
  /*
  if(t_p_leptop_lep_flav == 0 && t_p_leptop_lep_m>1.){
     cout<<"m_ip_leptop_lep = "<<m_ip_leptop_lep<<" m_p_leptop_lep_flav = "<<m_p_leptop_lep_flav
	<<" t_p_leptop_lep_flav = "<<t_p_leptop_lep_flav<<" t_p_leptop_lep_m = "<<t_p_leptop_lep_m<<endl;
    if(m_ip_leptop_lep>=0){
       cout<<"mc_pdgId->at(m_ip_leptop_lep) = "<<mc_pdgId->at(m_ip_leptop_lep)<<"mc_m->at(m_ip_leptop_lep) = "<<mc_m->at(m_ip_leptop_lep)<<endl;
    }

  }
  */
  t_p_leptop_nu_pt = m_tlv_p_leptop_nu->Pt(); 
  t_p_leptop_nu_eta = m_tlv_p_leptop_nu->Eta();
  t_p_leptop_nu_phi = m_tlv_p_leptop_nu->Phi();
  t_p_leptop_nu_m = m_tlv_p_leptop_nu->M(); 


  t_p_hadtop_b_pt = m_tlv_p_hadtop_b->Pt(); 
  t_p_hadtop_b_eta = m_tlv_p_hadtop_b->Eta();
  t_p_hadtop_b_phi = m_tlv_p_hadtop_b->Phi();
  t_p_hadtop_b_m = m_tlv_p_hadtop_b->M(); 

  t_p_hadtop_W_pt = m_tlv_p_hadtop_W->Pt(); 
  t_p_hadtop_W_eta = m_tlv_p_hadtop_W->Eta();
  t_p_hadtop_W_phi = m_tlv_p_hadtop_W->Phi();
  t_p_hadtop_W_m = m_tlv_p_hadtop_W->M(); 

  t_p_hadtop_q1_pt = m_tlv_p_hadtop_q1->Pt(); 
  t_p_hadtop_q1_eta = m_tlv_p_hadtop_q1->Eta();
  t_p_hadtop_q1_phi = m_tlv_p_hadtop_q1->Phi();
  t_p_hadtop_q1_m = m_tlv_p_hadtop_q1->M(); 
  if(m_ip_hadtop_q1 >= 0){ t_p_hadtop_q1_flav = mc_pdgId->at(m_ip_hadtop_q1); }

  t_p_hadtop_q2_pt = m_tlv_p_hadtop_q2->Pt(); 
  t_p_hadtop_q2_eta = m_tlv_p_hadtop_q2->Eta();
  t_p_hadtop_q2_phi = m_tlv_p_hadtop_q2->Phi();
  t_p_hadtop_q2_m = m_tlv_p_hadtop_q2->M(); 
  if(m_ip_hadtop_q2 >= 0){ t_p_hadtop_q2_flav = mc_pdgId->at(m_ip_hadtop_q2); }

  t_p_higgs_b1_pt = m_tlv_p_higgs_b1->Pt(); 
  t_p_higgs_b1_eta = m_tlv_p_higgs_b1->Eta();
  t_p_higgs_b1_phi = m_tlv_p_higgs_b1->Phi();
  t_p_higgs_b1_m = m_tlv_p_higgs_b1->M(); 
  
  t_p_higgs_b2_pt = m_tlv_p_higgs_b2->Pt(); 
  t_p_higgs_b2_eta = m_tlv_p_higgs_b2->Eta();
  t_p_higgs_b2_phi = m_tlv_p_higgs_b2->Phi();
  t_p_higgs_b2_m = m_tlv_p_higgs_b2->M(); 

  return 0;
}

int EventRecoAnalysis::FillMatchedJetOutputBranches(){

  t_j_leptop_b_ind = m_ij_ind_leptop_b;
  
  t_j_hadtop_b_ind = m_ij_ind_hadtop_b;
  t_j_hadtop_q1_ind = m_ij_ind_hadtop_q1;
  t_j_hadtop_q2_ind = m_ij_ind_hadtop_q2;

  t_j_higgs_b1_ind = m_ij_ind_higgs_b1;
  t_j_higgs_b2_ind = m_ij_ind_higgs_b2;

  t_j_hadW_cand_ind = m_ij_ind_hadW_cand;
  t_j_hadtop_nolepb_cand_ind = m_ij_ind_hadtop_nolepb_cand;
  t_j_hadtop_truth_cand_ind = m_ij_ind_hadtop_truth_cand;
  t_j_hadtop_Wbtrim_cand_ind = m_ij_ind_hadtop_Wbtrim_cand;

  //Fill the pj_dr branches here.
  if(m_ip_leptop_b>=0 && m_ij_leptop_b>=0){
    TLorentzVector tlv_j_leptop_b;
    tlv_j_leptop_b.SetPtEtaPhiE(jet_pt->at(m_ij_leptop_b)/GeV, jet_eta->at(m_ij_leptop_b), jet_phi->at(m_ij_leptop_b), jet_E->at(m_ij_leptop_b)/GeV);
    t_pj_leptop_b_dr = m_tlv_p_leptop_b->DeltaR(tlv_j_leptop_b);
  }

  TLorentzVector tlv_j_leptop_lep;
  tlv_j_leptop_lep.SetPtEtaPhiE(lep_pt/GeV, lep_eta, lep_phi, lep_E/GeV);

  
  
  t_j_leptop_lep_pt = tlv_j_leptop_lep.Pt(); 
  t_j_leptop_lep_eta = tlv_j_leptop_lep.Eta();
  t_j_leptop_lep_phi = tlv_j_leptop_lep.Phi();
  t_j_leptop_lep_m = tlv_j_leptop_lep.M(); 
  t_j_leptop_lep_flav = m_j_leptop_lep_flav; //t_p_leptop_lep_flav; //m_channel;
  t_j_leptop_lep_charge = lep_charge;

  /*
  if(t_j_leptop_lep_m<0){
    cout<<"lep_e = "<<lep_E<<endl;
    cout<<"lep_M = "<<lep_m<<endl;
    cout<<"stored lep_M = "<<t_j_leptop_lep_m<<endl;
  }

   if(lep_m<0){
    cout<<"2 lep_e = "<<lep_E<<endl;
    cout<<"2 lep_M = "<<lep_m<<endl;
    cout<<"2 stored lep_M = "<<t_j_leptop_lep_m<<endl;
  }
  */

  if(m_ip_leptop_lep >= 0){
    t_pj_leptop_lep_dr = m_tlv_p_leptop_lep->DeltaR(tlv_j_leptop_lep);
  }

  TLorentzVector tlv_j_leptop_nu;
  //tlv_j_leptop_nu.SetPtEtaPhiM(mc_pt->at(m_ip_leptop_nu)/GeV, 0., mc_phi->at(m_ip_leptop_nu), 0.);
  //tlv_j_leptop_nu.SetPtEtaPhiM(MET_et/GeV, 0., MET_phi, 0.);
  float MET_t=((m_reco_met_px*m_reco_met_px)+(m_reco_met_py*m_reco_met_py));
  float tmp_phi_MET=atan2(m_reco_met_py,m_reco_met_px);
  float phi_MET;
  if(tmp_phi_MET>PIGREC){
    phi_MET=tmp_phi_MET-(2*PIGREC);
  }
  else{
    phi_MET=tmp_phi_MET;
  }
  tlv_j_leptop_nu.SetPtEtaPhiM(MET_t, 0., MET_phi, 0.);
  
  
  t_j_leptop_nu_pt = sqrt(MET_t)/GeV;//tlv_j_leptop_nu.Pt(); 
  t_j_leptop_nu_phi = phi_MET;//tlv_j_leptop_nu.Phi();
  
  if(m_ip_leptop_nu>=0){
    t_pj_leptop_nu_dphi = 0.; //m_tlv_p_leptop_nu->DeltaPhi(tlv_j_leptop_nu);
  }
  if(m_ip_hadtop_b>=0 && m_ij_hadtop_b>=0){
    TLorentzVector tlv_j_hadtop_b;
    tlv_j_hadtop_b.SetPtEtaPhiE(jet_pt->at(m_ij_hadtop_b)/GeV, jet_eta->at(m_ij_hadtop_b), jet_phi->at(m_ij_hadtop_b), jet_E->at(m_ij_hadtop_b)/GeV);
    t_pj_hadtop_b_dr = m_tlv_p_hadtop_b->DeltaR(tlv_j_hadtop_b);
  }
  if(m_ip_hadtop_q1>=0 && m_ij_hadtop_q1>=0){
    TLorentzVector tlv_j_hadtop_q1;
    tlv_j_hadtop_q1.SetPtEtaPhiE(jet_pt->at(m_ij_hadtop_q1)/GeV, jet_eta->at(m_ij_hadtop_q1), jet_phi->at(m_ij_hadtop_q1), jet_E->at(m_ij_hadtop_q1)/GeV);
    t_pj_hadtop_q1_dr = m_tlv_p_hadtop_q1->DeltaR(tlv_j_hadtop_q1);
  }
  if(m_ip_hadtop_q2>=0 && m_ij_hadtop_q2>=0){
    TLorentzVector tlv_j_hadtop_q2;
    tlv_j_hadtop_q2.SetPtEtaPhiE(jet_pt->at(m_ij_hadtop_q2)/GeV, jet_eta->at(m_ij_hadtop_q2), jet_phi->at(m_ij_hadtop_q2), jet_E->at(m_ij_hadtop_q2)/GeV);
    t_pj_hadtop_q2_dr = m_tlv_p_hadtop_q2->DeltaR(tlv_j_hadtop_q2);
  }
  if(m_ip_higgs_b1>=0 && m_ij_higgs_b1>=0){
    TLorentzVector tlv_j_higgs_b1;
    tlv_j_higgs_b1.SetPtEtaPhiE(jet_pt->at(m_ij_higgs_b1)/GeV, jet_eta->at(m_ij_higgs_b1), jet_phi->at(m_ij_higgs_b1), jet_E->at(m_ij_higgs_b1)/GeV);
    t_pj_higgs_b1_dr = m_tlv_p_higgs_b1->DeltaR(tlv_j_higgs_b1);
  }
  if(m_ip_higgs_b2>=0 && m_ij_higgs_b2>=0){
    TLorentzVector tlv_j_higgs_b2;
    tlv_j_higgs_b2.SetPtEtaPhiE(jet_pt->at(m_ij_higgs_b2)/GeV, jet_eta->at(m_ij_higgs_b2), jet_phi->at(m_ij_higgs_b2), jet_E->at(m_ij_higgs_b2)/GeV);
    t_pj_higgs_b2_dr = m_tlv_p_higgs_b2->DeltaR(tlv_j_higgs_b2);
  }
  /*
   cout<<"t_pj_leptop_b_dr = "<<t_pj_leptop_b_dr
      <<"t_pj_hadtop_b_dr = "<<t_pj_hadtop_b_dr
      <<"t_pj_hadtop_q1_dr = "<<t_pj_hadtop_q1_dr
      <<"t_pj_hadtop_q2_dr = "<<t_pj_hadtop_q2_dr
      <<"t_pj_higgs_b1_dr = "<<t_pj_higgs_b1_dr
      <<"t_pj_higgs_b2_dr = "<<t_pj_higgs_b2_dr
      <<endl;
  */
  return 0;
}

int EventRecoAnalysis::FillPartonHistograms(){
  return 0;
}

int EventRecoAnalysis::FillJetHistograms(){
  return 0;
}


int EventRecoAnalysis::FillMatchedJetHistograms(){
  return 0;
}


int EventRecoAnalysis::OneLeptonSelection(){

  int nel = m_el_ind.size();
  int nmu = m_mu_ind.size();
  if(nel + nmu != 1){return -1;}
  else if(nel == 1){
    int ind = m_el_ind.at(0);
    m_j_leptop_lep_flav = ELE;
    lep_pt = el_pt->at(ind); 
    lep_eta = el_eta->at(ind);
    lep_phi = el_phi->at(ind);
    lep_E = el_E->at(ind);
    lep_charge = el_charge->at(ind);
    lep_ind = ind;
  }
  else if(nmu == 1){
    int ind = m_mu_ind.at(0);
    m_j_leptop_lep_flav = MUON;
    lep_pt = mu_pt->at(ind);
    lep_eta = mu_eta->at(ind);
    lep_phi = mu_phi->at(ind);
    lep_E = mu_E->at(ind);
    lep_charge = mu_charge->at(ind);
    lep_ind = ind;
  }



  //if(m_j_leptop_lep_flav > 1){ cout<<" m_j_leptop_lep_flav = "<<m_j_leptop_lep_flav<<" nel = "<<nel<<" nmu = "<<nmu<<endl;}
  return 1;  

}
////////////////////////////////////////////

int EventRecoAnalysis::TruthElectronSelection(){
  double _ptmin =  m_lep_ptmin;
  double _etamax = m_lep_etamax;
  
  for(int ii = 0; ii<el_n; ii++){
    if(el_pt->at(ii) >= _ptmin && (fabs(el_eta->at(ii)) < _etamax)){ m_el_ind.push_back(ii); }
  }
  
  int nel = m_el_ind.size();
  return nel;
  
}

int EventRecoAnalysis::TruthMuonSelection(){
  double _ptmin =  m_lep_ptmin;
  double _etamax = m_lep_etamax;
  
  for(int ii = 0; ii<mu_n; ii++){
    if(mu_pt->at(ii) >= _ptmin && (fabs(mu_eta->at(ii)) < _etamax)){ m_mu_ind.push_back(ii); }
  }
  
  int nmu = m_mu_ind.size();
  return nmu;

}




int EventRecoAnalysis::TruthElectronJetOverlapRemoval(int sfj_key,vector<int> &jettoremove){
  //Loop over electrons
  //For each electron, find the jet closest to it in DeltaR. If DeltaR<0.2, remove that jet's index from selection
  jettoremove.clear();

  TLorentzVector tlv_el; TLorentzVector tlv_jet;
  for(vector<int>::iterator el_it = m_el_ind.begin(); el_it != m_el_ind.end(); ++el_it){
    tlv_el.SetPtEtaPhiE(el_pt->at(*el_it), el_eta->at(*el_it), el_phi->at(*el_it), el_E->at(*el_it));
    double drmin = 99.; int iclos = -1;
    for(int jind = 0; jind != (int) fatjet_pt[sfj_key]->size(); ++jind){
      tlv_jet.SetPtEtaPhiM(fatjet_pt[sfj_key]->at(jind)/GeV, fatjet_eta[sfj_key]->at(jind), fatjet_phi[sfj_key]->at(jind), fatjet_m[sfj_key]->at(jind)/GeV);
      double dr_cur = tlv_el.DeltaR(tlv_jet);
      if(dr_cur > m_e_j_or_dr || dr_cur > drmin){continue;}
      drmin = dr_cur; iclos = jind;
    }
    if(drmin < m_e_j_or_dr){ jettoremove.push_back(iclos); }
  }
  return 0;
}




int EventRecoAnalysis::RemoveTruthElectronWithJetOverlap(int sfj_key){
  TLorentzVector tlv_el; TLorentzVector tlv_jet;
  //If an electron is within DeltaR<0.4 of selected jets, remove the electron index from selection
  int i_e = 0;
  for(vector<int>::iterator el_it = m_el_ind.begin(); el_it != m_el_ind.end(); ++el_it){
    tlv_el.SetPtEtaPhiE(el_pt->at(*el_it), el_eta->at(*el_it), el_phi->at(*el_it), el_E->at(*el_it));
    for(vector<int>::iterator j_it = m_fatjet_ind[sfj_key].begin(); j_it != m_fatjet_ind[sfj_key].end(); ++j_it){
      int jind = *j_it;
      tlv_jet.SetPtEtaPhiM(fatjet_pt[sfj_key]->at(jind)/GeV, fatjet_eta[sfj_key]->at(jind), fatjet_phi[sfj_key]->at(jind), fatjet_m[sfj_key]->at(jind)/GeV);
      double dr_cur = tlv_el.DeltaR(tlv_jet);
      if(dr_cur < m_j_e_or_dr){
	m_el_jet_overlap_ind.push_back(i_e);
	break;
      }
    }
    i_e++;
  }
  return 0;
}

int EventRecoAnalysis::RemoveTruthMuonWithJetOverlap(int sfj_key){
  TLorentzVector tlv_mu; TLorentzVector tlv_jet;
  //If a muon is within DeltaR<0.4 of the jet, remove the muon index from selection
  int i_m = 0;
  for(vector<int>::iterator mu_it = m_mu_ind.begin(); mu_it != m_mu_ind.end(); ++mu_it){
    tlv_mu.SetPtEtaPhiE(mu_pt->at(*mu_it), mu_eta->at(*mu_it), mu_phi->at(*mu_it), mu_E->at(*mu_it));
    for(vector<int>::iterator j_it = m_fatjet_ind[sfj_key].begin(); j_it != m_fatjet_ind[sfj_key].end(); ++j_it){
      int jind = *j_it;
      tlv_jet.SetPtEtaPhiM(fatjet_pt[sfj_key]->at(jind)/GeV, fatjet_eta[sfj_key]->at(jind), fatjet_phi[sfj_key]->at(jind), fatjet_m[sfj_key]->at(jind)/GeV);
      double dr_cur = tlv_mu.DeltaR(tlv_jet);
      if(dr_cur < m_j_mu_or_dr){
	m_mu_jet_overlap_ind.push_back(i_m);
	break;
      }
    }
    i_m++;
  }
  return 0;
}






///////////////////////////////////////
int EventRecoAnalysis::LeptonSelection(){
  
  int nel = m_el_ind.size();
  int nmu = m_mu_ind.size();
  if(nel + nmu != 1){return -1;}
  else if(nel == 1){
    int ind = m_el_ind.at(0);
    m_j_leptop_lep_flav = ELE;
    lep_pt = el_cl_E->at(ind) / cosh(el_tracketa->at(ind)); 
    lep_eta = el_cl_eta->at(ind);
    lep_phi = el_phi->at(ind);
    lep_E = el_E->at(ind);
    lep_m = el_m->at(ind);
    lep_charge = el_charge->at(ind);
    lep_ind = ind;
  }
  else if(nmu == 1){
    int ind = m_mu_ind.at(0);
    m_j_leptop_lep_flav = MUON;
    lep_pt = mu_pt->at(ind);
    lep_eta = mu_eta->at(ind);
    lep_phi = mu_phi->at(ind);
    lep_E = mu_E->at(ind);
    lep_m = mu_m->at(ind);
    lep_charge = mu_charge->at(ind);
    lep_ind = ind;
  }
  //if(m_j_leptop_lep_flav > 1){ cout<<" m_j_leptop_lep_flav = "<<m_j_leptop_lep_flav<<" nel = "<<nel<<" nmu = "<<nmu<<endl;}
  return 1;  
}

//truth ele - reco ele
//truth mu - reco ele
//truth mu - reco mu
//truth tau - reco ele
//truth tau - reco mu

int EventRecoAnalysis::MatchRecoElectronToTruth(TLorentzVector tlv_p_lep, double drcut){
  int ind = -1; double drmin = drcut;

  for(int ii = 0; ii < el_n; ii++){
    TLorentzVector tlv_r;
    tlv_r.SetPtEtaPhiM(el_pt->at(ii), el_eta->at(ii), el_phi->at(ii), el_m->at(ii));
    double dr = tlv_r.DeltaR(tlv_p_lep);
    if(dr >= drmin){continue;}
    drmin = dr; ind = ii;
  }
  
  return ind;
}

int EventRecoAnalysis::MatchRecoMuonToTruth(TLorentzVector tlv_p_lep, double drcut){
  int ind = -1; double drmin = drcut;
  
  for(int ii = 0; ii < mu_n; ii++){
    TLorentzVector tlv_r;
    tlv_r.SetPtEtaPhiM(mu_pt->at(ii), mu_eta->at(ii), mu_phi->at(ii), mu_m->at(ii));
    double dr = tlv_r.DeltaR(tlv_p_lep);
    if(dr >= drmin){continue;}
    drmin = dr; ind = ii;
  }
  
  return ind;
}


int EventRecoAnalysis::SteerTruthMatchLep(){

  TLorentzVector tlv_p_lep;
  tlv_p_lep.SetPtEtaPhiM(mc_pt->at(m_ip_leptop_lep), mc_eta->at(m_ip_leptop_lep), mc_phi->at(m_ip_leptop_lep), mc_m->at(m_ip_leptop_lep));
  double drcut = 0.15;
  
  int i_truthmatch_el = MatchRecoElectronToTruth(tlv_p_lep);
  int i_truthmatch_mu = MatchRecoMuonToTruth(tlv_p_lep);
  
  double dr_truthmatch_el = 100.;
  double dr_truthmatch_mu = 100.;
  
  int p_flav = m_p_leptop_lep_flav;
  int r_flav = -1;

  if(i_truthmatch_el < 0 && i_truthmatch_mu < 0){return -1;}

  if(i_truthmatch_el >= 0){
    TLorentzVector tlv_r;
    tlv_r.SetPtEtaPhiM(el_pt->at(i_truthmatch_el), el_eta->at(i_truthmatch_el), el_phi->at(i_truthmatch_el), el_m->at(i_truthmatch_el));
    dr_truthmatch_el = tlv_r.DeltaR(tlv_p_lep);
  }
  
  if(i_truthmatch_mu >= 0){
    TLorentzVector tlv_r;
    tlv_r.SetPtEtaPhiM(mu_pt->at(i_truthmatch_mu), mu_eta->at(i_truthmatch_mu), mu_phi->at(i_truthmatch_mu), mu_m->at(i_truthmatch_mu));
    dr_truthmatch_mu = tlv_r.DeltaR(tlv_p_lep);
  }
  
  if(dr_truthmatch_el < dr_truthmatch_mu && dr_truthmatch_el < drcut){r_flav = ELE;}
  else if(dr_truthmatch_mu < dr_truthmatch_el && dr_truthmatch_mu < drcut){r_flav = MUON;}

  string s_hist = "";
  if(m_IO & WHIST){
    string s_p_flav = "";
    string s_r_flav = "";
    
    if(p_flav == ELE){s_p_flav = "ELE";}
    else if(p_flav == MUON){s_p_flav = "MUON";} 
    else if(p_flav == TAU){s_p_flav = "TAU";}
    
    if(r_flav == MUON){s_r_flav = "MUON";}
    else if(r_flav == ELE){s_r_flav = "ELE";}
    
    
    s_hist = Form("truthmatch_%s_lepsel_%s_cutflow_hist", s_p_flav.c_str(), s_r_flav.c_str());
  }
  if(r_flav == MUON){ MuonCutFlow(i_truthmatch_mu, s_hist); }
  else if(r_flav == ELE){ ElectronCutFlow(i_truthmatch_el, s_hist); }

  return 0;
  
}

//Change electron and muon selection methods so that they can be reused for this

int EventRecoAnalysis::ElectronCutFlow(int ind, string s_hist){
  int i_cut = 0; int i_pass = 0;
  bool b_fill = false;
  if(s_hist != ""){b_fill = true;}

  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  
  double el_ET = el_cl_E->at(ind) / cosh(el_tracketa->at(ind));
  if(el_ET < m_el_ptmin){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  
  if(fabs(el_cl_eta->at(ind)) > m_el_etamax){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  
  if(! ((el_author->at(ind) == 1) || (el_author->at(ind) == 3)) ){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if(el_tightPP->at(ind) <= 0){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  
  if( (fabs(el_cl_eta->at(ind))>1.37) && (fabs(el_cl_eta->at(ind))<1.52) ){return i_pass;} 
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  
  if(el_trackz0pvunbiased->at(ind) > 2.){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if( (el_OQ->at(ind) & 1446) != 0){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if( (el_MI10_max40_ptsum->at(ind) / el_ET) >= 0.05 ){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;

  i_pass ++;
  
  return i_pass;
  
}

int EventRecoAnalysis::MuonCutFlow(int ind, string s_hist){

  int i_cut = 0; int i_pass = 0;
  bool b_fill = false;
  if(s_hist != ""){b_fill = true;}


  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  
  if(mu_pt->at(ind) < m_mu_ptmin){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if(fabs(mu_eta->at(ind)) > m_mu_etamax){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;

  if(mu_author->at(ind) != 12){return i_pass;} //combined MuID
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if(mu_tight->at(ind) <= 0){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if(mu_id_z0_exPV->at(ind) >= 2.){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  
  //Additional HIT requirements
  if( (mu_nPixHits->at(ind) + mu_nPixelDeadSensors->at(ind)) <= 0 ){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if( (mu_nSCTHits->at(ind) + mu_nSCTDeadSensors->at(ind)) <= 4 ){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if( (mu_nPixHoles->at(ind) + mu_nSCTHoles->at(ind)) >= 3 ){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if( (fabs(mu_eta->at(ind)) > 0.1) && (fabs(mu_eta->at(ind)) < 0.9) ){
    int n = mu_nTRTHits->at(ind) + mu_nTRTOutliers->at(ind);
    if( (n <= 5) || (mu_nTRTOutliers->at(ind) >= 0.9*n) ){return i_pass;} 
  } 
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  if( (mu_MI10_max40_ptsum->at(ind) / mu_pt->at(ind)) >= 0.05 ){return i_pass;}
  if(b_fill){m_h1d[s_hist]->Fill(i_cut);} i_cut++;
  
  i_pass++;
  return i_pass;

}

int EventRecoAnalysis::ElectronSelection(){

  string s_hist = "";
  if(m_IO & WHIST){s_hist = "el_lepsel_cutflow_hist";}

  for(int ii = 0; ii<el_n; ii++){
    int i_pass = ElectronCutFlow(ii, s_hist);
    if(i_pass > 0){ m_el_ind.push_back(ii); }
  }

  int nel = m_el_ind.size();
  return nel;

}

int EventRecoAnalysis::MuonSelection(){

  string s_hist = ""; 
  if(m_IO & WHIST){s_hist="mu_lepsel_cutflow_hist";}

  for(int ii = 0; ii<mu_n; ii++){
    int i_pass = MuonCutFlow(ii, s_hist);
    if(i_pass > 0){ m_mu_ind.push_back(ii); }
  }

  int nmu = m_mu_ind.size();
  return nmu;

}





int EventRecoAnalysis::ElectronJetOverlapRemoval(){
  //Loop over electrons
  //For each electron, find the jet closest to it in DeltaR. If DeltaR<0.2, remove that jet's index from selection
  TLorentzVector tlv_el; TLorentzVector tlv_jet;
  for(vector<int>::iterator el_it = m_el_ind.begin(); el_it != m_el_ind.end(); ++el_it){
    tlv_el.SetPtEtaPhiE(el_pt->at(*el_it), el_eta->at(*el_it), el_phi->at(*el_it), el_E->at(*el_it));
    double drmin = 99.; int iclos = -1;
    for(int jind = 0; jind != jet_pt->size(); ++jind){
      tlv_jet.SetPtEtaPhiE(jet_pt->at(jind), jet_eta->at(jind), jet_phi->at(jind), jet_E->at(jind));
      double dr_cur = tlv_el.DeltaR(tlv_jet);
      if(dr_cur > m_e_j_or_dr || dr_cur > drmin){continue;}
      drmin = dr_cur; iclos = jind;
    }
    if(drmin < m_e_j_or_dr){ m_jet_e_overlap_ind.push_back(iclos); }
  }
  return 0;
}

int EventRecoAnalysis::RemoveElectronWithJetOverlap(){
  TLorentzVector tlv_el; TLorentzVector tlv_jet;
  //If an electron is within DeltaR<0.4 of selected jets, remove the electron index from selection
  int i_e = 0;
  for(vector<int>::iterator el_it = m_el_ind.begin(); el_it != m_el_ind.end(); ++el_it){
    tlv_el.SetPtEtaPhiE(el_pt->at(*el_it), el_eta->at(*el_it), el_phi->at(*el_it), el_E->at(*el_it));
    for(vector<int>::iterator j_it = m_jet_ind.begin(); j_it != m_jet_ind.end(); ++j_it){
      int jind = *j_it;
      tlv_jet.SetPtEtaPhiE(jet_pt->at(jind), jet_eta->at(jind), jet_phi->at(jind), jet_E->at(jind));
      double dr_cur = tlv_el.DeltaR(tlv_jet);
      if(dr_cur < m_j_e_or_dr){
	m_el_jet_overlap_ind.push_back(i_e);
	break;
      }
    }
    i_e++;
  }
  return 0;
}

int EventRecoAnalysis::RemoveMuonWithJetOverlap(){
  TLorentzVector tlv_mu; TLorentzVector tlv_jet;
  //If a muon is within DeltaR<0.4 of the jet, remove the muon index from selection
  int i_m = 0;
  for(vector<int>::iterator mu_it = m_mu_ind.begin(); mu_it != m_mu_ind.end(); ++mu_it){
    tlv_mu.SetPtEtaPhiE(mu_pt->at(*mu_it), mu_eta->at(*mu_it), mu_phi->at(*mu_it), mu_E->at(*mu_it));
    for(vector<int>::iterator j_it = m_jet_ind.begin(); j_it != m_jet_ind.end(); ++j_it){
      int jind = *j_it;
      tlv_jet.SetPtEtaPhiE(jet_pt->at(jind), jet_eta->at(jind), jet_phi->at(jind), jet_E->at(jind));
      double dr_cur = tlv_mu.DeltaR(tlv_jet);
      if(dr_cur < m_j_mu_or_dr){
	m_mu_jet_overlap_ind.push_back(i_m);
	break;
      }
    }
    i_m++;
  }
  return 0;
}

/*
vector<PseudoJet> EventRecoAnalysis::inclCsFunctionTA( ClusterSequenceArea *csfoo) {
//vector<PseudoJet> inclCsFunctionTA( ClusterSequenceArea *csfoo) {

  PyObject *pRet;
  PyObject *arglist = Py_BuildValue( "(O)", PyCObject_FromVoidPtr( csfoo, PyDelClusterSequenceTA ));

  pRet = PyObject_CallObject( globalInclCsTA, arglist );
  Py_DECREF( arglist );     

  vector<PseudoJet> out_v; out_v.clear();
  //int size = PyList_Size( pRet );
  Py_ssize_t size = PyList_Size( pRet );

  for (int i=0; i<size; i++){
    void *temp = (PyCObject_AsVoidPtr( PyList_GetItem(pRet, i )));
    PseudoJet *pj = static_cast<PseudoJet*> (temp);
    out_v.push_back( *pj );
  }

  Py_DECREF( pRet);   

  return out_v;
 
}


vector<PseudoJet> EventRecoAnalysis::exclCsFunctionTA( ClusterSequenceArea *csfoo, int njets) {
//vector<PseudoJet> exclCsFunctionTA( ClusterSequenceArea *csfoo, int njets) {
   PyObject *pRet;
   PyObject *arglist = Py_BuildValue( "(Oi)", PyCObject_FromVoidPtr( csfoo, PyDelClusterSequenceTA ), njets);
   pRet = PyObject_CallObject( globalExclCsTA, arglist );
   Py_DECREF( arglist );     

   vector<PseudoJet> out_v; out_v.clear();
   Py_ssize_t size = PyList_Size( pRet );

   for (int i=0; i<size; i++) {
      void *temp = (PyCObject_AsVoidPtr( PyList_GetItem(pRet, i )));
      PseudoJet *pj = static_cast<PseudoJet*> (temp);
      out_v.push_back( *pj );
   }

   Py_DECREF( pRet);   

   return out_v;
}


double EventRecoAnalysis::CalcRWPDFWeight(){
  //w=pdfTool.event_weight(pow(mcevt_pdf_scale->at(0),2),mcevt_pdf_x1->at(0),mcevt_pdf_x2->at(0),mcevt_pdf_id1->at(0),mcevt_pdf_id2->at(0),10042);
  //  double event_weight(double pdf_scale2, double pdf_x1, double pdf_x2, int pdf_id1, int pdf_id2, int init_pdf=-1){//Q^2,x1,x2,f1,f2,init_pdf

  double rw_wgt = 1.;
  if(m_beam_nrg_RW > 0.){
    rw_wgt = 1;//m_pdfTool->event_weight( pow(mcevt_pdf_scale->at(0), 2), mcevt_pdf_x1->at(0), mcevt_pdf_x2->at(0), mcevt_pdf_id1->at(0), mcevt_pdf_id2->at(0) );
  }

  return rw_wgt;

}


double EventRecoAnalysis::EventPileupDensity(double radius){
  double pu_rho = 0.;

  vector<PseudoJet> event_particles; event_particles.clear();
  for(int i = 0; i<cl_n; i++){
    double cc_px=0.; double cc_py=0.; double cc_pz=0.; double cc_E=0.; double cc_pmag=0.; double cc_m=0.;

    cc_px = (cl_pt->at(i))*cos(cl_phi->at(i));
    cc_py = (cl_pt->at(i))*sin(cl_phi->at(i));
    cc_pz = (cl_pt->at(i))*sinh(cl_eta->at(i));
    cc_pmag = sqrt(cc_px*cc_px + cc_py*cc_py + cc_pz*cc_pz);
    cc_E = sqrt(cc_pmag*cc_pmag + cc_m*cc_m);

    PseudoJet cc(cc_px, cc_py, cc_pz, cc_E);
    event_particles.push_back(cc);
  }

  JetDefinition rho_jet_def = JetDefinition(kt_algorithm, radius, Best);
  AreaDefinition rho_area_def = AreaDefinition(VoronoiAreaSpec(0.9));
  Selector rho_selector = SelectorAbsRapMax(2.0);

  JetMedianBackgroundEstimator bge(rho_selector, rho_jet_def, rho_area_def);

  bge.set_particles(event_particles);
  pu_rho = bge.rho();

  event_particles.clear();

  // cout<<" pu_rho = "<<pu_rho<<endl;
  return pu_rho;
}
*/
//t_p_leptop_b
//t_p_hadtop_b
//t_p_hadtop_q1
//t_p_hadtop_q2
//t_p_higgs_b1
//t_p_higgs_b2

int EventRecoAnalysis::ClassifyMCJets(vector<int> part_ind){
  int i_class = -1;

  multiset<int> set_pi; set_pi.clear();
  for(int k = 0; k<part_ind.size(); k++){
    set_pi.insert(part_ind.at(k));
  }

  if(part_ind.size() <= 0){i_class = I_NOP;}
  else if(part_ind.size() == 1){
    if( set_pi.find(I_HADTOP_Q1) != set_pi.end() || set_pi.find(I_HADTOP_Q2) != set_pi.end() ){i_class = I_1P0B;}
    else if( set_pi.find(I_LEPTOP_B) != set_pi.end() ){i_class = I_BL;}
    else if( set_pi.find(I_HADTOP_B) != set_pi.end() || set_pi.find(I_HIGGS_B1) != set_pi.end() || set_pi.find(I_HIGGS_B2) != set_pi.end() ){i_class = I_1P1B;}
  }
  else if(part_ind.size() == 2){
    if(set_pi.find(I_HADTOP_Q1) != set_pi.end() && set_pi.find(I_HADTOP_Q2) != set_pi.end()){i_class = I_Q1Q2;}
    else if(  (set_pi.find(I_LEPTOP_B) != set_pi.end() || set_pi.find(I_HIGGS_B1) != set_pi.end() || set_pi.find(I_HIGGS_B2) != set_pi.end()) 
	 &&  (set_pi.find(I_HADTOP_Q1) != set_pi.end() || set_pi.find(I_HADTOP_Q2) != set_pi.end()) ){i_class = I_2P1BCOMB;}
    else if(  (set_pi.find(I_HADTOP_B) != set_pi.end())
	 &&  (set_pi.find(I_HADTOP_Q1) != set_pi.end() || set_pi.find(I_HADTOP_Q2) != set_pi.end()) ){i_class = I_2P1BPART;}
    else if( ( set_pi.find(I_LEPTOP_B) != set_pi.end() && set_pi.find(I_HADTOP_B) != set_pi.end() ) 
	     || ( set_pi.find(I_LEPTOP_B) != set_pi.end() && (set_pi.find(I_HIGGS_B1) != set_pi.end() || set_pi.find(I_HIGGS_B2) != set_pi.end()) ) 
	     || ( set_pi.find(I_HADTOP_B) != set_pi.end() && (set_pi.find(I_HIGGS_B1) != set_pi.end() || set_pi.find(I_HIGGS_B2) != set_pi.end()) ) ){i_class = I_2P2BCOMB;}
    else if(set_pi.find(I_HIGGS_B1) != set_pi.end() && set_pi.find(I_HIGGS_B2) != set_pi.end()){i_class = I_B1B2;} 
  }
  else if(part_ind.size() == 3){
    if( ( set_pi.find(I_LEPTOP_B) != set_pi.end() && set_pi.find(I_HADTOP_B) != set_pi.end() && (set_pi.find(I_HIGGS_B1) != set_pi.end() || set_pi.find(I_HIGGS_B2) != set_pi.end()) )
	|| ( set_pi.find(I_HIGGS_B1) != set_pi.end() && set_pi.find(I_HIGGS_B2) != set_pi.end() && (set_pi.find(I_LEPTOP_B) != set_pi.end() || set_pi.find(I_HADTOP_B) != set_pi.end()) ) ){i_class = I_3P3BCOMB;}

    else if( (set_pi.find(I_HADTOP_Q1) != set_pi.end() || set_pi.find(I_HADTOP_Q2) != set_pi.end())
	     && (  ( (set_pi.find(I_LEPTOP_B) != set_pi.end() || set_pi.find(I_HADTOP_B) != set_pi.end())
		     && (set_pi.find(I_HIGGS_B1) != set_pi.end() || set_pi.find(I_HIGGS_B2) != set_pi.end()) )
		   || ( (set_pi.find(I_LEPTOP_B) != set_pi.end() && set_pi.find(I_HADTOP_B) != set_pi.end())
			|| (set_pi.find(I_HIGGS_B1) != set_pi.end() && set_pi.find(I_HIGGS_B2) != set_pi.end()) ) ) ){i_class = I_3P2BCOMB;}

    else if(set_pi.find(I_HADTOP_B) != set_pi.end() && set_pi.find(I_HADTOP_Q1) != set_pi.end() && set_pi.find(I_HADTOP_Q2) != set_pi.end() ){i_class = I_BHQ1Q2;}
    else if( (set_pi.find(I_LEPTOP_B) != set_pi.end() || set_pi.find(I_HIGGS_B1) != set_pi.end() || set_pi.find(I_HIGGS_B2) != set_pi.end()) 
       && (set_pi.find(I_HADTOP_Q1) != set_pi.end() && set_pi.find(I_HADTOP_Q2) != set_pi.end()) ){i_class = I_3P1BCOMB;}
  }
  else if(part_ind.size() > 3){i_class = I_GT3P;}


  return i_class;
}

/*
  pregrooming : only take clusters belonging to ak4 jets with pT > 15 GeV
  addnl options : jets with JVF > 50%
  remove clusters associated to lepton (for now, the jet closest to the lepton within deltaR < 0.2)

  0x1 << 0 : remove clusters belonging to AK4 jets with pT > 15 GeV || |eta|>4.
  0x1 << 1 : remove clusters belonging to AK4 jets with JVF < 0.5
  0x1 << 2 : remove clusters belonging to closest AK4 jet to lepton if pTjet > 25 GeV and deltaR(jet, lepton)<0.2    

  0: no pregrooming
  1: only take clusters belonging to ak4 jets with pT > 15 GeV
  2: only take clusters belonging to ak4 jets with pT > 15 GeV and JVF > 0.5
  3: remove clusters 
 */


int EventRecoAnalysis::MakePregroomingInputCollection(string s_pgcol){
  int sj_key = I_PREGROOM_INPUT;

  vector<int> vint; vint.clear(); vint.push_back(m_pregroom_jetID);
  double ptmin = 15.*GeV; double etamax = 4.;
  
  MakeJetCollection(m_pregroom_jetcol, vint, ptmin, etamax, false);
  
  /*  
  //EDIT
  fatjet_n[sj_key] = thejetcol.size();
  for(vector<PseudoJet>::iterator pjit=thejetcol.begin(); pjit!=thejetcol.end(); ++pjit){
    fatjet_pt[sj_key]->push_back(-1.);
    fatjet_m[sj_key]->push_back(-1.);
    fatjet_eta[sj_key]->push_back(-1.);
    fatjet_phi[sj_key]->push_back(-1.);
    
    fatjet_constscale_pt[sj_key]->push_back(pjit->perp());
    fatjet_constscale_m[sj_key]->push_back(pjit->m());
    fatjet_constscale_eta[sj_key]->push_back(pjit->eta());
    fatjet_constscale_phi[sj_key]->push_back(pjit->phi_std());
    
    vector<int> v_co_ind; v_co_ind.clear();
    vector<PseudoJet> const_out=pjit->constituents();
    for(vector<PseudoJet>::iterator cjit=const_out.begin(); cjit!=const_out.end(); ++cjit){
      int co_ind = cjit->user_index();
      v_co_ind.push_back(co_ind);
    }
    const_out.clear();
    fatjet_constit_n[sj_key]->push_back(v_co_ind.size());
    fatjet_constit_index[sj_key]->push_back(v_co_ind);
  }

  // cout<<"Pregroomed jets = "<<fatjet_n[sj_key]<<endl;

  thejetcol.clear();
    
  */
  return 0;
}

vector<PseudoJet> EventRecoAnalysis::FillJetInputCollection(int b_pregroom, int input_type, int signal_type, vector<int>* vp_veto){
  /*
   cout<<"FILLJETINPUTCOLLECTION"<<endl;
   cout<<"0 b_pregroom = "<<b_pregroom<<endl;
   cout<<"0 b_pregroom & rc_el_cl = "<<(b_pregroom & rc_el_cl)<<endl;
   cout<<"0 b_pregroom & rc_ak4_elj = "<<(b_pregroom & rc_ak4_elj)<<endl;
   cout<<"0 b_pregroom & rc_ak4_nolepb = "<<(b_pregroom & rc_ak4_nolepb)<<endl;
   cout<<"0 b_pregroom & rc_ak4_15 = "<<(b_pregroom & rc_ak4_15)<<endl;
   cout<<"0 b_pregroom & rc_ak4_nonb = "<<(b_pregroom & rc_ak4_nonb)<<endl;
   cout<<"0 b_pregroom & rc_ak4_b = "<<(b_pregroom & rc_ak4_b)<<endl;
  */

  vector<int> v_veto; v_veto.clear();
  if(vp_veto != NULL){v_veto = *vp_veto;}
  if(b_pregroom & rc_ak4_nolepb){
    //cout<<"FILLJETINPUTCOLLECTION NOLEPB m_ij_leptop_b ="<<m_ij_leptop_b<<endl;
    if(m_ij_leptop_b >= 0){
      for(int k = 0; k < jet_constit_n->at( m_ij_leptop_b); k++){
	int i = jet_constit_index->at( m_ij_leptop_b).at(k);
	v_veto.push_back(i);
      }
    }
  }

  int const_in_n = 0;

  vector<PseudoJet> myconsts_in;
  myconsts_in.clear();
  int incol_n = 0;

  int iclos_cl_lep = -1;
  if( (m_j_leptop_lep_flav == ELE) && (b_pregroom & rc_el_cl) ){
    //cout<<"FILLJETINPUTCOLLECTION NOELCL"<<endl;
    //Loop over all clusters and find the one closest to the electron
    double drmin = 0.2;
    TLorentzVector tlv_el;
    tlv_el.SetPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_E);
    for(int i = 0; i < cl_n; i++){
      TLorentzVector tlv_cl;
      tlv_cl.SetPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_E);
      double drcur = tlv_cl.DeltaR(tlv_el);
      if(drcur > drmin){continue;}
      drmin = drcur; iclos_cl_lep = i;
    }
    
  }
  
  //Fill input cluster sequence
  if( (b_pregroom & rc_ak4_b) || (b_pregroom & rc_ak4_nonb) ){
    //cout<<"FILLJETINPUTCOLLECTION BJETS/NOBJETS"<<endl;
    
    //for(int jj = 0; jj < m_jet_ind.size(); jj++){
    //int j = m_jet_ind.at(jj);
    for(int j = 0; j < jet_n; j++){
      if( (jet_pt->at(j) < 20.*GeV) || (fabs(jet_eta->at(j)) > 4.0) ){continue;}
      if(b_pregroom & rc_ak4_b){
	if( jet_MV1->at(j) <= m_jet_mv1cut ){continue;}
      }
      else if(b_pregroom & rc_ak4_nonb){
	if( jet_MV1->at(j) > m_jet_mv1cut ){continue;}
      }
      
      /*
      if(b_pregroom & rc_ak4_nolepb){
	if(j == m_ij_leptop_b){ cout<<"VETO LEPB j = "<<j<<endl; continue;}
      }
       cout<<"Include jet j ="<<j<<endl;      
      */
      for(int k = 0; k < jet_constit_n->at(j); k++){
	int i = jet_constit_index->at(j).at(k);
	double cc_m=0.; double cc_E=0.; double cc_pmag=0.;
	double cc_px=0.; double cc_py=0.; double cc_pz=0.;
	
	if(cl_pt->at(i)<0 || cl_pt->at(i)!=cl_pt->at(i)){continue;}
	cc_pmag = (cl_pt->at(i))*cosh(cl_eta->at(i));
	cc_m = 0.;
	cc_px = (cl_pt->at(i))*cos(cl_phi->at(i));
	cc_py = (cl_pt->at(i))*sin(cl_phi->at(i));
	cc_pz = (cl_pt->at(i))*sinh(cl_eta->at(i));
	cc_E = sqrt(cc_pmag*cc_pmag + cc_m*cc_m);

	PseudoJet cc(cc_px, cc_py, cc_pz, cc_E);
	cc.set_user_index(i);
	myconsts_in.push_back(cc);
	const_in_n++;

      }
      
    }
  }
  
  else if(b_pregroom & rc_ak4_15){
    //cout<<"FILLJETINPUTCOLLECTION 15GEV"<<endl;
    int sj_key = I_PREGROOM_INPUT;

    //Loop over the s_pregroom_jetcol collection and add its constituents to myconsts_in
    //pT, eta selection and electron overlap removal should be done already in making the pregrooming input collection
    
    for(int j = 0; j < fatjet_n[sj_key]; j++){
      if( (fatjet_pt[sj_key]->at(j) < 15.*GeV) || (fabs(fatjet_eta[sj_key]->at(j)) > 4.0) ){continue;}
      for(int k = 0; k < fatjet_constit_n[sj_key]->at(j); k++){
	int i = fatjet_constit_index[sj_key]->at(j).at(k);
	if(i == iclos_cl_lep){continue;}
	double cc_m=0.; double cc_E=0.; double cc_pmag=0.;
	double cc_px=0.; double cc_py=0.; double cc_pz=0.;

	if(input_type == inTOPO){
	  if(cl_pt->at(i)<0 || cl_pt->at(i)!=cl_pt->at(i)){continue;}
	  cc_pmag = (cl_pt->at(i))*cosh(cl_eta->at(i));
	  cc_m = 0.;
	  cc_px = (cl_pt->at(i))*cos(cl_phi->at(i));
	  cc_py = (cl_pt->at(i))*sin(cl_phi->at(i));
	  cc_pz = (cl_pt->at(i))*sinh(cl_eta->at(i));
	  cc_E = sqrt(cc_pmag*cc_pmag + cc_m*cc_m);
	}
	else if(input_type == inTRK){;}
	else if(input_type == inTRUTH){
	  cc_pmag = (mc_pt->at(i))*cosh(mc_eta->at(i));
	  cc_m = mc_m->at(i);
	  cc_px = (mc_pt->at(i))*cos(mc_phi->at(i));
	  cc_py = (mc_pt->at(i))*sin(mc_phi->at(i));
	  cc_pz = (mc_pt->at(i))*sinh(mc_eta->at(i));
	  cc_E = sqrt(cc_pmag*cc_pmag + cc_m*cc_m);
	}
	PseudoJet cc(cc_px, cc_py, cc_pz, cc_E);
	cc.set_user_index(i);
	myconsts_in.push_back(cc);
	const_in_n++;
	
      }
    }//pregroom jetcol loop
  }//if pregrooming
  
  
  else if(input_type==inTOPO){
    //cout<<"FILLJETINPUTCOLLECTION NOPREGROOM"<<endl;

    incol_n = cl_n;
    for(int i=0; i<incol_n; i++){
      double cc_m=0.; double cc_E=0.; double cc_pmag=0.;
      double cc_px=0.; double cc_py=0.; double cc_pz=0.;
      if(cl_pt->at(i)<0 || cl_pt->at(i)!=cl_pt->at(i)){continue;}

      bool b_iveto = false;
      vector<int>::iterator vit = v_veto.begin();
      while(vit != v_veto.end()){
	if(i == *vit){ 
	  b_iveto = true; 
	  vit = v_veto.erase(vit);
	  break;
	}
	else{ ++vit;}
      }
      if(b_iveto){continue;}


      cc_pmag = (cl_pt->at(i))*cosh(cl_eta->at(i));
      cc_m = 0.;
      cc_px = (cl_pt->at(i))*cos(cl_phi->at(i));
      cc_py = (cl_pt->at(i))*sin(cl_phi->at(i));
      cc_pz = (cl_pt->at(i))*sinh(cl_eta->at(i));
      cc_E = sqrt(cc_pmag*cc_pmag + cc_m*cc_m);
      PseudoJet cc(cc_px, cc_py, cc_pz, cc_E);
      cc.set_user_index(i);
      myconsts_in.push_back(cc);
      const_in_n++;
    }
  }
  else if(input_type==inTRK){
    ;
    //incol_n = trk_n;
  }
  else if(input_type==inTRUTH){
    bool exclude_neutrinos = true;//true;
    bool exclude_muons = true;//true;
    incol_n = mc_n;

    for(int i=0; i<incol_n; i++){
      //determine whether particle has a GEANT vertex
      bool g4vertex = false;
      int nci = (mc_child_index->at(i)).size();
      for (int j=0; j != nci; ++j){
	if ( (mc_child_index->at(i))[j] < 0 || (mc_child_index->at(i))[j] == incol_n || (mc_child_index->at(i))[j] > incol_n ) continue;
	int p =(mc_child_index->at(i))[j];
	if ( mc_barcode->at(p) > 100000 ) g4vertex = true;
      }
	
      //Check decay status of particle
      int status = mc_status->at(i);
      bool is_undecayed = false, is_decayed_pythia = false, is_decayed_geant = false; 
      if(status%1000 == 1) is_undecayed=true; 
      if(status%1000 == 2 && status > 1000) is_decayed_pythia=true;
      if(status==2 && g4vertex) is_decayed_geant = true;
	
      if( !is_undecayed && !is_decayed_pythia && !is_decayed_geant ) continue;
      //determine whether the particle is GEANT   
      int barcode=mc_barcode->at(i);
      bool is_geant_particle = false; 
      if(barcode<200000)is_geant_particle = true; 
	
      if( !is_geant_particle ) continue;
	
      int pdg_id=mc_pdgId->at(i);

      //muons and neutrinos
      bool is_nu = false, is_muon = false;                                                                
      if( abs(pdg_id)==12 || abs(pdg_id)==14 || abs(pdg_id)==16 ) is_nu = true;
      if( is_nu && exclude_neutrinos ) continue;
	
      if( abs(pdg_id)==13 ) is_muon = true;
      if( is_muon && exclude_muons ) continue;
	
      //determine whether the particle is other non-interacting (is_susy covers all sorts of hypothetical particles such as Gravitons, KK excitations)
      bool is_susy = false, is_funny_gluon = false;
      if( (pdg_id==1000022 && status%1000==1 ) ||
	  (pdg_id==5100022 && status%1000==1 ) ||
	  (pdg_id==1000024 && status%1000==1 ) ||
	  (pdg_id==39      && status%1000==1 ) ||
	  (pdg_id==1000039 && status%1000==1 ) ||
	  (pdg_id==5000039 && status%1000==1 ) ) is_susy = true;
      if( is_susy ) continue;
      double mc_pmag = mc_pt->at(i)*cosh(mc_eta->at(i));
      double mc_mass = mc_m->at(i);
      double mc_E = sqrt(mc_pmag*mc_pmag + mc_mass*mc_mass);

      double pe = mc_E;//mc_E->at(i);    
      if( abs(pdg_id)==21 && pe==0 ) is_funny_gluon = true;
      if( is_funny_gluon ) continue;

      bool b_iveto = false;
      vector<int>::iterator vit = v_veto.begin();
      //cout<<"FILLJETINPUTCOLLECTION VETO size = "<<v_veto.size()<<endl;

      while(vit != v_veto.end()){
	if(i == *vit){ 
	  b_iveto = true; 
	  vit = v_veto.erase(vit);
	  break;
	}
	else{ ++vit;}
      }
      if(b_iveto){continue;}


      double  peta = mc_eta->at(i), pphi = mc_phi->at(i), ppt = mc_pt->at(i);
	
      double cc_px = ppt*cos(pphi), cc_py = ppt*sin(pphi), cc_pz = ppt*sinh(peta); 

      PseudoJet cc(cc_px, cc_py, cc_pz, mc_E);
      
      cc.set_user_index(i);
      myconsts_in.push_back(cc);
      const_in_n++;

    }//mc_n loop

  }//if truth
  else if(input_type==inMITRU){
    
    incol_n = cell_n;
    for(int i=0; i<incol_n; i++){
      
      bool b_iveto = false;
      vector<int>::iterator vit = v_veto.begin();
      //cout<<"FILLJETINPUTCOLLECTION VETO size = "<<v_veto.size()<<endl;
      
      while(vit != v_veto.end()){
	if(i == *vit){ 
	  b_iveto = true; 
	  vit = v_veto.erase(vit);
	  break;
	}
	else{ ++vit;}
      }
      if(b_iveto){continue;}
      

      double px, py, pz;
      px = cell_pt->at(i)*cos(cell_phi->at(i));
      py = cell_pt->at(i)*sin(cell_phi->at(i));
      pz = cell_pt->at(i)*sinh(cell_eta->at(i));
      
      PseudoJet onepseudojet(px, py, pz, cell_E->at(i));
      // Store as input to Fastjet
      
      onepseudojet.set_user_index(i);
      myconsts_in.push_back(onepseudojet);
      const_in_n++;
      
    }
  }//if michael truth
  
  
  
  
  
  if(const_in_n != myconsts_in.size()){ cout<<"CONTAINER SIZE MISMATCH"<<endl;}
  
  return myconsts_in;
  
  
}


int EventRecoAnalysis::MakeJetCollection(string s_rebuild, vector<int> v_sj_key, double fj_ptmin, double fj_etamax, double fj_ptmin_groom, bool doCalib, bool callSubstruct, bool ghost_match_mc, bool ghost_match_b,float JESsyst, vector<PseudoJet>* vp_inputs, int i_excl){

  vector<PseudoJet> v_inputs; v_inputs.clear();
  if(vp_inputs != NULL){v_inputs = *vp_inputs;}

  bool _doCalib = doCalib; 

  JetAlgorithm fj_alg; double fj_rad = 0.; string fj_s_rad = "";

  int signal_type = -1;
  int input_type = -1;

  
  //cout<<"000000000000000000000000000000000000000000000000000000000000"<<endl;
  //cout<<"Now building "<<s_rebuild<<endl;
  //cout<<"000000000000000000000000000000000000000000000000000000000000"<<endl;
  
  if(s_rebuild.find("AntiKt")!=string::npos){fj_alg = antikt_algorithm;}
  else if(s_rebuild.find("CamKt")!=string::npos){fj_alg = cambridge_algorithm;}
  else if(s_rebuild.find("Kt")!=string::npos){fj_alg = kt_algorithm;}
  else{ cout<<"Unknown jet algorithm"<<endl; return -1;}

  if(s_rebuild.find("15")!=string::npos){fj_rad = 1.5; fj_s_rad = "150";}
  else if(s_rebuild.find("12")!=string::npos){fj_rad = 1.2; fj_s_rad = "120";}
  else if(s_rebuild.find("10")!=string::npos){fj_rad = 1.0; fj_s_rad = "100";}
  else if(s_rebuild.find("8")!=string::npos){fj_rad = 0.8; fj_s_rad = "080";}
  else if(s_rebuild.find("6")!=string::npos){fj_rad = 0.6; fj_s_rad = "060";}
  else if(s_rebuild.find("4")!=string::npos){fj_rad = 0.4; fj_s_rad = "040";}
  else if(s_rebuild.find("2")!=string::npos){fj_rad = 0.2; fj_s_rad = "020";}
  else{ cout<<"Unknown jet radius"<<endl; return -1;}

  if(s_rebuild.find("Topo")!=string::npos){
    input_type = inTOPO;
    if(s_rebuild.find("LC")!=string::npos){signal_type = sigLC;}
    else if(s_rebuild.find("EM")!=string::npos){signal_type = sigEM;}
    else{ cout<<"Unknown signal state"<<endl; return -1;}
  }
  else if(s_rebuild.find("Track")!=string::npos){input_type = inTRK;}
  else if(s_rebuild.find("Truth")!=string::npos){input_type = inTRUTH;}
  else if(s_rebuild.find("Mitru")!=string::npos){input_type = inMITRU;}
  else{ cout<<"Unknown input objects"<<endl; return -1;}


  int i_pregroom = 0;
  if( s_rebuild.find("NoElCl") != string::npos ){ i_pregroom = i_pregroom | rc_el_cl;}
  else if( s_rebuild.find("NoElJet") != string::npos ){ i_pregroom = i_pregroom | rc_ak4_elj;}

  if( s_rebuild.find("NoLepB") != string::npos ){ i_pregroom = i_pregroom | rc_ak4_nolepb;}

  if( s_rebuild.find("Pregroom") != string::npos ){ i_pregroom = i_pregroom | rc_ak4_15;}
  else if( s_rebuild.find("NoB") != string::npos ){ i_pregroom = i_pregroom | rc_ak4_nonb;}
  else if( s_rebuild.find("OnlyB") != string::npos ){ i_pregroom = i_pregroom | rc_ak4_b;}
  /*
   cout<<"s_rebuild = "<<s_rebuild<<" fj_alg = "<<fj_alg<<" fj_rad = "<<fj_rad<<" signal_type = "<<signal_type<<" input_type = "<<input_type<<endl;
   cout<<"0 i_pregroom = "<<i_pregroom<<endl;
   cout<<"0 i_pregroom & rc_el_cl = "<<(i_pregroom & rc_el_cl)<<endl;
   cout<<"0 i_pregroom & rc_ak4_elj = "<<(i_pregroom & rc_ak4_elj)<<endl;
   cout<<"0 i_pregroom & rc_ak4_nolepb = "<<(i_pregroom & rc_ak4_nolepb)<<endl;
   cout<<"0 i_pregroom & rc_ak4_15 = "<<(i_pregroom & rc_ak4_15)<<endl;
   cout<<"0 i_pregroom & rc_ak4_nonb = "<<(i_pregroom & rc_ak4_nonb)<<endl;
   cout<<"0 i_pregroom & rc_ak4_b = "<<(i_pregroom & rc_ak4_b)<<endl;
  */

  //if ( (fj_alg == cambridge_algorithm) && (fj_s_rad =="150") ){ m_calib_Tool = new JetAnalysisCalib::JetCalibrationTool(algo+fj_s_rad+"LCTopo",config, m_isData);}
  //else{ _doCalib = false;  cout<<"No calibration available for specified algorithm"<<endl; }
  _doCalib = false;


  for(vector<int>::iterator v_it = v_sj_key.begin(); v_it != v_sj_key.end(); ++v_it ){

    int sj_key = *v_it;
    string sj = m_fatjetcol_IDs[sj_key]; 

    bool do_BDRS=false; bool do_filter=false; bool do_trim=false; bool do_HTT=false;

    if(sj.find("SplitFilt")!=string::npos){do_BDRS=true;}
    else if(sj.find("Filter")!=string::npos){do_filter=true;}

    if(sj.find("HEPTopTag")!=string::npos){do_HTT=true;}

    if(sj.find("Trim")!=string::npos){do_trim=true;}

    bool _callSubstruct = false;
    if(do_BDRS || do_trim){_callSubstruct = callSubstruct;}

    
    fatjet_n[sj_key] = 0;
    fatjet_pt[sj_key] = 0;
    fatjet_eta[sj_key] = 0;
    fatjet_phi[sj_key] = 0;
    fatjet_m[sj_key] = 0;
    fatjet_constscale_pt[sj_key] = 0;
    fatjet_constscale_eta[sj_key] = 0;
    fatjet_constscale_phi[sj_key] = 0;
    fatjet_constscale_m[sj_key] = 0;
    fatjet_ActiveArea[sj_key] = 0;
    fatjet_ActiveArea_px[sj_key] = 0;
    fatjet_ActiveArea_py[sj_key] = 0;
    fatjet_ActiveArea_pz[sj_key] = 0;
    fatjet_ActiveArea_E[sj_key] = 0;
    
    fatjet_SPLIT12[sj_key] = 0;
    fatjet_SPLIT23[sj_key] = 0;
    fatjet_ZCUT12[sj_key] = 0;
    fatjet_ZCUT23[sj_key] = 0;
    fatjet_WIDTH[sj_key] = 0;
    fatjet_PlanarFlow[sj_key] = 0;
    fatjet_Angularity[sj_key] = 0;
    fatjet_PullMag[sj_key] = 0;
    fatjet_PullPhi[sj_key] = 0;
    fatjet_Tau1[sj_key] = 0;
    fatjet_Tau2[sj_key] = 0;
    fatjet_Tau3[sj_key] = 0;
    fatjet_nbtag[sj_key] = 0;
    fatjet_nbtag_geom[sj_key] = 0;
    fatjet_MCclass[sj_key] = 0;
    fatjet_orig_ind[sj_key] = 0;
    fatjet_assoc_ind[sj_key] = 0;
    
    fatjet_constit_n[sj_key] = 0;    
    fatjet_constit_n[sj_key] = 0;
    fatjet_constit_index[sj_key] = 0;
    
    b_fatjet_n[sj_key] = 0;
    b_fatjet_pt[sj_key] = 0;
    b_fatjet_eta[sj_key] = 0;
    b_fatjet_phi[sj_key] = 0;
    b_fatjet_m[sj_key] = 0;
    b_fatjet_constscale_pt[sj_key] = 0;
    b_fatjet_constscale_eta[sj_key] = 0;
    b_fatjet_constscale_phi[sj_key] = 0;
    b_fatjet_constscale_m[sj_key] = 0;
    b_fatjet_ActiveArea[sj_key] = 0;
    b_fatjet_ActiveArea_px[sj_key] = 0;
    b_fatjet_ActiveArea_py[sj_key] = 0;
    b_fatjet_ActiveArea_pz[sj_key] = 0;
    b_fatjet_ActiveArea_E[sj_key] = 0;
    
    b_fatjet_SPLIT12[sj_key] = 0;
    b_fatjet_SPLIT23[sj_key] = 0;
    b_fatjet_ZCUT12[sj_key] = 0;
    b_fatjet_ZCUT23[sj_key] = 0;
    b_fatjet_WIDTH[sj_key] = 0;
    b_fatjet_PlanarFlow[sj_key] = 0;
    b_fatjet_Angularity[sj_key] = 0;
    b_fatjet_PullMag[sj_key] = 0;
    b_fatjet_PullPhi[sj_key] = 0;
    b_fatjet_Tau1[sj_key] = 0;
    b_fatjet_Tau2[sj_key] = 0;
    b_fatjet_Tau3[sj_key] = 0;
    b_fatjet_nbtag[sj_key] = 0;
    b_fatjet_nbtag_geom[sj_key] = 0;
    b_fatjet_MCclass[sj_key] = 0;
    b_fatjet_orig_ind[sj_key] = 0;
    b_fatjet_assoc_ind[sj_key] = 0;
    
    b_fatjet_constit_n[sj_key] = 0;
    b_fatjet_constit_index[sj_key] = 0;

    fatjet_pt[sj_key] = new vector<float>;
    fatjet_eta[sj_key] = new vector<float>;
    fatjet_phi[sj_key] = new vector<float>;
    fatjet_m[sj_key] = new vector<float>;
    fatjet_constscale_pt[sj_key] = new vector<float>;
    fatjet_constscale_eta[sj_key] = new vector<float>;
    fatjet_constscale_phi[sj_key] = new vector<float>;
    fatjet_constscale_m[sj_key] = new vector<float>;
    fatjet_ActiveArea[sj_key] = new vector<float>;
    fatjet_ActiveArea_px[sj_key] = new vector<float>;
    fatjet_ActiveArea_py[sj_key] = new vector<float>;
    fatjet_ActiveArea_pz[sj_key] = new vector<float>;
    fatjet_ActiveArea_E[sj_key] = new vector<float>;

    fatjet_nbtag[sj_key] = new vector<int>;
    fatjet_nbtag_geom[sj_key] = new vector<int>;
    fatjet_orig_ind[sj_key] = new vector<int>;
    fatjet_assoc_ind[sj_key] = new vector<int>;
    fatjet_MCclass[sj_key] = new vector<int>;

    fatjet_constit_n[sj_key] = new vector<int>;
    fatjet_constit_index[sj_key] = new vector<vector<int> >;

    fatjet_pt[sj_key]->clear();
    fatjet_eta[sj_key]->clear();
    fatjet_phi[sj_key]->clear();
    fatjet_m[sj_key]->clear();
    fatjet_constscale_pt[sj_key]->clear();
    fatjet_constscale_eta[sj_key]->clear();
    fatjet_constscale_phi[sj_key]->clear();
    fatjet_constscale_m[sj_key]->clear();
    fatjet_ActiveArea[sj_key]->clear();
    fatjet_ActiveArea_px[sj_key]->clear();
    fatjet_ActiveArea_py[sj_key]->clear();
    fatjet_ActiveArea_pz[sj_key]->clear();
    fatjet_ActiveArea_E[sj_key]->clear();

    fatjet_nbtag[sj_key]->clear();
    fatjet_nbtag_geom[sj_key]->clear();
    fatjet_orig_ind[sj_key]->clear();
    fatjet_assoc_ind[sj_key]->clear();
    fatjet_MCclass[sj_key]->clear();

    fatjet_constit_n[sj_key]->clear();
    fatjet_constit_index[sj_key]->clear();

    if(_callSubstruct){
      fatjet_SPLIT12[sj_key] = new vector<float>;
      fatjet_SPLIT23[sj_key] = new vector<float>;
      fatjet_ZCUT12[sj_key] = new vector<float>;
      fatjet_ZCUT23[sj_key] = new vector<float>;
      fatjet_WIDTH[sj_key] = new vector<float>;
      fatjet_PlanarFlow[sj_key] = new vector<float>;
      fatjet_Angularity[sj_key] = new vector<float>;
      fatjet_PullMag[sj_key] = new vector<float>;
      fatjet_PullPhi[sj_key] = new vector<float>;
      fatjet_Tau1[sj_key] = new vector<float>;
      fatjet_Tau2[sj_key] = new vector<float>;
      fatjet_Tau3[sj_key] = new vector<float>;

      fatjet_SPLIT12[sj_key]->clear();
      fatjet_SPLIT23[sj_key]->clear();
      fatjet_ZCUT12[sj_key]->clear();
      fatjet_ZCUT23[sj_key]->clear();
      fatjet_WIDTH[sj_key]->clear();
      fatjet_PlanarFlow[sj_key]->clear();
      fatjet_Angularity[sj_key]->clear();
      fatjet_PullMag[sj_key]->clear();
      fatjet_PullPhi[sj_key]->clear();
      fatjet_Tau1[sj_key]->clear();
      fatjet_Tau2[sj_key]->clear();
      fatjet_Tau3[sj_key]->clear();
    }

  }

  int const_in_n = 0;
  vector<PseudoJet> myconsts_in; 
  if(v_inputs.size() > 0){myconsts_in = v_inputs;}
  else{ myconsts_in = FillJetInputCollection(i_pregroom, input_type, signal_type); }
  const_in_n = myconsts_in.size();

  if(myconsts_in.size()<=0){return 0;}
  int const_mc_gh_n = 0;
  if(ghost_match_mc){
    //loop over anti-kT 0.4 jets, check if the jet is b-tagged, then change the pT to a soft value (1e-5) and add to myconsts_in
    //then later when extracting the constituents, check that the user index is postive. When extractng the ghosts, check that the
    //user index is < -900.

    if(m_ip_leptop_b >= 0){
      double geta = mc_eta->at(m_ip_leptop_b); double gphi = mc_phi->at(m_ip_leptop_b); double gpt = 1e-5;
      double cg_px = gpt*cos(gphi), cg_py = gpt*sin(gphi), cg_pz = gpt*sinh(geta), cg_E = gpt*cosh(geta);
      PseudoJet cg(cg_px, cg_py, cg_pz, cg_E);
      cg.set_user_index(-(MC_GHOST_SHIFT + I_LEPTOP_B));
      myconsts_in.push_back(cg);
      const_mc_gh_n++;
    }
    if(m_ip_hadtop_b >= 0){
      double geta = mc_eta->at(m_ip_hadtop_b); double gphi = mc_phi->at(m_ip_hadtop_b); double gpt = 1e-5;
      double cg_px = gpt*cos(gphi), cg_py = gpt*sin(gphi), cg_pz = gpt*sinh(geta), cg_E = gpt*cosh(geta);
      PseudoJet cg(cg_px, cg_py, cg_pz, cg_E);
      cg.set_user_index(-(MC_GHOST_SHIFT + I_HADTOP_B));
      myconsts_in.push_back(cg);
      const_mc_gh_n++;
    }
    if(m_ip_hadtop_q1 >= 0){
      double geta = mc_eta->at(m_ip_hadtop_q1); double gphi = mc_phi->at(m_ip_hadtop_q1); double gpt = 1e-5;
      double cg_px = gpt*cos(gphi), cg_py = gpt*sin(gphi), cg_pz = gpt*sinh(geta), cg_E = gpt*cosh(geta);
      PseudoJet cg(cg_px, cg_py, cg_pz, cg_E);
      cg.set_user_index(-(MC_GHOST_SHIFT + I_HADTOP_Q1));
      myconsts_in.push_back(cg);
      const_mc_gh_n++;
    }
    if(m_ip_hadtop_q2 >= 0){
      double geta = mc_eta->at(m_ip_hadtop_q2); double gphi = mc_phi->at(m_ip_hadtop_q2); double gpt = 1e-5;
      double cg_px = gpt*cos(gphi), cg_py = gpt*sin(gphi), cg_pz = gpt*sinh(geta), cg_E = gpt*cosh(geta);
      PseudoJet cg(cg_px, cg_py, cg_pz, cg_E);
      cg.set_user_index(-(MC_GHOST_SHIFT + I_HADTOP_Q2));
      myconsts_in.push_back(cg);
      const_mc_gh_n++;
    }
    if(m_ip_higgs_b1 >= 0){
      double geta = mc_eta->at(m_ip_higgs_b1); double gphi = mc_phi->at(m_ip_higgs_b1); double gpt = 1e-5;
      double cg_px = gpt*cos(gphi), cg_py = gpt*sin(gphi), cg_pz = gpt*sinh(geta), cg_E = gpt*cosh(geta);
      PseudoJet cg(cg_px, cg_py, cg_pz, cg_E);
      cg.set_user_index(-(MC_GHOST_SHIFT + I_HIGGS_B1));
      myconsts_in.push_back(cg);
      const_mc_gh_n++;
    }
    if(m_ip_higgs_b2 >= 0){
      double geta = mc_eta->at(m_ip_higgs_b2); double gphi = mc_phi->at(m_ip_higgs_b2); double gpt = 1e-5;
      double cg_px = gpt*cos(gphi), cg_py = gpt*sin(gphi), cg_pz = gpt*sinh(geta), cg_E = gpt*cosh(geta);
      PseudoJet cg(cg_px, cg_py, cg_pz, cg_E);
      cg.set_user_index(-(MC_GHOST_SHIFT + I_HIGGS_B2));
      myconsts_in.push_back(cg);
      const_mc_gh_n++;
    }

  }//if ghost matching MC partons


  int const_gh_n = 0;
  if(ghost_match_b){
    //loop over anti-kT 0.4 jets, check if the jet is b-tagged, then change the pT to a soft value (1e-5) and add to myconsts_in
    //then later when extracting the constituents, check that the user index is postive. When extractng the ghosts, check that the
    //user index is < -9000.
    for(int i = 0; i<jet_n; i++){
      if( (jet_pt->at(i) < m_jet_ptmin) || (fabs(jet_eta->at(i)) > m_jet_etamax) || (jet_MV1->at(i) < m_jet_mv1cut) ){continue;}
      double  geta = jet_eta->at(i), gphi = jet_phi->at(i), gpt = 1e-5; //gpt = jet_pt[i];
      double cg_px = gpt*cos(gphi), cg_py = gpt*sin(gphi), cg_pz = gpt*sinh(geta), cg_E = gpt*cosh(geta);
      
      PseudoJet cg(cg_px, cg_py, cg_pz, cg_E);
      cg.set_user_index(-(B_GHOST_SHIFT + i));
      myconsts_in.push_back(cg);
      const_gh_n++;
    }//b-jet loop

  }//if ghost matching b-jets

  JetDefinition jetDef = JetDefinition(fj_alg,fj_rad, Best);
  AreaDefinition areaDef = AreaDefinition(active_area, GhostedAreaSpec(SelectorAbsRapMax(4.0))); //GhostedAreaSpec(6.0, 1, 0.01, 1.0, 0.1, 1e-100));
  ClusterSequenceArea csDef(myconsts_in, jetDef, areaDef);

  vector<PseudoJet> thejetcol_out;
  thejetcol_out.clear();

  Selector sel_pt = SelectorPtMin(fj_ptmin);
  Selector sel_eta = SelectorAbsEtaMax(fj_etamax);
  Selector sel_out = sel_pt && sel_eta;

  if(i_excl == 0){
    thejetcol_out = sel_out( csDef.inclusive_jets(fj_ptmin) ); 
  }
  else{
    thejetcol_out = sel_out( csDef.exclusive_jets(i_excl) ); 
  }
  thejetcol_out = sorted_by_pt(thejetcol_out);

  //vector<PseudoJet> out_pj; out_pj.clear();

  for(int t = 0; t < thejetcol_out.size(); t++){

    thejetcol_out.at(t).set_user_index(-1*t);
  }

  //{
  //PseudoJet pj = thejetcol_out.at(t);
  //pj.set_user_index(-1*t);

    /*
    if(doCalib){

      PseudoJet calib_pj;
      calib_pj = ApplyJetCalibration( pj, csDef, R_para, m_InclusiveSubjets_areaCorrection);
      // cout << "Incl: calb_pj = " << calib_pj.perp() << endl;
      if (m_ScaleFactor_JES != 1) {
	fastjet::PseudoJet calib_pj_scaled = fastjet::PseudoJet( calib_pj.px()*m_ScaleFactor_JES,
								 calib_pj.py()*m_ScaleFactor_JES,
								 calib_pj.pz()*m_ScaleFactor_JES,
								 calib_pj.e()*m_ScaleFactor_JES );
	out_pj.push_back( calib_pj_scaled );
      }
      else {
	out_pj.push_back( calib_pj );
      }
    }
    else{
      out_pj.push_back(pj);
    }

  }
    */


  //000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  //Have the main ungroomed jet collection. Loop over v_jetcol and decide on grooming

  for(vector<int>::iterator v_it = v_sj_key.begin(); v_it != v_sj_key.end(); ++v_it ){
    int sj_key = *v_it;
    string sj = m_fatjetcol_IDs[sj_key]; 

    bool do_BDRS=false; bool do_filter=false; bool do_trim=false; bool do_HTT=false;

    if(sj.find("SplitFilt")!=string::npos){do_BDRS=true;}
    else if(sj.find("Filter")!=string::npos){do_filter=true;}

    if(sj.find("HEPTopTag")!=string::npos){do_HTT=true;}

    if(sj.find("Trim")!=string::npos){do_trim=true;}

    bool _callSubstruct = false;
    if(do_BDRS || do_trim){_callSubstruct = callSubstruct;}


    int count_groom=0;
    if(do_BDRS){count_groom++;}
    if(do_filter){count_groom++;}
    if(do_trim){count_groom++;}
    if(do_HTT){count_groom++;}

    // cout<<"do_BDRS = "<<do_BDRS<<" do_HTT = "<<do_HTT<<" do_filter = "<<do_filter<<" do_trim = "<<do_trim<<endl;
    //if( (do_BDRS && do_filter ) || (do_BDRS && do_HTT) || (do_BDRS && do_trim) || (do_filter && do_HTT) || (do_filter && do_trim) || (do_HTT && do_trim) ){
    if(count_groom > 1){ cout<<"More than one grooming not supported"<<endl; continue;}
    

    vector<PseudoJet> thejetcol;
    thejetcol.clear();
    
    map<int, vector<int> > m_const_index;
    m_const_index.clear();

    //TLorentzVector tlv_check_const;
    //tlv_check_const.SetPtEtaPhiM(mc_pt->at(co_ind), mc_eta->at(co_ind), mc_phi->at(co_ind), mc_m->at(co_ind));
    //tlv_check_sum += tlv_check_const;

    //BDRS tagging
    if(do_BDRS){
      vector<PseudoJet> thejetcol_filt;
      thejetcol_filt.clear();

      double mu_filt = 0.67; 
      double ycut_filt = 0.09;
      MassDropTagger mdtagger(mu_filt, ycut_filt);

      int i_BDRS = 0;      
      for(vector<PseudoJet>::iterator jit = thejetcol_out.begin(); jit!=thejetcol_out.end(); ++jit){
	// cout<<"jit->perp() = "<<jit->perp()/GeV<<endl;
	if(jit->perp() > fj_ptmin && fabs(jit->eta()) < fj_etamax){ //Run tagger only for jets in acceptance range
	  PseudoJet tagged_jet = mdtagger(*jit);
	  if(tagged_jet != 0){
	    vector<PseudoJet> tagged_pieces = tagged_jet.pieces();
	    double Rfilt = min(0.3, 0.5*tagged_pieces[0].delta_R(tagged_pieces[1]));
	    PseudoJet filtered_tagged_jet = Filter(Rfilt, SelectorNHardest(3))(tagged_jet); 
	    //filtered_tagged_jet.set_user_index(-1*i_BDRS);
	    filtered_tagged_jet.set_user_index(jit->user_index());
	    if( (filtered_tagged_jet.perp() > fj_ptmin_groom) && ( fabs(filtered_tagged_jet.eta()) < fj_etamax) ){
	      thejetcol_filt.push_back(filtered_tagged_jet);
	    
	      vector<int> v_co_ind; v_co_ind.clear();
	      vector<PseudoJet> const_out=filtered_tagged_jet.constituents();
	      for(vector<PseudoJet>::iterator cjit=const_out.begin(); cjit!=const_out.end(); ++cjit){
		int co_ind = cjit->user_index();
		v_co_ind.push_back(co_ind);
	      }
	      const_out.clear();
	      //m_const_index[-1*i_BDRS] = v_co_ind;
	      m_const_index[jit->user_index()] = v_co_ind;
	    }//if in acceptance
	  }//if_tagged
	}//ungroomed jet in acceptance range
	i_BDRS++;
      }//jet_loop
      //sort by pt
      thejetcol = sorted_by_pt(thejetcol_filt);
    }
    else if(do_HTT){



      cout<<"NO HTT code anymore!"<<endl;
      /*
      int i_HTT = 0;
      vector<PseudoJet> thejetcol_htt;
      thejetcol_htt.clear();
      
      // Run the HEPToptagger
      // Loop over the fatjets
      // mass window
      //Range mt_range(140.*GeV, 210.*GeV);
      Range mt_range(0.*GeV, 10.E4*GeV);
      //ClusterSequenceArea* cs = new ClusterSequenceArea(csDef); //*cs = csDef; //copy the original CS
      for(vector<PseudoJet>::iterator jit = thejetcol_out.begin(); jit!=thejetcol_out.end(); ++jit){
	if(jit->perp() > fj_ptmin && fabs(jit->eta())<fj_etamax){ //Run tagger only for jets in acceptance range
	  if (!(*jit).has_constituents()){i_HTT++; continue;}
	  //PseudoJet* _jet = new PseudoJet(); *_jet = *jit;
	  TopTagger toptagger( csDef,               // cluster sequence
			       *jit,   // fatjet
			       0.8,              // mass drop
			       172.3 * 1000.,    // top mass
			       80.4*1000.,       // W mass
			       mt_range,         // top mass window
			       0.3,              // filtering radius parameter
			       5,                // filtering njets parameter
			       30.*1000,         // mass cut off 
			       0.2,              // lower x bound
			       1.3,              // upper x bound
			       0.35,             // lower y bound
			       0.3,              // rel mass width
			       JetAlgorithm(1),  // tagger algo (1==C/A)   
			       false,            // sort candidates by pT (instead of closeness to 172.3)
			       false,	    // cut on massplane before sorting
			       inclCsFunctionTA,   // clustering+calibration for inclusive subjets
			       exclCsFunctionTA ); // clustering+calibration for exclusive subjets
	  
	  // check if the fatjet is tagged
	  bool is_toptagged = (toptagger.maybe_top( mt_range ) && toptagger.is_masscut_passed());
	  if(is_toptagged){
	    PseudoJet jet_htt = toptagger.top_candidate();
	    if( (jet_htt.perp() > fj_ptmin_groom) && ( fabs(jet_htt.eta()) < fj_etamax) ){
	      //jet_htt.set_user_index(-1*i_HTT);
	      jet_htt.set_user_index(jit->user_index());
	      thejetcol_htt.push_back(jet_htt);
	    
	      const vector<vector<int> >* v_subjet_const = toptagger.return_subjet_clusters();
	      vector<int> v_tt_const;
	    
	      //int j_const_n = 0;
	      for(int jj = 0; jj < v_subjet_const->size(); jj++){
		for(int kk = 0; kk < v_subjet_const->at(jj).size(); kk++){
		  v_tt_const.push_back(v_subjet_const->at(jj).at(kk)); //j_const_n++;
		}
	      }
	      // cout<<"v_tt_const.size() = "<<v_tt_const.size()<<endl;
	      //m_const_index[-1*i_HTT] = v_tt_const;
	      m_const_index[jit->user_index()] = v_tt_const;

	      v_tt_const.clear(); //delete v_subjet_const;
	    }//if in acceptance
	  }//if_toptag
	}//ungroomed jet in acceptance range
	i_HTT++;
      }//jet_loop
      //sort by pt
      thejetcol = sorted_by_pt(thejetcol_htt);
      //thejetcol = thejetcol_htt;
         
      for(int t = 0; t < thejetcol_out.size(); t++){
	 cout<<"03 UNGROOMED t = "<<t<<" jet Pt = "<<thejetcol_out.at(t).perp()<<" jet Eta = "<<thejetcol_out.at(t).eta()<<" jet Phi = "<<thejetcol_out.at(t).phi()<<endl;
      }
      */





    }
    else if(do_trim){
      vector<PseudoJet> thejetcol_trim;
      thejetcol_trim.clear();
      
      int i_trim = 0;

      double ptfrac_trim = 0.05;
      double r_trim = 0.2;
      
      Transformer *trimmer = new Filter(JetDefinition(kt_algorithm, r_trim), SelectorPtFractionMin(ptfrac_trim) ) ;
      const Transformer & groom = *trimmer;
      
      for(vector<PseudoJet>::iterator jit = thejetcol_out.begin(); jit!=thejetcol_out.end(); ++jit){
	if(jit->perp() > fj_ptmin && fabs(jit->eta())<fj_etamax){ //Run tagger only for jets in acceptance range
	  PseudoJet trimmed_jet = groom(*jit);
	  if(trimmed_jet != 0){
	    if( (trimmed_jet.perp() > fj_ptmin_groom) && ( fabs(trimmed_jet.eta()) < fj_etamax) ){

	      //trimmed_jet.set_user_index(-1*i_trim);
	      trimmed_jet.set_user_index(jit->user_index());
	      thejetcol_trim.push_back(trimmed_jet);
	    
	      vector<int> v_co_ind; v_co_ind.clear();
	      vector<PseudoJet> const_out=trimmed_jet.constituents();
	      for(vector<PseudoJet>::iterator cjit=const_out.begin(); cjit!=const_out.end(); ++cjit){
		int co_ind = cjit->user_index();
		v_co_ind.push_back(co_ind);
	      }
	      const_out.clear();
	      //m_const_index[-1*i_trim] = v_co_ind;
	      m_const_index[jit->user_index()] = v_co_ind;
	    }//if in acceptance
	  }//if trim_jet
	}//ungroomed jet in acceptance range
	i_trim++;
      }//jet_loop
      //sort by pt
      thejetcol = sorted_by_pt(thejetcol_trim);
      delete trimmer;
    }
    else{
      thejetcol = thejetcol_out;
      int i_ungroom = 0;
      for(vector<PseudoJet>::iterator jit = thejetcol_out.begin(); jit!=thejetcol_out.end(); ++jit){
	vector<int> v_co_ind; v_co_ind.clear();
	vector<PseudoJet> const_out=jit->constituents();
	for(vector<PseudoJet>::iterator cjit=const_out.begin(); cjit!=const_out.end(); ++cjit){
	  int co_ind = cjit->user_index();
	  v_co_ind.push_back(co_ind);
	}
	const_out.clear();
	m_const_index[jit->user_index()] = v_co_ind;
	i_ungroom++;
      }//jet_loop
    }

    fatjet_n[sj_key] = thejetcol.size();

    int counter_j=0;
    for(vector<PseudoJet>::iterator pjit=thejetcol.begin(); pjit!=thejetcol.end(); ++pjit){
      //TLorentzVector tlv_check_sum; TLorentzVector tlv_check_j;
      //tlv_check_j.SetPtEtaPhiM(pjit->perp(), pjit->eta(), pjit->phi(), pjit->m());
      //if (!(*pjit).has_constituents()){continue;}
      // cout<<"counter_j = "<<counter_j<<endl;
      if(input_type == inTOPO){      
	/*
	fatjet_pt[sj_key]->push_back(-1.);
	fatjet_m[sj_key]->push_back(-1.);
	fatjet_eta[sj_key]->push_back(-1.);
	fatjet_phi[sj_key]->push_back(-1.);
	*/
	//Store area-corrected quantities as "calibrated" ones

	fatjet_constscale_pt[sj_key]->push_back(pjit->perp());
	fatjet_constscale_m[sj_key]->push_back(pjit->m());
	fatjet_constscale_eta[sj_key]->push_back(pjit->eta());
	fatjet_constscale_phi[sj_key]->push_back(pjit->phi_std());

	TLorentzVector tlv_j_corr;
	tlv_j_corr.SetPtEtaPhiM(pjit->perp(), pjit->eta(), pjit->phi(), pjit->m());
	TLorentzVector tlv_a_corr;
	if(pjit->has_area()){
	  tlv_a_corr.SetPtEtaPhiM((pjit->area_4vector()).perp(), (pjit->area_4vector()).eta(), (pjit->area_4vector()).phi(), (pjit->area_4vector()).m());
	}
	else{tlv_a_corr.SetPtEtaPhiM(0.,-1.,0.,0.);}
	//tlv_j_corr -= t_Eventshape_rhoKt6LC * tlv_a_corr;

	fatjet_pt[sj_key]->push_back(tlv_j_corr.Pt());
	fatjet_eta[sj_key]->push_back(tlv_j_corr.Eta());
	fatjet_phi[sj_key]->push_back(tlv_j_corr.Phi());
	fatjet_m[sj_key]->push_back(tlv_j_corr.M());
      }
      else{

	TLorentzVector tlv_j;
	tlv_j.SetPtEtaPhiM(pjit->perp(), pjit->eta(), pjit->phi(), pjit->m());

	double Px_corr=tlv_j.Px()*JESsyst;
	double Py_corr=tlv_j.Py()*JESsyst;
	double Pz_corr=tlv_j.Pz()*JESsyst;
	double E_corr=tlv_j.E()*JESsyst;

	TLorentzVector tlv_j_corr;
	tlv_j_corr.SetPxPyPzE(Px_corr,Py_corr,Pz_corr,E_corr);


	fatjet_constscale_pt[sj_key]->push_back(pjit->perp());
	fatjet_constscale_m[sj_key]->push_back(pjit->m());
	fatjet_constscale_eta[sj_key]->push_back(pjit->eta());
	fatjet_constscale_phi[sj_key]->push_back(pjit->phi_std());

	fatjet_pt[sj_key]->push_back(tlv_j_corr.Pt());
	fatjet_m[sj_key]->push_back(tlv_j_corr.M());
	fatjet_eta[sj_key]->push_back(tlv_j_corr.Eta());
	fatjet_phi[sj_key]->push_back(tlv_j_corr.Phi());
      }
      fatjet_orig_ind[sj_key]->push_back(pjit->user_index());
      fatjet_assoc_ind[sj_key]->push_back(-1);

      if(pjit->has_area()){
	fatjet_ActiveArea[sj_key]->push_back(pjit->area());
	fatjet_ActiveArea_px[sj_key]->push_back((pjit->area_4vector()).px());
	fatjet_ActiveArea_py[sj_key]->push_back((pjit->area_4vector()).py());
	fatjet_ActiveArea_pz[sj_key]->push_back((pjit->area_4vector()).pz());
	fatjet_ActiveArea_E[sj_key]->push_back((pjit->area_4vector()).E());
      }
      else{
	fatjet_ActiveArea[sj_key]->push_back(-1.);
	fatjet_ActiveArea_px[sj_key]->push_back(-1.);
	fatjet_ActiveArea_py[sj_key]->push_back(-1.);
	fatjet_ActiveArea_pz[sj_key]->push_back(-1.);
	fatjet_ActiveArea_E[sj_key]->push_back(-1.);
      }
      
      vector<PseudoJet> v_const_ss; //constituents
      v_const_ss.clear();

      if (pjit->has_constituents()){
	v_const_ss = pjit->constituents();
      }//has constituents
      if( m_const_index.find(pjit->user_index()) != m_const_index.end() ){
	if(!(ghost_match_b || ghost_match_mc)){
	  fatjet_constit_n[sj_key]->push_back(m_const_index[pjit->user_index()].size());
	  fatjet_constit_index[sj_key]->push_back(m_const_index[pjit->user_index()]);
	  fatjet_nbtag[sj_key]->push_back(-1);
	  fatjet_nbtag_geom[sj_key]->push_back(-1);
	} // no b-jet matching
	else{
	  int nconst_r = 0, nconst_g_b = 0, nconst_g_mc = 0;
	  vector<int> vconst_r; vconst_r.clear();
	  vector<int> vconst_gh_mc; vconst_gh_mc.clear();
	  for(int i = 0; i<m_const_index[pjit->user_index()].size(); i++){
	    int c_ind = m_const_index[pjit->user_index()].at(i);
	    if(c_ind >= 0){
	      vconst_r.push_back(c_ind);
	      nconst_r++;
	    }
	    else if(c_ind <= -MC_GHOST_SHIFT && c_ind > -B_GHOST_SHIFT){
	      // cout<<"ghost ind = "<<-(c_ind + MC_GHOST_SHIFT)<<endl;
	      vconst_gh_mc.push_back(-(c_ind + MC_GHOST_SHIFT));
	      nconst_g_mc++;
	    }
	    else if(c_ind <= -B_GHOST_SHIFT){
	      nconst_g_b++;
	      /*
	      int gh_ind = -(c_ind + 9000);
	      double  geta = jet_eta[gh_ind], gphi = jet_phi[gh_ind], gpt = jet_pt[gh_ind];
	      double cg_px = gpt*cos(gphi), cg_py = gpt*sin(gphi), cg_pz = gpt*sinh(geta), cg_E = gpt*cosh(geta);
	      PseudoJet cg(cg_px, cg_py, cg_pz, cg_E);
	      double gh_dr = pjit->delta_R(cg);
	       cout<<" fatjet pt = "<<pjit->perp()/GeV<<" fatjet m = "<<pjit->m()/GeV<<" ghost pT = "<<gpt/GeV<<" ghost dR = "<<gh_dr<<endl;
	      */
	    }
	  }//constituent loop

	  fatjet_constit_n[sj_key]->push_back(nconst_r);
	  fatjet_constit_index[sj_key]->push_back(vconst_r);

	  if(ghost_match_b){
	    fatjet_nbtag[sj_key]->push_back(nconst_g_b);
	  }
	  else{
	    fatjet_nbtag[sj_key]->push_back(-1);
	  }
	  if(ghost_match_mc){
	    fatjet_MCclass[sj_key]->push_back(ClassifyMCJets(vconst_gh_mc));
	  }

	} //if ghost matching b-jets or MC partons

	/*
	for(int ci = 0; ci < fatjet_constit_n[sj_key]->at(counter_j); ci++){
	  TLorentzVector tlv_check_const;
	  int cind = fatjet_constit_index[sj_key]->at(counter_j).at(ci);
	  tlv_check_const.SetPtEtaPhiM(cl_pt->at(cind), cl_eta->at(cind), cl_phi->at(cind), 0);
	  tlv_check_sum += tlv_check_const;
	}
	*/
	if(!pjit->has_constituents()){
	  for(int i = 0; i<fatjet_constit_n[sj_key]->at(counter_j); i++ ){
	    int ij = fatjet_constit_index[sj_key]->at(counter_j).at(i);
	    TLorentzVector tlv_c;
	    if(input_type == inTOPO){
	      tlv_c.SetPtEtaPhiM(cl_pt->at(ij), cl_eta->at(ij), cl_phi->at(ij), 0.);
	    }
	    else if(input_type == inTRUTH){
	      tlv_c.SetPtEtaPhiM(mc_pt->at(ij), mc_eta->at(ij), mc_phi->at(ij), mc_m->at(ij));
	    }
	    else if(input_type == inMITRU){
	      tlv_c.SetPtEtaPhiE(cell_pt->at(ij), cell_eta->at(ij), cell_phi->at(ij), cell_E->at(ij));
	    }
	    else{tlv_c.SetPtEtaPhiM(-1.,-1.,-1.,-1.);}
	    PseudoJet pj_c(tlv_c.Px(), tlv_c.Py(), tlv_c.Pz(), tlv_c.E()); 
	    v_const_ss.push_back(pj_c);
	  }
	}
      }//if have constituent indices
      else{
	fatjet_constit_n[sj_key]->push_back(0);
	fatjet_nbtag[sj_key]->push_back(-1);
	fatjet_nbtag_geom[sj_key]->push_back(-1);
	fatjet_MCclass[sj_key]->push_back(-1);
      }
	
      if(_callSubstruct){
	if(fatjet_constit_n[sj_key]->at(counter_j) > 0){
	  JetDefinition jet_def = JetDefinition(kt_algorithm,M_PI/2,E_scheme,Best);
	  const ClusterSequence kt_clust_seq(v_const_ss, jet_def);

	  double split12 = kT_Split(kt_clust_seq, 1);
	  double split23 = kT_Split(kt_clust_seq, 2);

	  double zcut12 = kT_Zcut(split12, pjit->m());
	  double zcut23 = kT_Zcut(split23, pjit->m());

	  double tau1 = Tau(v_const_ss, kt_clust_seq, 1, fj_rad);
	  double tau2 = Tau(v_const_ss, kt_clust_seq, 2, fj_rad);
	  double tau3 = Tau(v_const_ss, kt_clust_seq, 3, fj_rad);
 
	  fatjet_SPLIT12[sj_key]->push_back(split12);
	  fatjet_SPLIT23[sj_key]->push_back(split23);
	  fatjet_ZCUT12[sj_key]->push_back(zcut12);
	  fatjet_ZCUT23[sj_key]->push_back(zcut23);
	  /*
	    fatjet_WIDTH[sj_key]->push_back(Width(v_const_ss));
	    fatjet_PlanarFlow[sj_key]->push_back(PlanarFlow(v_const_ss));
	    fatjet_Angularity[sj_key]->push_back(Angularity(v_const_ss));
	  */
	  fatjet_WIDTH[sj_key]->push_back(-1.);
	  fatjet_PlanarFlow[sj_key]->push_back(-1.);
	  fatjet_Angularity[sj_key]->push_back(-1.);
	  fatjet_PullMag[sj_key]->push_back(-1.);
	  fatjet_PullPhi[sj_key]->push_back(-1.);
	  fatjet_Tau1[sj_key]->push_back(tau1);
	  fatjet_Tau2[sj_key]->push_back(tau2);
	  fatjet_Tau3[sj_key]->push_back(tau3);

	  /*
	    fatjet_SPLIT12[sj_key]->push_back(-1.);
	    fatjet_SPLIT23[sj_key]->push_back(-1.);
	    fatjet_ZCUT12[sj_key]->push_back(-1.);
	    fatjet_ZCUT23[sj_key]->push_back(-1.);
	    fatjet_WIDTH[sj_key]->push_back(-1.);
	    fatjet_PlanarFlow[sj_key]->push_back(-1.);
	    fatjet_Angularity[sj_key]->push_back(-1.);
	    fatjet_PullMag[sj_key]->push_back(-1.);
	    fatjet_PullPhi[sj_key]->push_back(-1.);
	    fatjet_Tau1[sj_key]->push_back(-1.);
	    fatjet_Tau2[sj_key]->push_back(-1.);
	    fatjet_Tau3[sj_key]->push_back(-1.);
	  */

	}
	else{
	  fatjet_SPLIT12[sj_key]->push_back(-1.);
	  fatjet_SPLIT23[sj_key]->push_back(-1.);
	  fatjet_ZCUT12[sj_key]->push_back(-1.);
	  fatjet_ZCUT23[sj_key]->push_back(-1.);
	  fatjet_WIDTH[sj_key]->push_back(-1.);
	  fatjet_PlanarFlow[sj_key]->push_back(-1.);
	  fatjet_Angularity[sj_key]->push_back(-1.);
	  fatjet_PullMag[sj_key]->push_back(-1.);
	  fatjet_PullPhi[sj_key]->push_back(-1.);
	  fatjet_Tau1[sj_key]->push_back(-1.);
	  fatjet_Tau2[sj_key]->push_back(-1.);
	  fatjet_Tau3[sj_key]->push_back(-1.);
	}
      }
      /*
       cout<<counter_j<<" jet pt = "<<tlv_check_j.Pt()<<" const sum pt = "<<tlv_check_sum.Pt()<<endl;
       cout<<counter_j<<" jet eta = "<<tlv_check_j.Eta()<<" const sum eta = "<<tlv_check_sum.Eta()<<endl;
       cout<<counter_j<<" jet phi = "<<tlv_check_j.Phi()<<" const sum phi = "<<tlv_check_sum.Phi()<<endl;
       cout<<counter_j<<" jet m = "<<tlv_check_j.M()<<" const sum m = "<<tlv_check_sum.M()<<endl;
      */
      counter_j++;
    }//thejetcol loop
      
    sj.clear();
    thejetcol.clear();

  }//v_sj_key loop

  thejetcol_out.clear();

  return 0;
}


//000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000


double EventRecoAnalysis::Angularity(const PseudoJet &jet){
  double tau_a=-1.;
  return tau_a;
}

double EventRecoAnalysis::PlanarFlow(const PseudoJet &jet){
  double pf=-1.;
  return pf;
}

double EventRecoAnalysis::Width(const PseudoJet &jet){
  double wdt=-1.;
  return wdt;
}


double EventRecoAnalysis::Angularity(vector<PseudoJet> constit){
  double tau_a=-1.;
  return tau_a;
}

double EventRecoAnalysis::PlanarFlow(vector<PseudoJet> constit){
  double pf=-1.;
  return pf;
}

double EventRecoAnalysis::Width(vector<PseudoJet> constit){
  double wdt=-1.;
  return wdt;
}


double EventRecoAnalysis::kT_Split(const PseudoJet &jet, int degree){
  double split=-1.;
  if (!jet.has_constituents()){
     cout<<"ERROR! kT_Split can only be applied on jets for which the constituents are known."<<endl;
    return split;
  }

  vector<PseudoJet> constits = jet.constituents();

  split = kT_Split(constits, degree);

  return split;
}

double EventRecoAnalysis::kT_Split(vector<PseudoJet> constits, int degree){
  double split=-1.;

  JetDefinition ekt_jd = JetDefinition(kt_algorithm,1.5,E_scheme,Best);
  const ClusterSequence kt_seq_excl = ClusterSequence(constits,ekt_jd);
  split = kT_Split(kt_seq_excl, degree);

  return split;
}

double EventRecoAnalysis::kT_Split(const ClusterSequence &cs, int degree){
  double split=-1.;

  PseudoJet kt_jet = sorted_by_pt(cs.inclusive_jets())[0];
  //NOTE: Add check that degree does not exceed number of clustering steps
  split = 1.5*sqrt(cs.exclusive_subdmerge(kt_jet, degree));
  return split;
}

double EventRecoAnalysis::kT_Zcut(const PseudoJet &jet, int degree){
  double zcut=-1.;
  if (!jet.has_constituents()){
     cout<<"ERROR! kT_Zcut can only be applied on jets for which the constituents are known."<<endl;
    return zcut;
  }

  double mass = jet.m();
  vector<PseudoJet> constits = jet.constituents();

  zcut = kT_Zcut(constits, mass, degree);

  return zcut;
}

double EventRecoAnalysis::kT_Zcut(vector<PseudoJet> constits, double mass, int degree){
  double zcut=-1.;

  double split = kT_Split(constits, degree);
  zcut = (split*split)/(split*split + mass*mass);
  //std::pow(dmin12,2)/(std::pow(dmin12,2)+std::pow(jetmass,2));
  return zcut;
}

double EventRecoAnalysis::kT_Zcut(const ClusterSequence &cs, double mass, int degree){
  double zcut=-1.;

  double split = kT_Split(cs, degree);
  zcut = (split*split)/(split*split + mass*mass);
  //std::pow(dmin12,2)/(std::pow(dmin12,2)+std::pow(jetmass,2));
  return zcut;
}

double EventRecoAnalysis::kT_Zcut(double split, double mass){

  double zcut=-1.;
  zcut = (split*split)/(split*split + mass*mass);

  return zcut;
}



double EventRecoAnalysis::Tau(const PseudoJet &jet, int degree, double jet_rad){
  double tau=-1.; double alpha=1;

  if (!jet.has_constituents()){
     cout<<"ERROR! Tau can only be applied on jets for which the constituents are known."<<endl;
    return tau;
  }

  vector<PseudoJet> constits = jet.constituents();
  if(constits.size()<degree){
    // cout<<"ERROR! Tau"<<degree<<" can only be applied on jets for which there are at least "<<degree<<" constituents."<<endl;
    return tau;
  }
  tau = Tau(constits, degree, jet_rad);

  return tau;
}

double EventRecoAnalysis::Tau(vector<PseudoJet> constits, int degree, double jet_rad){
  double tau=-1.;

  if(constits.size()<degree){
    // cout<<"ERROR! Tau"<<degree<<" can only be applied on jets for which there are at least "<<degree<<" constituents."<<endl;
    return tau;
  }

  JetDefinition jet_def = JetDefinition(kt_algorithm,M_PI/2,E_scheme,Best);
  ClusterSequence kt_clust_seq(constits, jet_def);

  tau = Tau(constits, kt_clust_seq, degree, jet_rad);

  return tau;
}


double EventRecoAnalysis::Tau(vector<PseudoJet> constits, const ClusterSequence &cs, int degree, double jet_rad){
  double tau=-1.; double alpha=1;

  if(constits.size()<degree){
    // cout<<"ERROR! Tau"<<degree<<" can only be applied on jets for which there are at least "<<degree<<" constituents."<<endl;
    return tau;
  }

  double tauNum = 0.0;
  double tauDen = 0.0;

  vector<PseudoJet> ktaxes = cs.exclusive_jets(degree);
  for (int i = 0; i < constits.size(); i++) {
    // find minimum distance                                                                                               
    double minR = 10000.0; // large number                                                                                 
    for (int j = 0; j < ktaxes.size(); j++) {
      double tempR = sqrt(constits[i].squared_distance(ktaxes[j])); // delta R distance                                   
      if (tempR < minR) minR = tempR;
    }//axes
    tauNum += constits[i].perp() * pow(minR,alpha);
    tauDen += constits[i].perp() * pow(jet_rad,alpha);
  }//constituents

  tau = tauNum/tauDen;

  return tau;
}




int EventRecoAnalysis::JetSelection(){
  int nselec=0;

  vector<int> *truth_flav=new vector<int>;
  truth_flav->clear();
  JetTruthFlavor(jet_pt,jet_eta,jet_phi,jet_E,truth_flav,0.3);
   for(int ii=0; ii<jet_n; ii++){
    if(jet_pt->at(ii)<m_jet_ptmin || fabs(jet_eta->at(ii))>m_jet_etamax){continue;}
    if(find(m_jet_e_overlap_ind.begin(), m_jet_e_overlap_ind.end(), ii) != m_jet_e_overlap_ind.end()){continue;}

    if(m_jetcol_ID.find("Truth") == string::npos){
      if(jet_pt->at(ii) < 50.*GeV && jet_jvf->at(ii) < 0.5){continue;}
    }
    m_jet_ind.push_back(ii);

    if(m_IO & WTREE){
      
      t_jets_pt->push_back(jet_pt->at(ii));
      t_jets_eta->push_back(jet_eta->at(ii));
      t_jets_phi->push_back(jet_phi->at(ii));
      t_jets_trueflavor->push_back(truth_flav->at(ii));
      if(m_jetcol_ID.find("Truth") == string::npos){
	t_jets_MV1->push_back(jet_MV1->at(ii));
	int jetntrack =(int)(jet_nTrk_pv0_1GeV->at(ii));
	t_jets_ntrack->push_back(jetntrack);
	t_jets_trackwidth->push_back(jet_trackWIDTH_pv0_1GeV->at(ii));
      }
      TLorentzVector tlv_j;
      tlv_j.SetPtEtaPhiE(jet_pt->at(ii), jet_eta->at(ii), jet_phi->at(ii), jet_E->at(ii));
      t_jets_m->push_back(tlv_j.M());
      t_jets_charge->push_back(calculateJetCharge(ii));
      
    }
    nselec++;
  }
  
  return nselec;
}

int EventRecoAnalysis::FatJetSelection(int sfj_key){
  // cout<<"Selecting jet collection "<<m_fatjetcol_IDs[sfj_key]<<" IN "<<fatjet_n[sfj_key]<<endl;
  int nselec=0;
  vector<int> v_fj; 
  v_fj.clear(); 
  m_fatjet_ind[sfj_key] = v_fj;

  t_fatjets_n[sfj_key] = 0;
  t_fatjets_pt[sfj_key]= new vector<float>;
  t_fatjets_eta[sfj_key]= new vector<float>;
  t_fatjets_phi[sfj_key]= new vector<float>;
  t_fatjets_m[sfj_key]= new vector<float>;
  t_fatjets_constscale_pt[sfj_key]= new vector<float>;
  t_fatjets_constscale_eta[sfj_key]= new vector<float>;
  t_fatjets_constscale_phi[sfj_key]= new vector<float>;
  t_fatjets_constscale_m[sfj_key]= new vector<float>;
  t_fatjets_ActiveArea[sfj_key]= new vector<float>;
  t_fatjets_ActiveArea_px[sfj_key]= new vector<float>;
  t_fatjets_ActiveArea_py[sfj_key]= new vector<float>;
  t_fatjets_ActiveArea_pz[sfj_key]= new vector<float>;
  t_fatjets_ActiveArea_E[sfj_key]= new vector<float>;
  
  t_fatjets_SPLIT12[sfj_key]= new vector<float>;
  t_fatjets_SPLIT23[sfj_key]= new vector<float>;
  t_fatjets_ZCUT12[sfj_key]= new vector<float>;
  t_fatjets_ZCUT23[sfj_key]= new vector<float>;
  t_fatjets_WIDTH[sfj_key]= new vector<float>;
  t_fatjets_PlanarFlow[sfj_key]= new vector<float>;
  t_fatjets_Angularity[sfj_key]= new vector<float>;
  t_fatjets_PullMag[sfj_key]= new vector<float>;
  t_fatjets_PullPhi[sfj_key]= new vector<float>;
  t_fatjets_Tau1[sfj_key]= new vector<float>;
  t_fatjets_Tau2[sfj_key]= new vector<float>;
  t_fatjets_Tau3[sfj_key]= new vector<float>;
  t_fatjets_nbtag[sfj_key]= new vector<int>;
  t_fatjets_nbtag_geom[sfj_key]= new vector<int>;
  t_fatjets_orig_ind[sfj_key]= new vector<int>;
  t_fatjets_assoc_ind[sfj_key]= new vector<int>;
  t_fatjets_MCclass[sfj_key]= new vector<int>;
  t_fatjets_truth_label[sfj_key]= new vector<int>;
  
  // cout<<"fatjet_pt = "<<fatjet_pt[sfj_key]<<" t_fatjets_pt = "<<t_fatjets_pt[sfj_key]<<endl;
  // cout<<" fatjet_pt size = "<<fatjet_pt[sfj_key]->size()<<endl;
  // cout<<" t_fatjets_pt size = "<<t_fatjets_pt[sfj_key]->size()<<endl;
  string sfj = m_fatjetcol_IDs[sfj_key];

  // cout<<"sfj = "<<sfj<<endl;
  // cout<<"fatjet_n[sfj_key] = "<<fatjet_n[sfj_key]<<" fatjet_orig_ind[sfj_key].size() = "<<fatjet_orig_ind[sfj_key]->size()<<endl;
  for(int ii=0; ii<fatjet_n[sfj_key]; ii++){
    /*
    if(sfj.find("Rebuild")!=string::npos && sfj.find("Topo")!=string::npos){
      if(fatjet_constscale_pt[sfj_key]->at(ii)<m_fatjet_ptmin || fabs(fatjet_constscale_eta[sfj_key]->at(ii))>m_fatjet_etamax){continue;}
    }
    else{
      if(fatjet_pt[sfj_key]->at(ii)<m_fatjet_ptmin || fabs(fatjet_eta[sfj_key]->at(ii))>m_fatjet_etamax){continue;}
    }
    */
/*
     cout<<"ii = "<<ii<<" const_pt = "<<fatjet_constscale_pt[sfj_key]->at(ii)
	<<" const_eta = "<<fatjet_constscale_eta[sfj_key]->at(ii)
	<<" pt = "<<fatjet_pt[sfj_key]->at(ii)
	<<" eta = "<<fatjet_eta[sfj_key]->at(ii)
	<<endl;
*/ 
    if(sfj.find("AntiKt2")!=string::npos){
      if(find(m_ankt2_e_overlap_ind.begin(), m_ankt2_e_overlap_ind.end(), ii) != m_ankt2_e_overlap_ind.end()){continue;}
      if(fatjet_pt[sfj_key]->at(ii)<15.*1000 || fabs(fatjet_eta[sfj_key]->at(ii))>2.5){continue;}
    }
    else if(sfj.find("AntiKt4")!=string::npos){
      if(find(m_ankt4_e_overlap_ind.begin(), m_ankt4_e_overlap_ind.end(), ii) != m_ankt4_e_overlap_ind.end()){continue;}
      if(fatjet_pt[sfj_key]->at(ii)<25.*1000 || fabs(fatjet_eta[sfj_key]->at(ii))>m_fatjet_etamax){continue;}
    }
    else if(sfj.find("Kt6")!=string::npos){
      if(fatjet_pt[sfj_key]->at(ii)<60.*1000 || fabs(fatjet_eta[sfj_key]->at(ii))>m_fatjet_etamax){continue;}
    }
    else if(sfj.find("Kt10")!=string::npos){
      if(fatjet_pt[sfj_key]->at(ii)<40.*1000 || fabs(fatjet_eta[sfj_key]->at(ii))>m_fatjet_etamax){continue;}
    }
    else{
      if(sfj.find("Topo")!=string::npos){
	if(fatjet_constscale_pt[sfj_key]->at(ii)<m_fatjet_ptmin_loose || fabs(fatjet_constscale_eta[sfj_key]->at(ii))>m_fatjet_etamax){continue;}
      }
      else{
	if(fatjet_pt[sfj_key]->at(ii)<m_fatjet_ptmin_loose || fabs(fatjet_eta[sfj_key]->at(ii))>m_fatjet_etamax){continue;}
      }
    }

    m_fatjet_ind[sfj_key].push_back(ii);

    t_fatjets_pt[sfj_key]->push_back(fatjet_pt[sfj_key]->at(ii));
    t_fatjets_eta[sfj_key]->push_back(fatjet_eta[sfj_key]->at(ii));
    t_fatjets_phi[sfj_key]->push_back(fatjet_phi[sfj_key]->at(ii));
    t_fatjets_m[sfj_key]->push_back(fatjet_m[sfj_key]->at(ii));

    


/*
    TLorentzVector tlv_j_corr;
    tlv_j_corr.SetPtEtaPhiM(fatjet_constscale_pt[sfj_key]->at(ii), fatjet_constscale_eta[sfj_key]->at(ii), fatjet_constscale_phi[sfj_key]->at(ii), fatjet_constscale_m[sfj_key]->at(ii));
    TLorentzVector tlv_a_corr;
    tlv_a_corr.SetPxPyPzE(fatjet_ActiveArea_px[sfj_key]->at(ii), fatjet_ActiveArea_py[sfj_key]->at(ii), fatjet_ActiveArea_pz[sfj_key]->at(ii), fatjet_ActiveArea_E[sfj_key]->at(ii));
    tlv_j_corr -= t_Eventshape_rhoKt6LC * tlv_a_corr;
    
    t_fatjets_pt[sfj_key]->push_back(tlv_j_corr.Pt());
    t_fatjets_eta[sfj_key]->push_back(tlv_j_corr.Eta());
    t_fatjets_phi[sfj_key]->push_back(tlv_j_corr.Phi());
    t_fatjets_m[sfj_key]->push_back(tlv_j_corr.M());
*/   

    t_fatjets_constscale_pt[sfj_key]->push_back(fatjet_constscale_pt[sfj_key]->at(ii));
    t_fatjets_constscale_eta[sfj_key]->push_back(fatjet_constscale_eta[sfj_key]->at(ii));
    t_fatjets_constscale_phi[sfj_key]->push_back(fatjet_constscale_phi[sfj_key]->at(ii));
    t_fatjets_constscale_m[sfj_key]->push_back(fatjet_constscale_m[sfj_key]->at(ii));
    t_fatjets_ActiveArea[sfj_key]->push_back(fatjet_ActiveArea[sfj_key]->at(ii));
    t_fatjets_ActiveArea_px[sfj_key]->push_back(fatjet_ActiveArea_px[sfj_key]->at(ii));
    t_fatjets_ActiveArea_py[sfj_key]->push_back(fatjet_ActiveArea_py[sfj_key]->at(ii));
    t_fatjets_ActiveArea_pz[sfj_key]->push_back(fatjet_ActiveArea_pz[sfj_key]->at(ii));
    t_fatjets_ActiveArea_E[sfj_key]->push_back(fatjet_ActiveArea_E[sfj_key]->at(ii));
    
    if(fatjet_SPLIT12[sfj_key] != NULL){
      t_fatjets_SPLIT12[sfj_key]->push_back(fatjet_SPLIT12[sfj_key]->at(ii));
      t_fatjets_SPLIT23[sfj_key]->push_back(fatjet_SPLIT23[sfj_key]->at(ii));
      t_fatjets_ZCUT12[sfj_key]->push_back(fatjet_ZCUT12[sfj_key]->at(ii));
      t_fatjets_ZCUT23[sfj_key]->push_back(fatjet_ZCUT23[sfj_key]->at(ii));
      t_fatjets_WIDTH[sfj_key]->push_back(fatjet_WIDTH[sfj_key]->at(ii));
      t_fatjets_PlanarFlow[sfj_key]->push_back(fatjet_PlanarFlow[sfj_key]->at(ii));
      //t_fatjets_Angularity[sfj_key]->push_back(fatjet_Angularity[sfj_key]->at(ii));
      //t_fatjets_PullMag[sfj_key]->push_back(fatjet_PullMag[sfj_key]->at(ii));
      //t_fatjets_PullPhi[sfj_key]->push_back(fatjet_PullPhi[sfj_key]->at(ii));
      t_fatjets_Tau1[sfj_key]->push_back(fatjet_Tau1[sfj_key]->at(ii));
      t_fatjets_Tau2[sfj_key]->push_back(fatjet_Tau2[sfj_key]->at(ii));
      t_fatjets_Tau3[sfj_key]->push_back(fatjet_Tau3[sfj_key]->at(ii));
    }
    if(sfj.find("Rebuild")!=string::npos){
       t_fatjets_orig_ind[sfj_key]->push_back(fatjet_orig_ind[sfj_key]->at(ii));
       t_fatjets_assoc_ind[sfj_key]->push_back(fatjet_assoc_ind[sfj_key]->at(ii));
       t_fatjets_nbtag[sfj_key]->push_back(-1);
       t_fatjets_nbtag_geom[sfj_key]->push_back(-1);
       //t_fatjets_nbtag[sfj_key]->push_back(fatjet_nbtag[sfj_key]->at(ii));
       //t_fatjets_nbtag_geom[sfj_key]->push_back(fatjet_nbtag_geom[sfj_key]->at(ii));
       t_fatjets_MCclass[sfj_key]->push_back(fatjet_MCclass[sfj_key]->at(ii));
       /*
       cout<<"sfj_key = "<<sfj_key<<" sfj = "<<sfj<<" ii = "<<ii<<" orig = "<<fatjet_orig_ind[sfj_key]->at(ii)
	   <<" assoc = "<<fatjet_assoc_ind[sfj_key]->at(ii)
	   <<" pt = "<<fatjet_pt[sfj_key]->at(ii)<<" m = "<<fatjet_m[sfj_key]->at(ii)
	   <<" eta = "<<fatjet_eta[sfj_key]->at(ii)<<" phi = "<<fatjet_phi[sfj_key]->at(ii)<<endl;
       */
     }
    else{
      t_fatjets_orig_ind[sfj_key]->push_back(-1);
      t_fatjets_assoc_ind[sfj_key]->push_back(-1);
      t_fatjets_nbtag[sfj_key]->push_back(-1);
      t_fatjets_nbtag_geom[sfj_key]->push_back(-1);
      t_fatjets_MCclass[sfj_key]->push_back(-1);
     }

    nselec++;
  }
  JetTruthFlavorM(t_fatjets_pt[sfj_key],t_fatjets_eta[sfj_key],t_fatjets_phi[sfj_key],t_fatjets_m[sfj_key],t_fatjets_truth_label[sfj_key],0.15);
  // cout<<"sfj_key = "<<sfj_key<<" nselec = "<<nselec<<endl;
  t_fatjets_n[sfj_key] = nselec; sfj.clear();
  return nselec;
}

///////////////////////////////////////////////////////////
// RECONSTRUCT EVENT
int ReconstructEvent(int i_opt){

  //vector<PseudoJet> v_input_hadW; v_input_W.clear();
  //vector<PseudoJet> v_input_hadtop; v_input_hadtop.clear();
  //vector<PseudoJet> v_input_Higgs; v_input_higgs.clear();


  return 0;


}

///////////////////////////////////////////////////////////


bool EventRecoAnalysis::IsLepMatchedToGen(int i_gen){
  if(i_gen<0){return -1;}
  bool i_clos = false;
  TLorentzVector* tlv_p = new TLorentzVector();
  tlv_p->SetPtEtaPhiM(mc_pt->at(i_gen), mc_eta->at(i_gen), mc_phi->at(i_gen), mc_m->at(i_gen));
  i_clos = IsLepMatchedToGen(tlv_p);

  delete tlv_p;
  return i_clos;
}

int EventRecoAnalysis::FindMatchedJetToGen(int i_gen){
  if(i_gen<0){return -1;}
  int i_clos = -1;
  TLorentzVector* tlv_p = new TLorentzVector();
  tlv_p->SetPtEtaPhiM(mc_pt->at(i_gen), mc_eta->at(i_gen), mc_phi->at(i_gen), mc_m->at(i_gen));
  i_clos = FindMatchedJetToGen(tlv_p);

  delete tlv_p;
  return i_clos;
}

int EventRecoAnalysis::FindMatchedFatJetToGen(int i_gen, int fj_ID){
  int i_clos = -1;
  TLorentzVector* tlv_p = new TLorentzVector();
  tlv_p->SetPtEtaPhiM(mc_pt->at(i_gen), mc_eta->at(i_gen), mc_phi->at(i_gen), mc_m->at(i_gen));

  i_clos = FindMatchedFatJetToGen(tlv_p, fj_ID);

  delete tlv_p;
  return i_clos;
}

void EventRecoAnalysis::MCParticlesDump(){
   cout<<"mc_n="<<mc_n<<" BARCODE_size = "<<mc_barcode->size()
      <<" STATUS_size = "<<mc_status->size()
      <<" PDGID_size = "<<mc_pdgId->size()
      <<" PARENT_INDEX_size = "<<mc_parent_index->size()
      <<" CHILD_INDEX_size = "<<mc_child_index->size()
      <<endl;

   cout<<"INDEX BARCODE STATUS ID   PT ETA PHI  PARENT1 PARENT2 CHILD1 CHILD2"<<endl;
  bool has_higgs62 = false;
  int n_higgs62 = 0;
  for(int i=0; i<mc_n; i++){
    //if(abs(mc_pdgId->at(i))!=25 && abs(mc_pdgId->at(i))!=5 ){continue;}
    //if( (mc_status->at(i)!=1) && (mc_status->at(i)<21 || mc_status->at(i)>29) ){continue;}
    //if(mc_status->at(i)>=40 || mc_pdgId->at(i)==22 || mc_pdgId->at(i)==21){continue;}
    //if(mc_status->at(i)<21 || mc_status->at(i)>29){continue;}
    //if(!((mc_status->at(i)==1) || (mc_status->at(i)==3))){continue;}
     cout<<"INDEX = "<<i<<" BARCODE = "<<mc_barcode->at(i)<<" STATUS = "<<mc_status->at(i)<<" PDGID = "<<mc_pdgId->at(i)
	<<" PT = "<<mc_pt->at(i)/GeV<<" ETA = "<<mc_eta->at(i)<<" PHI = "<<mc_phi->at(i)<<" PARENT_SIZE = "<<mc_parent_index->at(i).size()<<" CHILD_SIZE = "<<mc_child_index->at(i).size();
    for(int alpha = 0; alpha < mc_parent_index->at(i).size(); alpha++){
       cout<<" PARENT "<<alpha<<" = "<<mc_parent_index->at(i).at(alpha)<<endl;
    }
    for(int alpha = 0; alpha < mc_child_index->at(i).size(); alpha++){
       cout<<" CHILD "<<alpha<<" = "<<mc_child_index->at(i).at(alpha)<<endl;
    }
     cout<<endl;
    if(mc_status->at(i)==62 && abs(mc_pdgId->at(i)) == 25){has_higgs62 = true; n_higgs62++;}
  }
   cout<<" HAS_ HIGGS _ 62 == "<<has_higgs62<<" N_ HIGGS_62 =  "<<n_higgs62<<endl<<endl<<endl;
  return;

}

bool EventRecoAnalysis::IsLepMatchedToGen(TLorentzVector* tlv_p){
  bool i_clos=false;
  double DR = m_drmatch_l;
  TLorentzVector* tlv_l = new TLorentzVector();
  tlv_l->SetPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_E);  
  double dr_cur = tlv_l->DeltaR(*tlv_p);
  if(dr_cur<DR){i_clos = true;}
  delete tlv_l;
  return i_clos;
}

int EventRecoAnalysis::FindMatchedJetToGen(TLorentzVector* tlv_p){
  double DR = m_drmatch_j;

  //Match by finding the closest jet to the parton within a cone of size DR
  //So loop over the jet container and look for minimum dR
  double drmin=0.3; int i_clos=-1;
  TLorentzVector* tlv_j = new TLorentzVector();
  for(int ii=0; ii<m_jet_ind.size(); ii++){
    int i=m_jet_ind.at(ii);
    double dr_cur=0.;
    tlv_j->SetPtEtaPhiE(jet_pt->at(i), jet_eta->at(i), jet_phi->at(i), jet_E->at(i));  
    dr_cur = tlv_j->DeltaR(*tlv_p);
    // cout<<"selec index = "<<ii<<" D3PD index = "<<i<<" DeltaR = "<<dr_cur
    //<<" drmin = "<<drmin<<" i_clos = "<<i_clos<<" DR = "<<DR
    //	<<" Not minimum = "<<(dr_cur > drmin)<<" Out_of_range = "<<(dr_cur > DR)<<" Do_continue = "<<( (dr_cur > drmin) || (dr_cur > DR) )<<endl;
    if((dr_cur>DR) || (dr_cur>drmin)){continue;}
    drmin = dr_cur; i_clos=ii;
  }
  delete tlv_j;
  // cout<<"drmin = "<<drmin<<" i_clos = "<<i_clos<<" parton_pT = "<<mc_pt->at(i_gen)/GeV<<" jet pT = "<<jet_pt->at(i_clos)/GeV<<endl;
  return i_clos;
}

int EventRecoAnalysis::FindMatchedFatJetToGen(TLorentzVector* tlv_p, int fj_ID){
  double DR = m_drmatch_fj;
  // cout<<"i_gen = "<<i_gen<<"fj_ID ="<<fj_ID<<endl;
  //Match by finding the closest jet to the parton within a cone of size DR
  //So loop over the jet container and look for minimum dR
  double drmin=99.; int i_clos=-1;
  TLorentzVector* tlv_j = new TLorentzVector();

  for(int ii=0; ii<m_fatjet_ind[fj_ID].size(); ii++){
    // cout<<"ii = "<<ii<<endl;
    int i=m_fatjet_ind[fj_ID].at(ii);
    // cout<<"ii = "<<ii<<" i = "<<i<<endl;

    double dr_cur=0.;
    tlv_j->SetPtEtaPhiM(fatjet_pt[fj_ID]->at(i), fatjet_eta[fj_ID]->at(i), fatjet_phi[fj_ID]->at(i), fatjet_m[fj_ID]->at(i));  
    dr_cur = tlv_j->DeltaR(*tlv_p);
    // cout<<"selec index = "<<ii<<" D3PD index = "<<i<<" DeltaR = "<<dr_cur
    //	<<" drmin = "<<drmin<<" i_clos = "<<i_clos<<" DR = "<<DR
    //	<<" Not minimum = "<<(dr_cur > drmin)<<" Out_of_range = "<<(dr_cur > DR)<<" Do_continue = "<<( (dr_cur > drmin) || (dr_cur > DR) )<<endl;
    if((dr_cur>DR) || (dr_cur>drmin)){continue;}
    drmin = dr_cur; i_clos=ii;
  }
  // cout<<"drmin = "<<drmin<<" i_clos = "<<i_clos<<" parton_pT = "<<mc_pt->at(i_gen)/GeV<<" fatjet pT = "<<fatjet_pt[fj_ID]->at(i_clos)/GeV<<endl;
  delete tlv_j;
  return i_clos;
}

bool EventRecoAnalysis::m_is_indx_leptop(int indx){
  int t_indx=indx;
  bool is_top = m_is_indx_top(indx);
  if(!is_top){return false;}
  vector<int> childList = mc_child_index->at(t_indx);
  if(childList.size()>2){ cout<<"WARNING: TOO MANY TOP CHILDREN "<<childList.size()<<endl;}
  bool is_leptop = false;
  int b_indx=-1; int W_indx=-1;

  for(vector<int>::iterator c_it = childList.begin(); c_it!=childList.end(); ++c_it){
    if(m_is_indx_top(*c_it)){m_is_indx_leptop(*c_it);}
    else if(m_is_indx_b(*c_it)){
      if(b_indx<0){b_indx=*c_it;}
      else{ cout<<"TOO MANY B IN TOP DECAY"<<endl;}
    }
    else if(m_is_indx_W(*c_it)){
      if(W_indx<0){is_leptop = m_is_indx_lepW(*c_it);}
      else{ cout<<"TOO MANY W IN TOP DECAY"<<endl;}
    }
    else{continue;}
  } 

  if(is_leptop){
    m_ip_leptop = t_indx;
    m_ip_leptop_b = b_indx;
    m_ip_leptop_W = W_indx;
  }
  return is_leptop;
}

bool EventRecoAnalysis::m_is_indx_hadtop(int indx){
  int t_indx=indx;
  bool is_top = m_is_indx_top(indx);
  if(!is_top){return false;}
  vector<int> childList = mc_child_index->at(t_indx);
  if(childList.size()>2){ cout<<"WARNING: TOO MANY TOP CHILDREN "<<childList.size()<<endl;}
  bool is_hadtop = false;
  int b_indx=-1; int W_indx=-1;

  for(vector<int>::iterator c_it = childList.begin(); c_it!=childList.end(); ++c_it){
    if(m_is_indx_top(*c_it)){m_is_indx_hadtop(*c_it);}
    else if(m_is_indx_b(*c_it)){
      if(b_indx<0){b_indx=*c_it;}
      else{ cout<<"TOO MANY B IN TOP DECAY"<<endl;}
    }
    else if(m_is_indx_W(*c_it)){
      if(W_indx<0){is_hadtop = m_is_indx_hadW(*c_it);}
      else{ cout<<"TOO MANY W IN TOP DECAY"<<endl;}
    }
    else{continue;}
  } 

  if(is_hadtop){
    m_ip_hadtop = t_indx;
    m_ip_hadtop_b = b_indx;
    m_ip_hadtop_W = W_indx;
  }
  return is_hadtop;
}


bool EventRecoAnalysis::m_is_indx_lepW(int indx){
  int W_indx=indx;
  bool is_W = m_is_indx_W(indx);
  if(!is_W){return false;}
  vector<int> childList = mc_child_index->at(W_indx);
  if(childList.size()>2){ cout<<"WARNING: TOO MANY W CHILDREN "<<childList.size()<<endl;}
  bool is_lepW = false;
  int l_indx=-1; int n_indx=-1;

  for(vector<int>::iterator c_it = childList.begin(); c_it!=childList.end(); ++c_it){
    if(m_is_indx_W(*c_it)){m_is_indx_lepW(*c_it);}
    else if(m_is_indx_lep(*c_it)){
      is_lepW = true;
      if(l_indx<0){l_indx=*c_it;}
      else{ cout<<"TOO MANY LEP IN W DECAY"<<endl;}
    }
    else if(m_is_indx_nu(*c_it)){
      if(n_indx<0){n_indx=*c_it;}
      else{ cout<<"TOO MANY NU IN TOP DECAY"<<endl;}
    }
    else{continue;}
  } 

  if(is_lepW){
    m_ip_leptop_lep = l_indx;
    m_ip_leptop_nu = n_indx;
  }
  return is_lepW;
}

bool EventRecoAnalysis::m_is_indx_hadW(int indx){
  int W_indx=indx;
  bool is_W = m_is_indx_W(indx);
  if(!is_W){return false;}
  vector<int> childList = mc_child_index->at(W_indx);
  if(childList.size()>2){ cout<<"WARNING: TOO MANY W CHILDREN "<<childList.size()<<endl;}
  bool is_hadW = false;
  int q1_indx=-1; int q2_indx=-1;

  for(vector<int>::iterator c_it = childList.begin(); c_it!=childList.end(); ++c_it){
    if(m_is_indx_W(*c_it)){m_is_indx_hadW(*c_it);}
    else if(m_is_indx_q(*c_it)){
      is_hadW = true;
      if(q1_indx<0){q1_indx=*c_it;}
      else if(q2_indx<0){q2_indx=*c_it;}
      else{ cout<<"TOO MANY HAD IN W DECAY"<<endl;}
    }
    else{continue;}
  } 

  if(is_hadW){
    m_ip_hadtop_q1 = q1_indx;
    m_ip_hadtop_q2 = q2_indx;
  }
  return is_hadW;
}

bool EventRecoAnalysis::m_is_indx_Hbb(int indx){
  int h_indx=indx;
  bool is_higgs = m_is_indx_higgs(indx);
  if(!is_higgs){return false;}

  vector<int> childList = mc_child_index->at(h_indx);
  if(childList.size()>2){ cout<<"WARNING: TOO MANY Higgs CHILDREN "<<childList.size()<<endl;}
  bool is_Hbb = false;
  int b1_indx=-1; int b2_indx=-1;

  for(vector<int>::iterator c_it = childList.begin(); c_it!=childList.end(); ++c_it){
    if(m_is_indx_higgs(*c_it)){m_is_indx_Hbb(*c_it);}
    else if(m_is_indx_b(*c_it)){
      is_Hbb = true;
      if(b1_indx<0){b1_indx=*c_it;}
      else if(b2_indx<0){b2_indx=*c_it;}
      else{ cout<<"TOO MANY B IN HIGGS DECAY"<<endl;}
    }
    else{continue;}
  } 
  
  if(is_Hbb){
    m_ip_higgs = h_indx;
    m_ip_higgs_b1 = b1_indx;
    m_ip_higgs_b2 = b2_indx;
  }
  return is_Hbb;
  
}

void EventRecoAnalysis::MakeMCBarcodeMap(){
  for(int i=0; i<mc_n; i++){
    m_mc_bc_ind[mc_barcode->at(i)]=i;
  }
  return;
}

bool EventRecoAnalysis::m_is_stat_hard_top(int ID, int i_gen){
  bool b_ret = false;
  b_ret = (i_gen == I_PYTHIA8) && (ID == 22);
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_hard_higgs(int ID, int i_gen){
  bool b_ret = false;
  b_ret = (i_gen == I_PYTHIA8) && (ID == 22);
  return b_ret; 
}

bool EventRecoAnalysis::m_is_stat_top(int ID, int i_gen){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 62);}
  else if(i_gen == I_PYTHIA6){b_ret = (ID == 3);}
  else if(i_gen == I_MADPYTH){b_ret = (ID == 3);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_higgs(int ID, int i_gen){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 62);}
  else if(i_gen == I_PYTHIA6){b_ret = (ID == 3);}
  else if(i_gen == I_MADPYTH){b_ret = (ID == 2);}// ttA new EFT
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_W(int ID, int i_gen){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 22);}
  else if(i_gen == I_PYTHIA6){b_ret = (ID == 3);}
  else if(i_gen == I_MADPYTH){b_ret = (ID == 3);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_lep(int ID, int i_gen){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8 || i_gen == I_PYTHIA6){b_ret = ( ((ID >= 1)&&(ID <= 3)) || (ID == 10902) );}
  else if(i_gen == I_MADPYTH){b_ret = (ID == 3);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_nu(int ID, int i_gen){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8 || i_gen == I_PYTHIA6){b_ret = ( ((ID >= 1)&&(ID <= 3)) || (ID == 10902) );}
  else if(i_gen == I_MADPYTH){b_ret = (ID == 3);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_lq(int ID, int i_gen){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 23);}
  else if(i_gen == I_PYTHIA6){b_ret = (ID == 3);}
  else if(i_gen == I_MADPYTH){b_ret = (ID == 3);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_b(int ID, int i_gen){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 23);}
  else if(i_gen == I_PYTHIA6){b_ret = (ID == 3);}
  else if(i_gen == I_MADPYTH){b_ret = ( (ID == 2) || (ID == 3));}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}

/*
bool EventRecoAnalysis::m_is_stat_top(int ID, int i_gen, bool b_rad){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 62);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_higgs(int ID, int i_gen, bool b_rad){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 62);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_W(int ID, int i_gen, bool b_rad){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 52);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_lep(int ID, int i_gen, bool b_rad){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = ( ((ID >= 1)&&(ID <= 3)) || (ID == 10902) );}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_nu(int ID, int i_gen, bool b_rad){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = ( ((ID >= 1)&&(ID <= 3)) || (ID == 10902) );}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_lq(int ID, int i_gen, bool b_rad){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 23);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}
bool EventRecoAnalysis::m_is_stat_b(int ID, int i_gen, bool b_rad){
  bool b_ret = false;
  if(i_gen == I_PYTHIA8){b_ret = (ID == 23);}
  else if(i_gen == I_HERWIG){b_ret = ( (ID == 123) || (ID == 124) );}
  return b_ret; 
}

 */


bool EventRecoAnalysis::m_is_indx_stat_hard_top(int indx, int i_gen){
  return m_is_stat_hard_top(mc_status->at(indx), i_gen);
}
bool EventRecoAnalysis::m_is_indx_stat_hard_higgs(int indx, int i_gen){
  return m_is_stat_hard_higgs(mc_status->at(indx), i_gen);
}


bool EventRecoAnalysis::m_is_indx_stat_top(int indx, int i_gen){
  return m_is_stat_top(mc_status->at(indx), i_gen);
}
bool EventRecoAnalysis::m_is_indx_stat_higgs(int indx, int i_gen){
  return m_is_stat_higgs(mc_status->at(indx), i_gen);
}
bool EventRecoAnalysis::m_is_indx_stat_W(int indx, int i_gen){
  return m_is_stat_W(mc_status->at(indx), i_gen);
}
bool EventRecoAnalysis::m_is_indx_stat_lep(int indx, int i_gen){
  return m_is_stat_lep(mc_status->at(indx), i_gen);
}
bool EventRecoAnalysis::m_is_indx_stat_nu(int indx, int i_gen){
  return m_is_stat_nu(mc_status->at(indx), i_gen);
}
bool EventRecoAnalysis::m_is_indx_stat_lq(int indx, int i_gen){
  return m_is_stat_lq(mc_status->at(indx), i_gen);
}
bool EventRecoAnalysis::m_is_indx_stat_b(int indx, int i_gen){
  return m_is_stat_b(mc_status->at(indx), i_gen);
}



int EventRecoAnalysis::AssignGenPartIndx(int i_gen){
  if(i_gen < 0){return -1;}

  ///LEPTONS RELATED STUFF////

  el_pt->clear();
  el_eta->clear();
  el_phi->clear();
  el_E->clear();
  el_m->clear();
  el_charge->clear();

  mu_pt->clear();
  mu_eta->clear();
  mu_phi->clear();
  mu_E->clear();
  mu_m->clear();
  mu_charge->clear();

  ///////////////////////////
  for(int i=0; i<mc_n; i++){
    // cout<<"pdg id = "<<mc_pdgId->at(i)<<" status = "<<mc_status->at(i)<<endl;
    //if( (mc_status->at(i)<1 || mc_status->at(i)>3) && (mc_status->at(i)!=10902) && (mc_status->at(i)<21 || mc_status->at(i)>29) ){continue;}
    if(m_is_indx_top(i)){
      if(m_is_indx_stat_top(i, i_gen)){
	if(mc_charge->at(i)>0){m_ip_top = i;}
	else{m_ip_atop = i;}
      }
      else if(m_is_indx_stat_hard_top(i, i_gen)){
	if(mc_charge->at(i)>0){m_ip_top_hard = i;}
	else{m_ip_atop_hard = i;}
      }
      else{continue;}
    }//TOP
    else if(m_is_indx_higgs(i)){
      if(m_is_indx_stat_higgs(i, i_gen)){
	m_ip_higgs = i; 
      }
      else if(m_is_indx_stat_hard_higgs(i, i_gen)){
	m_ip_higgs_hard = i; 
      }
      else{continue;}
    }//HIGGS
    else if(m_is_indx_W(i)){
      if(!m_is_indx_stat_W(i, i_gen)){continue;}
      if(mc_charge->at(i)>0){m_ip_top_W = i;}
      else{m_ip_atop_W = i;}
    }//W-BOSON
    else if(m_is_indx_b(i)){
      if(!m_is_indx_stat_b(i, i_gen)){continue;}
      
      //Determine if from top or Higgs
      //(NOTE: if bkgd process, then extra b's not from W 'assigned' as [fake] Higgs daughters)
      //Check parent
      if(mc_parent_index->at(i).size() > 0){
 	//int p_ind = m_mc_bc_ind[mc_parent_index->at(i).at(0)];
	int p_ind = mc_parent_index->at(i).at(0);
	if(m_is_indx_top(p_ind)){
	  if(mc_charge->at(i)>0){m_ip_atop_b = i;}
	  else{m_ip_top_b = i;}
	}
	else if(m_is_indx_higgs(p_ind)){
 	  if(m_ip_higgs_b1<0){m_ip_higgs_b1 = i;}
	  else if(m_ip_higgs_b2<0){m_ip_higgs_b2 = i;}
	  else{ cout<<"TOO MANY HIGGS DAUGHTERS"<<endl;} //MCParticlesDump();}
	}
	else if(m_is_indx_W(p_ind)){
 	  if(m_ip_hadtop_q1<0){m_ip_hadtop_q1 = i;}
	  else if(m_ip_hadtop_q2<0){m_ip_hadtop_q2 = i;}
	  //else{ cout<<"TOO MANY HADRONIC W DAUGHTERS"<<endl; }
	  
	}
	else if(m_isSignal<2){
 	  if(m_ip_higgs_b1<0){m_ip_higgs_b1 = i;}
	  else if(m_ip_higgs_b2<0){m_ip_higgs_b2 = i;}
	  //else{ cout<<"TOO MANY HIGGS DAUGHTERS"<<endl; MCParticlesDump();}
	}
      }
    }//B-QUARKS
    else if(m_is_indx_lep(i)){
      
      if(m_is_indx_stat_lep(i, i_gen)){
	if(mc_parent_index->at(i).size() > 0){
	  //int p_ind = m_mc_bc_ind[mc_parent_index->at(i).at(0)];
	  int p_ind = mc_parent_index->at(i).at(0);
	  if(m_is_indx_W(p_ind)&& m_ip_leptop_lep < 0){
	    m_ip_leptop_lep = i;
	    if(m_is_indx_el(i)){m_p_leptop_lep_flav = ELE;}
	    else if(m_is_indx_mu(i)){m_p_leptop_lep_flav=MUON;}
	    else if(m_is_indx_tau(i)){m_p_leptop_lep_flav=TAU;} 
	  }//first W-daughter
	}//has parents
      }

    }//LEPTON
    else if(m_is_indx_nu(i)){
      if(!m_is_indx_stat_nu(i, i_gen)){continue;}
      if(mc_parent_index->at(i).size() > 0){
	int p_ind = mc_parent_index->at(i).at(0);
	//int p_ind = m_mc_bc_ind[mc_parent_index->at(i).at(0)];
	if(m_is_indx_W(p_ind) && m_ip_leptop_nu < 0){m_ip_leptop_nu = i;}
      }
    }//NEUTRINO
    else if(m_is_indx_lq(i)){
      if(!m_is_indx_stat_lq(i, i_gen)){continue;}
      if(mc_parent_index->at(i).size() > 0){
	//int p_ind = m_mc_bc_ind[mc_parent_index->at(i).at(0)];
	int p_ind = mc_parent_index->at(i).at(0);
	if(m_is_indx_W(p_ind)){
	  if(m_ip_hadtop_q1<0){m_ip_hadtop_q1 = i;}
	  else if(m_ip_hadtop_q2<0){m_ip_hadtop_q2 = i;}
	  //else{ cout<<"TOO MANY HADRONIC W DAUGHTERS"<<endl; }
	}
      }
    }//LIGHT-QUARKS
    
    
    
    ////FINAL STATE ELE - MUONS/////
    
      
    //determine whether particle has a GEANT vertex
    bool g4vertex = false;
    int nci = (mc_child_index->at(i)).size();
    for (int j=0; j != nci; ++j){
      if ( (mc_child_index->at(i))[j] < 0 || (mc_child_index->at(i))[j] == mc_n || (mc_child_index->at(i))[j] > mc_n ) continue;
      int p =(mc_child_index->at(i))[j];
      if ( mc_barcode->at(p) > 100000 ) g4vertex = true;
    }
    
    //Check decay status of particle
    int status = mc_status->at(i);
    bool is_undecayed = false, is_decayed_pythia = false, is_decayed_geant = false; 
    if(status%1000 == 1) is_undecayed=true; 
    if(status%1000 == 2 && status > 1000) is_decayed_pythia=true;
    if(status==2 && g4vertex) is_decayed_geant = true;

   
    if( !is_undecayed && !is_decayed_pythia && !is_decayed_geant ) continue;



    //determine whether the particle is GEANT   
    int barcode=mc_barcode->at(i);
    bool is_geant_particle = false; 
    if(barcode<200000)is_geant_particle = true; 
    
    if( !is_geant_particle ) continue;
    
    int pdg_id=mc_pdgId->at(i);

       
    float mc_pmag = mc_pt->at(i)*cosh(mc_eta->at(i));
    float mc_mass = mc_m->at(i);
    float mc_E;
    //if(mc_mass>0){
    //  mc_E = sqrt(mc_pmag*mc_pmag + mc_mass*mc_mass);
    //}
    //else{
    //  mc_E = sqrt(mc_pmag*mc_pmag);
    //}
    
    /*
    if( abs(pdg_id)==15 ){
      int ci = (mc_child_index->at(i)).size();
      for (int j=0; j != ci; ++j){
	
	if(abs(mc_pdgId->at(mc_child_index->at(i).at(j)))==11||abs(mc_pdgId->at(mc_child_index->at(i).at(j)))==13){

	  cout<<"Electron or Muon from tau = "<< mc_child_index->at(i).at(j)<<"status == "<<mc_status->at(mc_child_index->at(i).at(j))<<endl;

	}


      }
    }

    */

    mc_E = sqrt(mc_pmag*mc_pmag + mc_mass*mc_mass);

    float pe = mc_E;
    
    float  peta = mc_eta->at(i), pphi = mc_phi->at(i), ppt = mc_pt->at(i);
    
    float cc_px = ppt*cos(pphi), cc_py = ppt*sin(pphi), cc_pz = ppt*sinh(peta); 
    
    float charge=mc_charge->at(i);
    
    
    //electrons
    
    if( abs(pdg_id)==11 ){
      

      el_pt->push_back(ppt);
      el_eta->push_back(peta);
      el_phi->push_back(pphi);
      el_E->push_back(pe);
      el_m->push_back(mc_mass);
      el_charge->push_back(charge);
      
      
    } 
    
    if( abs(pdg_id)==13 ){
      

      mu_pt->push_back(ppt);
      mu_eta->push_back(peta);
      mu_phi->push_back(pphi);
      mu_E->push_back(pe);
      mu_m->push_back(mc_mass);
      mu_charge->push_back(charge);
      
      
    }

    ////////////////////////////////


  }//PARTICLE LOOP


  el_n=el_pt->size();
  mu_n=mu_pt->size();

 //Now assign to leptonic and hadronic legs
  if(m_ip_leptop_lep >= 0 && m_ip_hadtop_q1 >= 0){
    // cout<<"ENTERED LEP-HAD ASSGN"<<endl;
    if(mc_charge->at(m_ip_leptop_lep)>0){
      m_ip_leptop_hard = m_ip_top_hard;
      m_ip_leptop = m_ip_top;
      m_ip_leptop_W = m_ip_top_W;
      m_ip_leptop_b = m_ip_top_b;

      m_ip_hadtop_hard = m_ip_atop_hard;
      m_ip_hadtop = m_ip_atop;
      m_ip_hadtop_W = m_ip_atop_W;
      m_ip_hadtop_b = m_ip_atop_b;
    }
    else{
      m_ip_leptop_hard = m_ip_atop_hard;
      m_ip_leptop = m_ip_atop;
      m_ip_leptop_W = m_ip_atop_W;
      m_ip_leptop_b = m_ip_atop_b;

      m_ip_hadtop_hard = m_ip_top_hard;
      m_ip_hadtop = m_ip_top;
      m_ip_hadtop_W = m_ip_top_W;
      m_ip_hadtop_b = m_ip_top_b;
    }
  }

  /*
  if(m_ip_leptop_lep >=0 && mc_charge->at(m_ip_leptop_lep)>0){
    //Lepton is antiparticle, i.e. lep_W is positive, top is particle
    //top is leptop, atop is hadtop
    m_ip_leptop = m_ip_top;
    m_ip_leptop_W = m_ip_top_W;
    m_ip_leptop_b = m_ip_top_b;

    m_ip_hadtop = m_ip_atop;
    m_ip_hadtop_W = m_ip_atop_W;
    m_ip_hadtop_b = m_ip_atop_b;
  }
  else if(m_ip_leptop_lep >=0 && mc_charge->at(m_ip_leptop_lep)>0){
    m_ip_leptop = m_ip_atop;
    m_ip_leptop_W = m_ip_atop_W;
    m_ip_leptop_b = m_ip_atop_b;

    m_ip_hadtop = m_ip_top;
    m_ip_hadtop_W = m_ip_top_W;
    m_ip_hadtop_b = m_ip_top_b;
  }
  */
  //Check if everything was correctly filled:
  //Now have to check for correct channel.i.e. may have no hadronic W or no b
  int sc = 0;
  /*
  if(m_ip_top < 0){ cout<<"NO TOP"<<endl; sc = -1;}
  if(m_ip_atop < 0){ cout<<"NO ATOP"<<endl; sc = -1;}
  if(m_ip_leptop < 0){ cout<<"NO LEPTOP"<<endl; sc = -1;}
  if(m_ip_hadtop < 0){ cout<<"NO HADTOP"<<endl; sc = -1;}
  if((m_isSignal == 2) && (m_ip_higgs < 0)){ cout<<"NO HIGGS"<<endl; sc = -2;}

  if(m_ip_leptop_b < 0){ cout<<"NO LEPTOP_B"<<endl; sc = -1;}
  if(m_ip_leptop_W < 0){ cout<<"NO LEPTOP_W"<<endl; sc = -1;}
  if(m_ip_leptop_lep < 0){ cout<<"NO LEPTOP_LEP"<<endl; sc = -1;}
  if(m_ip_leptop_nu < 0){ cout<<"NO LEPTOP_NU"<<endl; sc = -1;}

  if(m_ip_hadtop_b < 0){ cout<<"NO HADTOP_B"<<endl; sc = -1;}
  if(m_ip_hadtop_W < 0){ cout<<"NO HADTOP_W"<<endl; sc = -2;}
  if(m_ip_hadtop_q1 < 0){ cout<<"NO HADTOP_Q1"<<endl; sc = -1;}
  if(m_ip_hadtop_q2 < 0){ cout<<"NO HADTOP_Q2"<<endl; sc = -1;}

  if((m_isSignal == 2) && (m_ip_higgs_b1 < 0)){ cout<<"NO HIGGS_B1"<<endl; sc = -2;}
  if((m_isSignal == 2) && (m_ip_higgs_b2 < 0)){ cout<<"NO HIGGS_B2"<<endl; sc = -2;}


  if( (m_isSignal > 0) && ( (m_ip_top < 0) || (m_ip_atop < 0) ) ){ sc = -2; return sc;}
  if( (m_isSignal > 0) && ( (m_ip_leptop < 0) || (m_ip_hadtop < 0) ) ){ sc = -2; return sc;}
  if((m_isSignal == 2) && (m_ip_higgs < 0)){sc = -2; return sc;}
  */
  /*
   cout<<"m_isSignal = "<<m_isSignal<<" m_ip_top = "<<m_ip_top<<" m_ip_atop = "<<m_ip_atop<<" m_ip_leptop = "<<m_ip_leptop<<" m_ip_hadtop = "<<m_ip_hadtop<<endl;
  if(m_ip_leptop_lep >= 0){
     cout<<"lepton charge = "<<mc_charge->at(m_ip_leptop_lep)<<endl;
  }
   cout<<"m_ip_leptop_lep = "<<m_ip_leptop_lep<<" m_ip_leptop_nu = "<<m_ip_leptop_nu<<endl;
   cout<<"m_ip_hadtop_q1 = "<<m_ip_hadtop_q1<<" m_ip_hadtop_q2 = "<<m_ip_hadtop_q2<<endl;
   cout<<"m_ip_higgs_b1 = "<<m_ip_higgs_b1<<" m_ip_higgs_b2 = "<<m_ip_higgs_b2<<endl;
   cout<<"m_ip_leptop_b = "<<m_ip_leptop_b<<" m_ip_leptop_W = "<<m_ip_leptop_W<<endl;
   cout<<"m_ip_hadtop_b = "<<m_ip_hadtop_b<<" m_ip_hadtop_W = "<<m_ip_hadtop_W<<endl;
   cout<<"m_ip_atop_b = "<<m_ip_atop_b<<" m_ip_atop_W = "<<m_ip_atop_W<<endl;
   cout<<"m_ip_top_b = "<<m_ip_top_b<<" m_ip_top_W = "<<m_ip_top_W<<endl;
  */

  if(m_ip_leptop_b < 0){sc = -1;}
  if((m_isSignal > 0) && (m_ip_leptop_W < 0)){sc = -2; return sc;}
  if(m_ip_leptop_lep < 0){sc = -1;}
  if(m_ip_leptop_nu < 0){sc = -1;}

  if(m_ip_hadtop_b < 0){sc = -1;}
  if((m_isSignal > 0) && (m_ip_hadtop_W < 0)){sc = -2; return sc;}
  if(m_ip_hadtop_q1 < 0){sc = -1;}
  if(m_ip_hadtop_q2 < 0){sc = -1;}

  if((m_isSignal == 2) && (m_ip_higgs_b1 < 0)){sc = -2; return sc;}
  if((m_isSignal == 2) && (m_ip_higgs_b2 < 0)){sc = -2; return sc;}

  return sc;

}

int EventRecoAnalysis::MatchPartonJets(){
  
  m_ij_ind_leptop_b = FindMatchedJetToGen(m_ip_leptop_b);
  m_ij_ind_hadtop_b = FindMatchedJetToGen(m_ip_hadtop_b);

  m_ij_ind_hadtop_q1 = FindMatchedJetToGen(m_ip_hadtop_q1);
  m_ij_ind_hadtop_q2 = FindMatchedJetToGen(m_ip_hadtop_q2);
  
  m_ij_ind_higgs_b1 = FindMatchedJetToGen(m_ip_higgs_b1);
  m_ij_ind_higgs_b2 = FindMatchedJetToGen(m_ip_higgs_b2);


  m_ij_leptop_b = (m_ij_ind_leptop_b >= 0) ?  m_jet_ind.at(m_ij_ind_leptop_b) : -1;
  m_ij_hadtop_b = (m_ij_ind_hadtop_b >= 0) ? m_jet_ind.at(m_ij_ind_hadtop_b) : -1;
  m_ij_hadtop_q1 = (m_ij_ind_hadtop_q1 >= 0) ? m_jet_ind.at(m_ij_ind_hadtop_q1) : -1;
  m_ij_hadtop_q2 = (m_ij_ind_hadtop_q2 >= 0) ? m_jet_ind.at(m_ij_ind_hadtop_q2) : -1;
  m_ij_higgs_b1 = (m_ij_ind_higgs_b1 >= 0) ? m_jet_ind.at(m_ij_ind_higgs_b1) : -1;
  m_ij_higgs_b2 = (m_ij_ind_higgs_b2 >= 0) ? m_jet_ind.at(m_ij_ind_higgs_b2) : -1;

  
  //Check if matches were found for everything
  int sc = 0;

  return sc;
  
}
/*
void EventRecoAnalysis::PyDelClusterSequenceTA(void *ptr) {
    ClusterSequenceArea *old_ptr = static_cast<ClusterSequenceArea*> (ptr);
    delete old_ptr;
    return;
}
*/
void EventRecoAnalysis::FillCells(){

  cell_n=-1;
  
  cell_pt->clear();
  cell_eta->clear();
  cell_phi->clear();
  cell_E->clear();


  bool exclude_neutrinos = true;//true;
  bool exclude_muons = true;//true;

  double cells[ETABINS][PHIBINS];
  for (int i = 0; i < ETABINS; i ++)
    for (int j = 0; j < PHIBINS; j ++)
      cells[i][j] = 0;
  double deta = (ETAHIGH - ETALOW)/ETABINS;
  double dphi = 2*PIGREC/PHIBINS;


  
  for(int i=0; i<mc_n; i++){
    //determine whether particle has a GEANT vertex
      bool g4vertex = false;
      int nci = (mc_child_index->at(i)).size();
      for (int j=0; j != nci; ++j){
	if ( (mc_child_index->at(i))[j] < 0 || (mc_child_index->at(i))[j] == mc_n || (mc_child_index->at(i))[j] > mc_n ) continue;
	int p =(mc_child_index->at(i))[j];
	if ( mc_barcode->at(p) > 100000 ) g4vertex = true;
      }
      
      //Check decay status of particle
      int status = mc_status->at(i);
      bool is_undecayed = false, is_decayed_pythia = false, is_decayed_geant = false; 
      if(status%1000 == 1) is_undecayed=true; 
      if(status%1000 == 2 && status > 1000) is_decayed_pythia=true;
      if(status==2 && g4vertex) is_decayed_geant = true;
      
      if( !is_undecayed && !is_decayed_pythia && !is_decayed_geant ) continue;
      //determine whether the particle is GEANT   
      int barcode=mc_barcode->at(i);
      bool is_geant_particle = false; 
      if(barcode<200000)is_geant_particle = true; 
      
      if( !is_geant_particle ) continue;
      
      int pdg_id=mc_pdgId->at(i);
      
      //muons and neutrinos
      bool is_nu = false, is_muon = false;                                                                
      if( abs(pdg_id)==12 || abs(pdg_id)==14 || abs(pdg_id)==16 ) is_nu = true;
      if( is_nu && exclude_neutrinos ) continue;
      
      if( abs(pdg_id)==13 ) is_muon = true;
      if( is_muon && exclude_muons ) continue;
      
      //determine whether the particle is other non-interacting (is_susy covers all sorts of hypothetical particles such as Gravitons, KK excitations)
      bool is_susy = false, is_funny_gluon = false;
      if( (pdg_id==1000022 && status%1000==1 ) ||
	  (pdg_id==5100022 && status%1000==1 ) ||
	  (pdg_id==1000024 && status%1000==1 ) ||
	  (pdg_id==39      && status%1000==1 ) ||
	  (pdg_id==1000039 && status%1000==1 ) ||
	  (pdg_id==5000039 && status%1000==1 ) ) is_susy = true;
      if( is_susy ) continue;
      double mc_pmag = mc_pt->at(i)*cosh(mc_eta->at(i));
      double mc_mass = mc_m->at(i);
      double mc_E = sqrt(mc_pmag*mc_pmag + mc_mass*mc_mass);
      
      double pe = mc_E;//mc_E->at(i);    
      if( abs(pdg_id)==21 && pe==0 ) is_funny_gluon = true;
      if( is_funny_gluon ) continue;    
      
      double  peta = mc_eta->at(i), pphi = mc_phi->at(i), ppt = mc_pt->at(i);
      
      if (fabs(peta) >= ETAHIGH) continue;
      
      double cc_px = ppt*cos(pphi), cc_py = ppt*sin(pphi), cc_pz = ppt*sinh(peta); 
      
      PseudoJet cc(cc_px, cc_py, cc_pz, mc_E);
      
      int ieta = (int) ((cc.eta() - ETALOW)/deta);
      int iphi = (int) (cc.phi()/dphi);

      //cout<<"ieta = "<<ieta<<" iphi = "<<iphi<<endl;
      //cout<<"Energy = "<<cc.e()<<endl;
      //cout<<"eta bins = "<<ETABINS<<" PHIBINS = "<<PHIBINS<<endl;
      if(cc.e()>0){
	cells[ieta][iphi] += cc.e();
      }
      
      
      
  }//mc_n loop
  
  for (int i = 0; i < ETABINS; i ++){
    for (int j = 0; j < PHIBINS; j ++){
      double p = cells[i][j];
      if (p < 0.01) continue;
      double eta = ETALOW + (i + 0.5)*deta;
      double phi = (j + 0.5)*dphi;
      
      double pt = p/cosh(eta);

      cell_pt->push_back(pt);
      cell_eta->push_back(eta);
      cell_phi->push_back(phi);
      cell_E->push_back(p);
      
      
    }
  }
  cell_n=cell_pt->size();
  return;
}

void EventRecoAnalysis::JetTruthFlavorPJ(vector<PseudoJet> jets,vector<int> *jet_truth_flavor,float DR){

  vector<float> *pt=new vector<float>;
  vector<float> *eta=new vector<float>;
  vector<float> *phi=new vector<float>;
  vector<float> *E=new vector<float>;
  pt->clear();
  eta->clear();
  phi->clear();
  E->clear();
  for(int i=0;i < (int) jets.size();i++){

    pt->push_back(jets.at(i).perp());
    eta->push_back(jets.at(i).eta());
    phi->push_back(jets.at(i).phi_std());
    E->push_back(jets.at(i).E());

  }

  JetTruthFlavor(pt,eta,phi,E,jet_truth_flavor,DR);


  delete pt;
  delete phi;
  delete E;
  delete eta;
  return;
}

void EventRecoAnalysis::JetTruthFlavorM(vector<float> *jets_pt,vector<float> *jets_eta,vector<float> *jets_phi,vector<float> *jets_M,vector<int> *jet_truth_flavor,float DR){

  vector<float> *E=new vector<float>;
  E->clear();
  for(int i=0;i < (int) jets_pt->size();i++){
    TLorentzVector jet;
    jet.SetPtEtaPhiM(jets_pt->at(i),jets_eta->at(i),jets_phi->at(i),jets_M->at(i));
    E->push_back(jet.E());

  }

  JetTruthFlavor(jets_pt,jets_eta,jets_phi,E,jet_truth_flavor,DR);

  delete E;

  return;
}


void EventRecoAnalysis::JetTruthFlavor(vector<float> *jets_pt,vector<float> *jets_eta,vector<float> *jets_phi,vector<float> *jets_E,vector<int> *jet_truth_flavor,float DR){
  
  float deltaRcut=DR;
  float pTCut=5*GeV;
  jet_truth_flavor->clear();
  for(int i=0;i< (int) jets_pt->size();i++){
    double deltaRC = 999.;
    double deltaRB = 999.;
    double deltaRT = 999.;
    double deltaR  = 999.;
    TLorentzVector jet;
    jet.SetPtEtaPhiE(jets_pt->at(i),jets_eta->at(i),jets_phi->at(i),jets_E->at(i));
    for(int j=0;j < (int) mc_pt->size();j++){
      int partonflav=0;
      bool isStable=true;
      //partonflav =  abs(mc_pdgId->at(j));
      
      if      (abs(mc_pdgId->at(j)/1000)%10 == 4)  partonflav =  abs(mc_pdgId->at(j)/1000);
      else if (abs(mc_pdgId->at(j)/100 )%10 == 4)  partonflav =  abs(mc_pdgId->at(j)/100);
      else if (abs(mc_pdgId->at(j)/1000)%10 == 5)  partonflav =  abs(mc_pdgId->at(j)/1000);
      else if (abs(mc_pdgId->at(j)/100 )%10 == 5)  partonflav =  abs(mc_pdgId->at(j)/100); 
      
      if(partonflav==5||partonflav==4){
	if(mc_pt->at(j)>pTCut){
	  for(int z=0;z < (int) mc_child_index->at(j).size();z++){
	    int c_idx=mc_child_index->at(j).at(z);
	    int childflav=0;
	    //childflav= abs(mc_pdgId->at(c_idx));
	    
	    if      (abs(mc_pdgId->at(c_idx)/1000)%10 == 4)  childflav =  abs(mc_pdgId->at(c_idx)/1000);
	    else if (abs(mc_pdgId->at(c_idx)/100 )%10 == 4)  childflav =  abs(mc_pdgId->at(c_idx)/100);
	    else if (abs(mc_pdgId->at(c_idx)/1000)%10 == 5)  childflav =  abs(mc_pdgId->at(c_idx)/1000);
	    else if (abs(mc_pdgId->at(c_idx)/100 )%10 == 5)  childflav =  abs(mc_pdgId->at(c_idx)/100); 
	    
	    if(childflav==partonflav){
	      isStable=false;
	    }
	    
	  }//loop children
	  if(isStable){
	    TLorentzVector hadron;
	    hadron.SetPtEtaPhiM(mc_pt->at(j),mc_eta->at(j),mc_phi->at(j),mc_m->at(j));
	    deltaR=hadron.DeltaR(jet);
	    if(partonflav==4&&deltaR<deltaRC){deltaRC=deltaR;}
	    if(partonflav==5&&deltaR<deltaRB){deltaRB=deltaR;}
	  }
	}//5GeV cut
      }//B or C hadron 
      
    }//mc loop
    if(deltaRB<deltaRcut){
      jet_truth_flavor->push_back(5);
    }
    else if(deltaRC<deltaRcut){
      jet_truth_flavor->push_back(4);
    }
    else{
      jet_truth_flavor->push_back(0);
    }
    
    
  }//jet loop
  
  return;
  
}


void EventRecoAnalysis::SetTopData(GeneratorInfo::input_str *td){

  
  td->EventNumber=eventNumber;
  td->mc_channel_number=channelNumber;
  td->mc_n=mc_n;
  //td->mc_E=mc_E;
  td->mc_pt=mc_pt;
  td->mc_eta=mc_eta;
  td->mc_phi=mc_phi;
  //td->mc_m=mc_m;
  td->mc_status=mc_status;
  td->mc_barcode=mc_barcode;
  td->mc_pdgId=mc_pdgId;
  td->mc_charge=mc_charge;
  td->mc_parent_index=mc_parent_index;
  td->mc_child_index=mc_child_index;
  td->jet_AntiKt4Truth_n= jet_n;
  td->jet_AntiKt4Truth_E= jet_E;
  td->jet_AntiKt4Truth_pt= jet_pt;
  td->jet_AntiKt4Truth_eta= jet_eta;
  td->jet_AntiKt4Truth_phi= jet_phi;

}


void EventRecoAnalysis::RecoMET(){

  float tmp_px=0;
  float tmp_py=0;
  
  for(int i =0;i<(int) cell_pt->size();i++){
    TLorentzVector cell;
    cell.SetPtEtaPhiE(cell_pt->at(i),cell_eta->at(i),cell_phi->at(i),cell_E->at(i));
    tmp_px+=cell.Px();
    tmp_py+=cell.Py();

  }

  for(int j=0;j<(int) mu_pt->size();j++){
    TLorentzVector mu;
    mu.SetPtEtaPhiM(mu_pt->at(j),mu_eta->at(j),mu_phi->at(j),mu_m->at(j));
    tmp_px+=mu.Px();
    tmp_py+=mu.Py();
  }

  m_reco_met_px=tmp_px;
  m_reco_met_py=tmp_py;
  
}
