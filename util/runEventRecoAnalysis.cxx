#include <TROOT.h>
#include <TStyle.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <TFile.h>
#include <TChain.h>

#include "EventRecoAnalysis/EventRecoAnalysis.h"
using namespace std;

int main(int argc, char** argv){

  if(argc<2){

    cout<<"usage: \n\t"<<"./runTruthAnalysis -i FileList.txt -d DatasetID -f FatJetCollectionList \n"<<endl
	<<"\t-i \t Text file containing list of input files, one on each line"  << endl
	<<"\t-d \t DatasetID = Sample identifier"  << endl
	<<"----------------------------------"<<endl
	<<"------- optional arguments -------"<<endl
	<<"----------------------------------"<<endl
	<<endl
	<<"\t-o \t bits for output option : 01 for histogram, 10 for tree " << endl 
	<<"\t-n \t Base name of output file (without extension); will be appended to DatasetID "  << endl
	<<"\t-t \t Name of input tree, default = physics"  << endl
        <<"\t-s \t First event"  << endl
        <<"\t-e \t Last event"  << endl
        <<"\t-JES \t JES treatment"  << endl;
    

    return 1;
  }


  gStyle->SetOptFit(1);
  //gROOT->Reset();
  //gROOT->ProcessLine("#include <map>");
  //gROOT->ProcessLine("#include <vector>");
/*
  if(argc<1){
    cout<<"Specify input file"<<endl;
    exit(1);
  }
*/
  /*
    -i input filelist
    -n output filename
    -o output options
    -d ds_ID
    -t tree

    TruthAnalysis(string ds_ID, vector<string> ds_fileloc, vector<string> v_fatjetcols, string s_jetcol, string s_tree);

   */

  int _ds_ID = -1;
  string _s_fjetcol_list = "";
  string _s_input = "";
  int _b_io = 0x1 << 0;
  string _s_out = "";
  string _s_tree = "";
  int _nfirst = 0;
  int _nlast=-1;
  int _jes=0;
  vector<string> _ds_fileloc; _ds_fileloc.clear();

  //cout<<"0 &_s_out = "<<&_s_out<<" _s_out="<<_s_out<<"_s_out.length() = "<<_s_out.length()<<endl;
  //int _sigInt = 1;
  //------------------
  // Parse cmd line options
  //------------------
  int optind = 1;

  stringstream ss; string sw = "";
  string lit_i="-i";
  string lit_o="-o";
  string lit_n="-n";
  string lit_t="-t";
  string lit_d="-d";
  string lit_s="-s";
  string lit_e="-e";
  string lit_jes="-JES";

  cout<<"0 _s_out.length() = "<<_s_out.length()<<" _s_out.capacity() = "<<_s_out.capacity()<<endl;
  while ((optind < argc) && (argv[optind][0]=='-')) {
    cout<<"Parsing '"<<argv[optind]<<"'"<<endl;
    ss << argv[optind]; sw = ss.str();
    cout<<"Parsing '"<<argv[optind]<<"' ss.str() = "<<ss.str()<<" sw = "<<sw<<" (sw==lit_d) : "<<(sw==lit_d)<<endl;
    if     (sw == lit_i){optind++; ss.str(""); ss << argv[optind]; ss >> _s_input; cout<<"_s_input = "<<_s_input<<endl;}
    else if(sw == lit_o){optind++; ss.str(""); ss << argv[optind]; ss >> _b_io; cout<<"_b_io = "<<_b_io<<endl;}
    else if(sw == lit_n){optind++; ss.str(""); ss << argv[optind]; ss >> _s_out; cout<<"_s_out = "<<_s_out<<endl;}
    else if(sw == lit_t){optind++; ss.str(""); ss << argv[optind]; ss >> _s_tree; cout<<"_s_tree = "<<_s_tree<<endl;}
    else if(sw == lit_d){optind++; ss.str(""); ss << argv[optind]; ss >> _ds_ID; cout<<"_ds_ID = "<<_ds_ID<<endl;}
    else if(sw == lit_s){optind++; ss.str(""); ss << argv[optind]; ss >> _nfirst; cout<<"_nfirst = "<<_nfirst<<endl;}
    else if(sw == lit_e){optind++; ss.str(""); ss << argv[optind]; ss >> _nlast; cout<<"_nlast = "<<_nlast<<endl;}
    else if(sw == lit_jes){optind++; ss.str(""); ss << argv[optind]; ss >> _jes; cout<<"_jes = "<<_jes<<endl;}

    else cout<<"Unknown switch "<<argv[optind]<<" sw = "<<sw<<endl;
    optind++; sw.clear();  ss.str(""); ss.clear();
  }

  lit_i.clear();
  lit_o.clear();
  lit_n.clear();
  lit_t.clear();
  lit_d.clear();
  lit_s.clear();
  lit_e.clear();
  lit_jes.clear();
  cout<<"0 _s_out.length() = "<<_s_out.length()<<" _s_out.capacity() = "<<_s_out.capacity()<<endl;

  if(_s_out==""){_s_out = "HistogramList";}
  if(_s_tree==""){_s_tree = "physics";}

  cout<<"Parsed options"<<endl;
  if (_ds_ID==-1 || _s_input=="") {
    cout<<"-d = "<<_ds_ID<<" -i = "<<_s_input<<" -f = "<<_s_fjetcol_list<<endl;
    cout << "Check SYNTAX:" << endl;
    cout << "Syntax: ./runTruthAnalysis_GRID -i FileList.txt -d DatasetID" << endl;
    return 1 ;
  }

  //read the contents of the file and make the filelist
  for (size_t i=0,n; i <= _s_input.length(); i=n+1)
    {
      n = _s_input.find_first_of(',',i);
      if (n == string::npos)
        n = _s_input.length();
      string tmp = _s_input.substr(i,n-i);
      _ds_fileloc.push_back(tmp);
      tmp.clear();
    }

  int sc=0;
  //cout<<"Before declaring TruthAnalysis"<<endl;
  //TruthAnalysis(string ds_ID, vector<string> ds_fileloc, vector<string> v_fatjetcols, string s_jetcol, string s_tree);
  /*
  cout<<"_ds_ID = "<<_ds_ID<<endl;
  cout<<"_ds_fileloc.size() = "<<_ds_fileloc.size()<<endl;
  cout<<"_b_io = "<<_b_io<<endl;
  cout<<"_s_out = "<<_s_out<<endl;
  cout<<"_s_tree = "<<_s_tree<<endl;
  */


  //MiniSLAnalysis(int ds_ID, vector<string> ds_fileloc, int b_io, string s_out, vector<string> v_fatjetcols, string s_jetcol, int i_channel, string s_tree);
  EventRecoAnalysis* eana=new EventRecoAnalysis(_ds_ID, _ds_fileloc, _b_io, _s_out, _s_tree,_nfirst,_nlast,_jes);
  cout<<"Declared D3PDAnalysis"<<endl;

  _ds_fileloc.clear();
  _s_out.clear();

  sc=eana->Init();
  sc=eana->Loop();
  cout<<"looped"<<endl;
  sc=eana->Terminate();
  cout<<"terminated sc = "<<sc<<endl;

  delete eana;
  //delete tana;
  //cout<<"deleted all"<<endl;
  return sc;
}
